------------------------------ MODULE DoublePointers -------------------------------
EXTENDS Naturals, Sequences, FiniteSets

VARIABLES pointers
CONSTANT MAX_SCOPE
CONSTANT MAX_POINTERS

(* Verify that pointers contain valid memory *)
TypeOK == \A p \in pointers: p.scope >= p.mem

Init == pointers = {}

(* Create a new pointer *)
Push == \E sp \in 0..MAX_SCOPE: \E mem \in 0..MAX_SCOPE:
    mem <= sp
    /\ pointers' = pointers \union {[scope |-> sp, mem |-> mem, pd |-> 1]}

(* Create a pointer to pointer *)
Ref == \E p \in pointers: p.pd = 1 /\ \E sp \in 0..MAX_SCOPE:
    p.scope <= sp
    /\ pointers' = pointers \union {[scope |-> sp, mem |-> p.mem, subscope |-> p.scope, pd |-> 2]}


MIN(a, b) == IF a > b THEN b ELSE a

(* Update pointer that double pointer points to *)
Write == \E p \in pointers: p.pd = 2 /\ \E p1 \in pointers:
    /\ p1.pd = 1
    /\ p1.scope <= p.subscope
    \* /\ p1.scope <= p.scope \* fails because pointer can escape the scope
    /\ pointers' = pointers \union {[scope |-> p.scope, subscope |-> MIN(p.subscope, p1.scope), mem |-> p1.mem, pd |-> 2], [scope |-> p.subscope, mem |-> p1.mem, pd |-> 1]}

(* Dereference pointer *)
DeRef == \E p \in pointers: p.pd = 2 /\ \E sp \in 0..MAX_SCOPE:
    p.scope <= sp
    /\ pointers' = pointers \union {[scope |-> sp, mem |-> p.mem, pd |-> 1]}

Next == IF Cardinality(pointers) < MAX_POINTERS THEN DeRef \/ Ref \/ Push \/ Write ELSE UNCHANGED pointers

Spec == Init /\ [][Next]_<<pointers>>

=============================================================================
