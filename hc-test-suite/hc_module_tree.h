#pragma once

#include ".hc_type.h"
#include ".types.h"
#include ".functions.h"


typedef struct hc_t {
    hc_function_t functions;
} hc_t;


extern hc_t hc;

extern void hc_module_tree_init ();

#define HC_CALL(m) hc.functions.m._
