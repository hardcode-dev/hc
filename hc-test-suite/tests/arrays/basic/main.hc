@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int[4] f1 = (1, 2, 3, 4);
if (#f1 != 4) {
    return 1;
}

int[4] f2;
if (#f2 != 4) {
    return 1;
}
f2 := (5, 6, 7, 8);

if (f1[1] + f2[3] != 10) {
    return 1;
}

int[3] v = (3, 2, 1);

if (#v != 3) {
    return 1;
}

int acc = 0;

let (index (v) i = 0) {
    acc += v[i];
} else {
    return 1;
}

let (index (v) i = 1) {
    acc += v[i];
} else {
    return 1;
}

let (index (v) i = 2) {
    acc += v[i];
} else {
    return 1;
}

if (acc != 6) {
    return 1;
}

/*
  Questionable:
  int[] x = (size = 5, filler = 7);
*/

return 0;
@@
