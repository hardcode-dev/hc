@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
bool x = true;
bool y = false;

{
    word w = -1;
    byte b = -2;
    short s = -3;
    int i = -4;
    long l = -5;
}
{
    uword w = 1;
    ubyte b = 2;
    ushort s = 3;
    uint i = 4;
    ulong l = 5;
}

float f = 3.0;
double d = 4;

return 0;
@@
