@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int[] elements = (1, 7, 2, 4, 9, 88);
for (word:ranged i desc #elements - 1, 1)
    for (word:ranged j asc 0, i - 1)
        sort2 (elements[j], elements[j + 1]);

let (index (elements) i = 0) {
    
    if (elements[i] != 1)
        return 1;

} else {
    return 1;
}

let (index (elements) i1 = 0 &&
     index (elements) i2 = 1 &&
     index (elements) i3 = 2 &&
     index (elements) i4 = 3 &&
     index (elements) i5 = 4 &&
     index (elements) i6 = 5) {

    if (elements[i1] != 1 ||
        elements[i2] != 2 ||
        elements[i3] != 4 ||
        elements[i4] != 7 ||
        elements[i5] != 9 ||
        elements[i6] != 88)
        return 1;

} else {
    return 1;
}

int[] arr = (1, 2, 3, 4, 5);
mut int acc = 0;
for (index (arr) i desc arr)
    acc += arr[i];

if (acc != 15) {
    return 1;
}

return 0;
@@
