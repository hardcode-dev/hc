@function_return_type@
void
@@

@function_parameters@
world& w
@@

@function_body@
w.`break` = true;
`w`.`break` = true; // Same
// `w.break` = true; // Compilation error (fine for parser, fails at semantic level)
@@
