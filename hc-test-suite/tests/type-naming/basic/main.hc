@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
class world w = (fine = true);

bool `break` = true;
if (`break`) {
    building.`break` (b);
    `building.break` (b);
}

if (w.fine) {
    return 1;
}

return 0;
@@
