include(${PROJECT_SOURCE_DIR}/common.cmake)

get_filename_component(current_dir ${CMAKE_CURRENT_SOURCE_DIR} NAME)

subdirlist(subdirs ${CMAKE_CURRENT_SOURCE_DIR})
foreach(subdir ${subdirs})
    # add_hc_test(${current_dir} ${subdir})
    add_hc_syn_gold_test(${current_dir} ${subdir})
endforeach()
