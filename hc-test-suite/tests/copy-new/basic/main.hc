@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
class math.boxd b = expand ((-10, -10, 20, 20), (-5, -5, 30, 15));

if (b.left != -10 || b.top != -10 || b.right != 30 || b.bottom != 20)
    return 1;

class node* a = make_loop (10, 20);

if (a.value != 10 || a.next.value != 20)
    return 1;

return 0;
@@
