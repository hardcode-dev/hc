@function_return_type@
class node*
@@

@function_parameters@
int& a, int& b
@@

@function_body@
class node* a_ref = new (value = a);
class node* b_ref = new (value = b);
a_ref.next = b_ref;
b_ref.next = a_ref;

return a_ref;
@@
