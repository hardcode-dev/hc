@class@
double left;
double top;
double right;
double bottom;
@@

@function_kind@
constructor
@@

@function_parameters@
double& left, double& top, double& right, double& bottom
@@

@function_body@
return (
    left = left,
    top = top,
    right = right,
    bottom = bottom,
);
@@
