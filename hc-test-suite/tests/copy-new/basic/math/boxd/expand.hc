@function_return_value@
class_this
@@

@function_parameters@
class_this& a, class_this& b
@@

@function_body@
mut retval; // Declaring parameters as mutable in only allowed at the beginning of function

retval = copy a;
if (retval.left > b.left)
    retval.left := b.left;
if (retval.right < b.right)
    retval.right := b.right;
if (retval.top > b.top)
    retval.top := b.top;
if (retval.bottom < b.bottom)
    retval.bottom := b.bottom;
@@
