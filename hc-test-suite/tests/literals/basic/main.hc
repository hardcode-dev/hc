@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int a = 3 + 5;
int b = 7/3; // b == 2
// int c = 7.0/3.0; // Compilation error: can't coerce rational literal to integer variable
float d = 7.0/3.0*3.0; // d == 7.0 exactly since (7.0/3.0*3.0) is evaluated as rational number
// float e = 7.0/3; // Compilation error: can't divide rational by integer
// float f = 7/3*1.5; // Compilation error: can't multiply integer by rational
float f = rational (7/3)*1.5; // Exactly 3.0 since 7/3 == 2

bool a = 3 > 5; // false
if (a) {
    return 1;
}
// bool b = 0; // Compilation error: integer can't be coerced to boolean
/*
  Compilation error: integer can't be coerced to boolean

int c = 1;
if (c) {
    something ();
}
*/

ubyte[] en = "Hello, world!";
ubyte[] ru = "Привет, мир!"; // UTF-8 is good

return 0;
@@
