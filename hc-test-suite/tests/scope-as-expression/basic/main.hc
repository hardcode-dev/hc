@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int x = {
    int a = 5;
    int b = 7;
    a + b;
};
if (x != 12) {
    return 1;
}

int x = if (true) {
            3;
        } else {
            5;
        };
if (x != 3) {
    return 1;
}
// This is going to be fugly, but duh 
// Some time in the future it should become ;-less
int y = if (false) 3; else 5;;
if (x != 5) {
    return 1;
}
int z = if (false) {
            return 1; // Also valid
        } else {
            5;
        };
if (z != 5) {
    return 1;
}

return 0;
@@
