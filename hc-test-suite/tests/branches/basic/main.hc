@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
bool t = true;
bool f = false;

if (f) {
    return 1;
}

int x = 5;

if (t) {
    x := 7;
} else {
    return 1;
}

int[] a = (1, 2, 3);

let (index (a) i = 7) {
    return 1;
}

let (index (a) i = 2) {
    if (a[i] != 3)
        return 1;
} else {
    return 1;
}

if (let (index (a) i = 0) && x == 7) {
    if (a[i] == 1)
        return 1;
} else {
    return 1;
}

/*
  Compile error: i not exposed

if (let (index (a) i = 0) || x == 5) {
    print (a[i])
}
*/

return 0;
@@
