@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int x = 5;
int y = 7;
int z = x + y;

int x1, y1, z1;
x1 = 5;
calc_y (y1);
z1 = x1 + y1;
if (z1 != 27) {
    return 1;
}

mut int x2 = 5;
int y2 = 7;
int z2 = x2 + y2;
x2 := 50;
int o = z2 + x2;
if (o != 62) {
    return 1;
}

int[] elements = (3, 7, 12);
mut int acc = 0;

for (index (elements) i asc 0, #elements - 1) {
    acc += elements[i];
}
if (acc != 22) {
    return 1;
}

int x3 = 5;
mut int y3 = 7;
negate (y3);
int z3 = x3 + y3;
if (z3 != -2) {
    return 1;
}

return 0;
@@
