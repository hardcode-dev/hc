@function_kind@
constructor
@@

@function_parameters@
double& right, double& bottom
@@

@function_body@
retval = (
    right = right,
    bottom = bottom
);

retval.left = -right;
retval.bottom = -top;
@@
