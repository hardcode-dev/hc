@class@
double center_x;
double center_y;
double radius;
@@

@function_kind@
constructor
@@

@function_parameters@
double& center_x, double& center_y, double& radius
@@

@function_body@
return (
    center_x = center_x,
    center_y = center_y,
    raidus = radius
);
@@
