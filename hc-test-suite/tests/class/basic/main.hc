@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
class math.boxd box1 = (); // Calling default constructor
if (box1.left != 0) { // All class fields are public
    return 1;
}
class math.boxd box2 = math.boxd (); // Also calling default constructor

class math.circle box = (5, 10, 120); // Calling default constructor with parameters

class math.boxd box3 = math.boxd.identity (); // Calling named constructor
class math.boxd box4 = math.boxd.symmetric (10, 20); // Calling named constructor
return 0;
@@
