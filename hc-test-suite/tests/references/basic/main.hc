@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int x = 5;
int z;
{
    int y = 7;
    int& y2 = y;
    z = x + y2;
}
if (z != 12) {
    return 1;
}

int* a = new (3);
int* b = get_5 ();
if (a - b != -2) {
    return 1;
}
int& a_ref = a;
int c = a_ref + b;
if (c != 8) {
    return 1;
}

int* m = new (5);
int n = 7;
int p = sum (m, n);
if (p != 12) {
    return 1;
}

return 0;
@@
