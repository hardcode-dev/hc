@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
int[] elements = (1, 2, 3);
int s = sum (elements);

class math.point_ref point = {
    int* x = new (3);
    int* y = new (12);
    (x, y);
};

class math.point_ref* point2 = {
    int* x = new (3);
    int* y = new (12);
    new (x, y);
};

{
    mut int x = 3;
    bool overflow;
    add (x, 5, overflow);
    if (overflow) {
        return 1;
    }
    if (x != 8) {
        return 1;
    }
}

{
    mut int x = 3;
    if (add_alt (x, 5)) {
        return 1;
    }
    if (x != 8) {
        return 1;
    }
}

return 0;
@@
