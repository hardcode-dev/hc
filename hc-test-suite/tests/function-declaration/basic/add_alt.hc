@function_return_type@
void
@@

@function_parameters@
mut int accumutalor, int addition
@@

@function_body@
if (accumutalor > 2147483647 - addition) {
    accumutalor := 2147483647;
    return true; // Initialized exactly once at each branch
}

accumutalor += addition;
retval = false; // "return true;" would also suffice
@@
