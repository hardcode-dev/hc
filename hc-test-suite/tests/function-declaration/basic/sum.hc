@function_return_type@
int
@@

@function_parameters@
int[]& elements
@@

@function_body@
mut int acc = 0;

for (index (elements) i asc 0, #elements - 1) {
    acc += elements[i];
}

return acc;
@@
