@class@
int* x;
int* y;
@@

@function_kind@
constructor
@@

@function_parameters@
int* x, int* y
@@

@function_body@
return (
    x = x,
    y = y
);
@@
