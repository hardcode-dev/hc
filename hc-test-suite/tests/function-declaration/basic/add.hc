@function_return_type@
void
@@

@function_parameters@
mut int accumutalor, int addition, out bool overflow
@@

@function_body@
if (accumutalor > 2147483647 - addition) {
    accumutalor := 2147483647;
    overflow = true; // Initialized exactly once at each branch
} else {
    accumutalor += addition;
    overflow = false; // Initialized exactly once at each branch
}
@@
