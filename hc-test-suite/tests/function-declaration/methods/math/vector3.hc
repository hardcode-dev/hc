@class@
double x;
double y;
double z;
@@

@function_kind@
constructor
@@

@function_parameters@
double x, double y, double z
@@

@function_body@
return (
    x = x,
    y = y,
    z = z
);
@@
