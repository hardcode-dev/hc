@function_kind@
method
@@

@function_parameters@
class vector3& addition
@@

@function_body@
x += addition.x;
y += addition.y;
z += addition.z;
@@
