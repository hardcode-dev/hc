@function_return_type@
int
@@

@function_parameters@
@@

@function_body@
mut class math.vector3 v = (1, 0, 3);

v.add (math.vector3 (2, 0, 1));

double len = v.length ();

return if (len == 5.0) 0; else 1;;
@@
