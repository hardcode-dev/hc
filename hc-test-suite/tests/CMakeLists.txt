add_subdirectory(hello-world)
add_subdirectory(scalar-types)
add_subdirectory(arrays)
add_subdirectory(references)
add_subdirectory(coercion)
add_subdirectory(scope-as-expression)
add_subdirectory(branches)
add_subdirectory(literals)
add_subdirectory(variables)
add_subdirectory(loops)
add_subdirectory(function-declaration)
add_subdirectory(class)
# add_subdirectory(copy-new) too weird for now
# add_subdirectory(type-naming) too weird for now