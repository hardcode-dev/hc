#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 1024

int main (int argc, char **argv)
{
    // argv[1] must be the gold, argv[2] must be the temp file

    FILE *gold_file = fopen(argv[1], "r");
    if (gold_file == NULL) {
        fprintf(stderr, "Couldn't open the gold file!\n");
        return -1;
    }

    FILE *temp_file = fopen(argv[2], "r");
    if (temp_file == NULL) {
        fprintf(stderr, "Couldn't open the temp file!\n");
        return -1;
    }

    printf("Comparing:\n\tGold: %s\n\tTemp: %s", argv[1], argv[2]);

    int line_index = 0;
    int read_gold_bytes = 0;
    int read_temp_bytes = 0;

    char *gold_line_buffer_ptr = NULL;
    size_t gold_line_buffer_size = BUFFER_SIZE;

    char *temp_line_buffer_ptr = NULL;
    size_t temp_line_buffer_size = BUFFER_SIZE;

    while (1) {
        read_gold_bytes = getline(&gold_line_buffer_ptr, &gold_line_buffer_size, gold_file);
        read_temp_bytes = getline(&temp_line_buffer_ptr, &temp_line_buffer_size, temp_file);

        if (read_gold_bytes != read_temp_bytes) {
            fprintf(stderr, "Files differ in line length at line %d!\n", line_index);
            return -1;
        }

        // They are equal, so only check one!
        if (read_gold_bytes == -1) {
            printf("File contents are identical!\n");
            free(gold_line_buffer_ptr);
            free(temp_line_buffer_ptr);
            return 0;
        }

        if (strcmp(gold_line_buffer_ptr, temp_line_buffer_ptr) != 0) {
            fprintf(stderr, "Files differ at line %d!\nIt should be:\n\t%s\nIt was:\n\t%s\n", line_index, gold_line_buffer_ptr, temp_line_buffer_ptr);
            free(gold_line_buffer_ptr);
            free(temp_line_buffer_ptr);
            return -1;
        }

        ++line_index;
    }
}