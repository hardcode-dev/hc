function(subdirlist result curdir)
    file(GLOB children RELATIVE ${curdir} ${curdir}/*)
    set(dirlist "")
    foreach(child ${children})
        if(IS_DIRECTORY ${curdir}/${child})
            list(APPEND dirlist ${child})
        endif()
    endforeach()
    set(${result} ${dirlist} PARENT_SCOPE)
endfunction()

function(add_hc_test dirname subdir)
    set(generated_source_dir ${PROJECT_SOURCE_DIR}/tests/${dirname}/${subdir})
    set(generated_target_dir ${CMAKE_CURRENT_BINARY_DIR}/.${dirname}_${subdir}_hc)
    set(generated_target ${generated_target_dir}/.types.h)
    set(target ${dirname}_${subdir})

    string(REPLACE " " "_" target ${target})

    add_custom_command(
        OUTPUT ${generated_target}
        COMMAND ${PROJECT_SOURCE_DIR}/../hardcode/hc/hc --compile-snapshot ${generated_source_dir} ${generated_target_dir}
        DEPENDS ${generated_source_dir}
    )

    add_executable(${target}
        ${PROJECT_SOURCE_DIR}/main.c
        ${PROJECT_SOURCE_DIR}/hc_type.c
        ${PROJECT_SOURCE_DIR}/hc_module_tree.c
        ${generated_target}
    )
    target_include_directories(${target} PRIVATE ${generated_target_dir})
    target_compile_options(${target} PRIVATE -Wall -Wextra -Werror=implicit-function-declaration -D_GNU_SOURCE=1)

    add_test(
        NAME ${target}-test
        COMMAND ${target}
    )
endfunction()

function(add_hc_syn_gold_test dirname subdir)
    set(source_dir ${PROJECT_SOURCE_DIR}/tests/${dirname}/${subdir})
    set(gold_syn_file ${PROJECT_SOURCE_DIR}/tests/${dirname}/${dirname}_${subdir}_syn_gold)
    set(generated_syn_file ${CMAKE_CURRENT_BINARY_DIR}/${dirname}_${subdir}_syn)


    set(syn_test_target ${dirname}_${subdir})
    string(REPLACE " " "_" syn_test_target ${syn_test_target})

    add_custom_command(
        OUTPUT ${generated_syn_file}
        COMMAND ${PROJECT_SOURCE_DIR}/../hardcode/hc/hc --dump-syn ${source_dir} ${generated_syn_file}
        DEPENDS ${source_dir}
    )

    add_custom_target(${syn_test_target} ALL DEPENDS ${generated_syn_file})

    add_test(
        NAME ${syn_test_target}_test
        COMMAND ${CMAKE_BINARY_DIR}/goldcheck ${gold_syn_file} ${generated_syn_file}
    )
endfunction()
