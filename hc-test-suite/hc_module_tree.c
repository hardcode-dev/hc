#include "hc_module_tree.h"

#include <stdio.h>


hc_t hc;


#include ".generated_functions_impl.h"


static void load_functions ()
{
#include ".load_functions_impl.h"
}
static void load_generated_functions ()
{
#include ".generated_functions_load.h"
}

void hc_module_tree_init ()
{
    load_functions ();
    load_generated_functions ();
}
