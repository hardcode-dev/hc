Running tests
=============

```bash
mkdir build
cd build
cmake ..
make
make test
```
