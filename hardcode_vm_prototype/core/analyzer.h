#ifndef ANALYZER_H
#define ANALYZER_H

#include <variant>
#include <string>
#include <core/program.h>
#include <sstream>
#include <variant>

namespace NVirtualMachine::NCore {
    namespace NFormula {
        struct TMax;
        struct TSum;
        struct TSub;
        struct TMultiply;
        struct TVariable;
        struct TConst;
        using TFormulaElement = std::variant<TMax, TSum, TSub, TMultiply, TComplexity, TVariable, TConst>;

        struct TBinaryElement {
            TFormulaElement *left;
            TFormulaElement *right;
        };

        struct TMax : TBinaryElement {};
        struct TSum : TBinaryElement {};
        struct TSub : TBinaryElement {};
        struct TMultiply : TBinaryElement {};

        struct TVariable {
            std::string name;
        };

        struct TConst {
            uint64_t value;
        };
    }

    enum class EAnalyzerError {
        UnmarkedSet = 0,
        TooHard,
        MAX_REASON
    };

    using TAnalysisResult = std::variant<NFormula::TFormulaElement*, EAnalyzerError>;

    TAnalysisResult HandleInstructionAnalysis(const TFunction& p, uint64_t &instructionPointer);

    TAnalysisResult AnalyzeScope(const TFunction& p, uint64_t start, uint64_t end) {
        end = std::min(end, p.bytecode.size());
        NFormula::TFormulaElement* currentComplexity = nullptr;

        for(uint64_t instructionPointer = start; instructionPointer < end; ++instructionPointer) {
            auto result = HandleInstructionAnalysis(p, instructionPointer);
            if (std::holds_alternative<EAnalyzerError>(result)) {
                // TODO fix memory leak
                return result;
            }
            NFormula::TFormulaElement* sum = new NFormula::TFormulaElement();
            if (currentComplexity != nullptr) {
                *sum = (NFormula::TSum) {
                    currentComplexity,
                    std::get<NFormula::TFormulaElement*>(result)
                };
                currentComplexity = sum;
            } else {
                currentComplexity = std::get<NFormula::TFormulaElement*>(result);
            }
        }
        if (currentComplexity == nullptr) {
            currentComplexity = new NFormula::TFormulaElement();
            *currentComplexity = (TComplexity) {0, 0};
        }
        return currentComplexity;
    }

    TAnalysisResult HandleLoopInstruction(const TFunction& p, uint64_t &instructionPointer) {
        const auto &instr = p.bytecode[instructionPointer];
        uint64_t endAddr = instr.next;
        uint64_t newInstructionPointer = endAddr;
        auto result = AnalyzeScope(p, instructionPointer + 1, newInstructionPointer);
        instructionPointer = newInstructionPointer;
        if (std::holds_alternative<EAnalyzerError>(result)) {
            return result;
        }
        auto endCost = HandleInstructionAnalysis(p, endAddr);
        if (std::holds_alternative<EAnalyzerError>(endCost)) {
            return endCost;
        }
        NFormula::TFormulaElement* mult = new NFormula::TFormulaElement();
        NFormula::TFormulaElement* max = new NFormula::TFormulaElement();
        NFormula::TFormulaElement* start = new NFormula::TFormulaElement();
        NFormula::TFormulaElement* end = new NFormula::TFormulaElement();
        NFormula::TFormulaElement* sub = new NFormula::TFormulaElement();
        NFormula::TFormulaElement* zero = new NFormula::TFormulaElement();
        NFormula::TFormulaElement* sum = new NFormula::TFormulaElement();
        // TODO track bounds
        *start = NFormula::TVariable {"A"};
        *end = NFormula::TVariable {"B"};
        *zero = NFormula::TConst {0};
        *sub = NFormula::TSub {
            end,
            start
        };
        *max = NFormula::TMax {
            sub,
            zero
        };
        *mult = NFormula::TMultiply {
            max,
            std::get<NFormula::TFormulaElement*>(result)
        };
        *sum = NFormula::TSum{
            mult,
            std::get<NFormula::TFormulaElement*>(endCost)
        };
        return sum;
    }

    TAnalysisResult HandleInstructionAnalysis(const TFunction& p, uint64_t &instructionPointer) {
        const auto &current = p.bytecode[instructionPointer];
        const auto &instr = p.bytecode[instructionPointer];
        switch (current.type)
        {
        case EInstructionType::IF:
        {
            uint64_t elseAddr = instr.next;
            auto result = AnalyzeScope(p, instructionPointer + 1, elseAddr + 1);
            if (std::holds_alternative<EAnalyzerError>(result)) {
                return result;
            }
            if (p.bytecode[elseAddr].type != EInstructionType::ELSE) {
                instructionPointer = elseAddr; // Should be endif
                NFormula::TFormulaElement* sum = new NFormula::TFormulaElement();
                NFormula::TFormulaElement* complexity = new NFormula::TFormulaElement();
                *complexity = instructionComplexity.at(instr.type);
                *sum = (NFormula::TSum) {
                    complexity,
                    std::get<NFormula::TFormulaElement*>(result)
                };
                return sum;
            }
            uint64_t endif_addr = p.bytecode[elseAddr].next;
            TAnalysisResult else_result = AnalyzeScope(p, elseAddr + 1, endif_addr);
            if (std::holds_alternative<EAnalyzerError>(else_result)) {
                return else_result;
            }
            NFormula::TFormulaElement* max = new NFormula::TFormulaElement();
            NFormula::TFormulaElement* sum = new NFormula::TFormulaElement();
            NFormula::TFormulaElement* complexity = new NFormula::TFormulaElement();
            *complexity = instructionComplexity.at(instr.type);
            *max = (NFormula::TMax) {
                std::get<NFormula::TFormulaElement*>(result),
                std::get<NFormula::TFormulaElement*>(else_result),
            };
            *sum = (NFormula::TSum) {
                complexity,
                max
            };
            instructionPointer = endif_addr;
            return sum;
        }
        case EInstructionType::RANGED_LOOP_ASCENDING:
        {
            return HandleLoopInstruction(p, instructionPointer);
        }
        default:
            NFormula::TFormulaElement* complexity = new NFormula::TFormulaElement();
            *complexity = instructionComplexity.at(instr.type);
            return complexity;
        }
    }



    // TODO actually pretty
    std::string PrettyPrintFormula(const NFormula::TFormulaElement& f) {
        std::stringstream ss;
        if (std::holds_alternative<NFormula::TSum>(f)) {
            auto sum = std::get<NFormula::TSum>(f);
            ss << PrettyPrintFormula(*sum.left) << " + " << std::endl << PrettyPrintFormula(*sum.right);
        } else if (std::holds_alternative<TComplexity>(f)) {
            auto complexity = std::get<TComplexity>(f);
            ss << complexity.computational << " + " << complexity.memoryTransfer << " mt";
        } else if (std::holds_alternative<NFormula::TSub>(f)) {
            auto sub = std::get<NFormula::TSub>(f);
            ss << PrettyPrintFormula(*sub.left) << " - " << PrettyPrintFormula(*sub.right);
        } else if (std::holds_alternative<NFormula::TVariable>(f)) {
            auto var = std::get<NFormula::TVariable>(f);
            ss << var.name;
        } else if (std::holds_alternative<NFormula::TMultiply>(f)) {
            auto mul = std::get<NFormula::TMultiply>(f);
            ss << PrettyPrintFormula(*mul.left) << " * {" << std::endl <<
                PrettyPrintFormula(*mul.right) << std::endl << "}";
        } else if (std::holds_alternative<NFormula::TMax>(f)) {
            auto max = std::get<NFormula::TMax>(f);
            ss << "max {" << std::endl << PrettyPrintFormula(*max.left) << "," << std::endl <<
                PrettyPrintFormula(*max.right) << std::endl << "}";
        } else if (std::holds_alternative<NFormula::TConst>(f)) {
            auto var = std::get<NFormula::TConst>(f);
            ss << var.value;
        } else {
            ss << "[NOT IMPLEMENTED]";
        }
        return ss.str();
    }

    // Function should be resolved
    TAnalysisResult AnalyzeFunction(const TFunction& f) {
        return AnalyzeScope(f, 0, f.bytecode.size());
    }


}

#endif