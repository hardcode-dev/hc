#ifndef RESOLVER_H
#define RESOLVER_H

#include <core/program.h>
#include <stack>
#include <functional>

namespace NVirtualMachine::NCore {
    using resolultion_stack_t = std::stack<std::reference_wrapper<TInstruction>>;

    bool HandleInstruction(
            resolultion_stack_t &rstack,
            TInstruction &instr,
            const uint64_t address,
            const std::vector<TInstruction> &bytecode,
            uint64_t &currentRedzoneOffset) {
        auto resolveTopInstructionIf = [&rstack, &address](EInstructionType type) -> bool {
            if (rstack.empty()) {
                return false;
            }
            auto &v = rstack.top().get();
            if (v.type == type) {
                v.next = address;

                rstack.pop();
                return true;
            }
            return false;
        };

        switch (instr.type)
        {
        case EInstructionType::CALL:
        {
            // Calls are only allowed to point to functions
            // And no recursion is allowed (add flag?)
            // TODO check for recursion and call validity
            break;
        }
        case EInstructionType::FUNC:
            if (address != 0) {
                return false;
            }
            currentRedzoneOffset = instr.args[0] + 3;
            // Function cannot be defined in some scope
            if (!rstack.empty()) {
                return false;
            }
            rstack.push(std::ref(instr));
            break;
        case EInstructionType::RET:
            // Function may return inside some scope
            // close function only if ret is in the parent
            if (rstack.size() == 1 && !resolveTopInstructionIf(EInstructionType::FUNC)) {
                return false;
            }
            currentRedzoneOffset = 0;
            break;
        case EInstructionType::RANGED_LOOP_ASCENDING:
            currentRedzoneOffset -= 2;
            instr.redzoneOffset = currentRedzoneOffset;
        case EInstructionType::IF:
            rstack.push(std::ref(instr));
            break;
        case EInstructionType::RANGED_LOOP_ASCENDING_END:
            if (!resolveTopInstructionIf(EInstructionType::RANGED_LOOP_ASCENDING)) {
                return false;
            }
            currentRedzoneOffset += 2;
            break;
        case EInstructionType::ENDIF:
            if (!(resolveTopInstructionIf(EInstructionType::IF) ||
                    resolveTopInstructionIf(EInstructionType::ELSE))) {
                return false;
            }
            break;
        case EInstructionType::ELSE:
            if (!resolveTopInstructionIf(EInstructionType::IF)) {
                return false;
            }
            rstack.push(std::ref(instr));
            break;
        default:
            break;
        }
        return true;
    }

    // Makes program ready for execution
    // returns false if program is not valid
    bool ResolveProgram(TProgram &p) {
        bool isProgramValid = true;
        for (auto& func : p.functions) {
            resolultion_stack_t resolution_stack;
            bool valid = true;
            uint64_t currentRedzoneOffset = 0;
            for (uint64_t address = 0; address < func.bytecode.size() && valid; ++address) {
                valid = HandleInstruction(resolution_stack, func.bytecode[address], address, func.bytecode, currentRedzoneOffset);
            }
            isProgramValid &= valid && resolution_stack.empty();
        }
        return isProgramValid;
    }
}
#endif