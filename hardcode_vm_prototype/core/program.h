#ifndef PROGRAM_H
#define PROGRAM_H

#include "instructions.h"
#include <vector>

namespace NVirtualMachine::NCore {

    struct TFunction {
        std::vector<TInstruction> bytecode;
    };

    struct TProgram {
        std::vector<TFunction> functions;
    };

}
#endif // PROGRAM_H