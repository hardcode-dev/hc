#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H
#include <cstdint>
#include <map>
#include <algorithm>
#include "registers.h"
#include <string>
#include <stdexcept>

namespace NVirtualMachine::NCore {

    struct TComplexity{
        int computational;
        int memoryTransfer;
    };

    enum class EInstructionType {
        // ML = memory location
        SET_64 = 0, // sets ML at [0] to value at [1]
        COPY_64, // stores value at ML [1] to ML [0]
        ADD_UINT64, // adds ML [0] to ML [1] and stores the result in ML [2]
        SUB_UINT64, // subtracts ML [0] from ML [1] and stores the result in ML [2]
        RANGED_LOOP_ASCENDING, // iterates while ML [0] <= ML [1]
        RANGED_LOOP_ASCENDING_END, // ends the loop
        IF, // Continues if ML[0] is not zero, otherwise goes to else or endif
        ELSE, // Marking instruction
        ENDIF, // Marking instruction
        FUNC, // Start of the function, [0] is the redzone size - 3
        RET, // Returns from the function by reseting stack pointer
        CALL, // calls function with index [0] where offset at [1] is the start of the stack
        MARK, // fake instruction used to mark variables, marks ML at [0] as variable index [1]
        UNMARK, // fake instruction used to unmark variables, unmarks ML [0]
        REF, // Creates a reference to local address at [1] and puts it in ML [0]
        GETREF, // Gets value of refence at [1] and puts it in ML [0]
        SETREF, // Sets value of refence at [1] to value in ML [1]
    };


    const std::map<EInstructionType, TComplexity> instructionComplexity = {
        {EInstructionType::SET_64, {1, 0}},
        {EInstructionType::COPY_64, {1, 1}},
        {EInstructionType::ADD_UINT64, {5, 0}},
        {EInstructionType::SUB_UINT64, {5, 0}},
        {EInstructionType::IF, {2, 0}}, // TODO tweak cost
        {EInstructionType::ELSE, {2, 0}}, // TODO tweak cost
        {EInstructionType::ENDIF, {2, 0}}, // TODO tweak cost
        {EInstructionType::RANGED_LOOP_ASCENDING, {2, 0}}, // TODO tweak cost
        {EInstructionType::RANGED_LOOP_ASCENDING_END, {9, 0}}, // TODO tweak cost
        {EInstructionType::CALL, {13, 0}}, // TODO tweak cost
        {EInstructionType::FUNC, {14, 0}}, // TODO tweak cost
        {EInstructionType::RET, {15, 0}}, // TODO tweak cost
        {EInstructionType::REF, {15, 0}}, // TODO tweak cost
        {EInstructionType::SETREF, {15, 0}}, // TODO tweak cost
        {EInstructionType::GETREF, {15, 0}}, // TODO tweak cost
    };

    struct TInstruction {
        EInstructionType type;
        uint64_t args[3];
        uint64_t next; // used for conditional instructions
        uint64_t redzoneOffset; // used for internal data storage
    };

    TInstruction set(uint64_t address, uint64_t value) {
        TInstruction instr;
        instr.type = EInstructionType::SET_64;
        instr.args[0] = address;
        instr.args[1] = value;
        return instr;
    }

    TInstruction copy(uint64_t dest, uint64_t src) {
        TInstruction instr;
        instr.type = EInstructionType::COPY_64;
        instr.args[0] = dest;
        instr.args[1] = src;
        return instr;
    }

    TInstruction add(uint64_t address0, uint64_t address1, uint64_t address2) {
        TInstruction instr;
        instr.type = EInstructionType::ADD_UINT64;
        instr.args[0] = address0;
        instr.args[1] = address1;
        instr.args[2] = address2;
        return instr;
    }

    TInstruction sub(uint64_t address0, uint64_t address1, uint64_t address2) {
        TInstruction instr;
        instr.type = EInstructionType::SUB_UINT64;
        instr.args[0] = address0;
        instr.args[1] = address1;
        instr.args[2] = address2;
        return instr;
    }

    TInstruction ranged_ascending(uint64_t iteration_var_addr, uint64_t limit_addr) {
        TInstruction instr;
        instr.type = EInstructionType::RANGED_LOOP_ASCENDING;
        instr.args[0] = iteration_var_addr;
        instr.args[1] = limit_addr;
        return instr;
    }

    TInstruction ranged_ascending_end() {
        TInstruction instr;
        instr.type = EInstructionType::RANGED_LOOP_ASCENDING_END;
        return instr;
    }

    TInstruction if_instr(uint64_t address) {
        TInstruction instr;
        instr.type = EInstructionType::IF;
        instr.args[0] = address;
        return instr;
    }

    TInstruction else_instr() {
        TInstruction instr;
        instr.type = EInstructionType::ELSE;
        return instr;
    }

    TInstruction endif_instr() {
        TInstruction instr;
        instr.type = EInstructionType::ENDIF;
        return instr;
    }

    TInstruction mark(uint64_t address, const std::string &variable, const std::vector<std::string> &variable_names) {
        TInstruction instr;
        auto it = std::find(variable_names.begin(), variable_names.end(), variable);
        if (it == variable_names.end()) {
            throw std::runtime_error("no such variable");
        }
        instr.type = EInstructionType::MARK;
        instr.args[0] = address;
        instr.args[1] = it - variable_names.begin();
        return instr;
    }

    TInstruction unmark(uint64_t address) {
        TInstruction instr;
        instr.type = EInstructionType::UNMARK;
        instr.args[0] = address;
        return instr;
    }

    TInstruction func(uint64_t redzoneSize) {
        TInstruction instr;
        instr.type = EInstructionType::FUNC;
        instr.args[0] = redzoneSize;
        return instr;
    }

    TInstruction ret() {
        TInstruction instr;
        instr.type = EInstructionType::RET;
        return instr;
    }

    TInstruction call(uint64_t funcAddr, uint64_t stackOffset) {
        TInstruction instr;
        instr.type = EInstructionType::CALL;
        instr.args[0] = funcAddr;
        instr.args[1] = stackOffset;
        return instr;
    }

    TInstruction ref(uint64_t refaddr, uint64_t stackOffset) {
        TInstruction instr;
        instr.type = EInstructionType::REF;
        instr.args[0] = refaddr;
        instr.args[1] = stackOffset;
        return instr;
    }

    TInstruction getref(uint64_t stackOffset, uint64_t refaddr) {
        TInstruction instr;
        instr.type = EInstructionType::GETREF;
        instr.args[0] = stackOffset;
        instr.args[1] = refaddr;
        return instr;
    }

    TInstruction setref(uint64_t refaddr, uint64_t stackOffset) {
        TInstruction instr;
        instr.type = EInstructionType::SETREF;
        instr.args[0] = refaddr;
        instr.args[1] = stackOffset;
        return instr;
    }
}
#endif