#ifndef REGISTERS_H
#define REGISTERS_H

namespace NVirtualMachine::NCore {
    enum class ERegs : uint64_t {
        RegA = 0,
        RegB,
        RegI, // Loop variable register
        RegM, // Loop limit register
        REGISTER_MAX,
    };
}
#endif