#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <core/instructions.h>
#include <core/program.h>
#include <core/registers.h>
#include <stdexcept>
#include <stack>
#include <optional>


namespace NVirtualMachine::NCore {
    struct TLoopState {
        uint64_t loop_variable, limit, loop_start_instruction;
    };

    struct TProgramState {
        uint64_t frameStart;
        TComplexity complexity;
        std::vector<uint64_t> stack;
        std::stack<TLoopState> loop_state_stash;
        std::optional<TLoopState> current_loop_state;
    };


    // Program should be resolved
    TProgramState executeProgram(const TProgram &program) {
        TProgramState state;
        state.stack = std::vector<uint64_t>(1024, 0);
        state.complexity = {0, 0};
        state.frameStart = 0;
        auto restoreLoopState = [&state] () {
            if (!state.loop_state_stash.empty()) {
                state.current_loop_state = state.loop_state_stash.top();
                state.loop_state_stash.pop();
            } else {
                state.current_loop_state = std::nullopt;
            }
        };

        auto resolveRelativeAddress = [&](uint64_t address) -> uint64_t {
            return address + state.frameStart;
        };

        auto writeMemoryRelative = [&](uint64_t address, uint64_t value) {
            uint64_t resolved = resolveRelativeAddress(address);
            state.stack[resolved] = value;
        };

        auto readMemoryRelative = [&](uint64_t address) -> uint64_t {
            auto resolved = resolveRelativeAddress(address);
            return state.stack[resolved];
        };

        uint64_t functionOffset = 0;
        for (uint64_t instructionPointer = 0; instructionPointer < program.functions[functionOffset].bytecode.size(); ++instructionPointer) {
            const TFunction* currentFunction = &program.functions[functionOffset];
            const TInstruction &instr = currentFunction->bytecode[instructionPointer];
            auto instr_complexity = instructionComplexity.at(instr.type);
            state.complexity.computational += instr_complexity.computational;
            state.complexity.memoryTransfer += instr_complexity.memoryTransfer;
            switch (instr.type)
            {
            case EInstructionType::SET_64:
                // TODO check error
                writeMemoryRelative(instr.args[0], instr.args[1]);
                break;
            case EInstructionType::ADD_UINT64:
            {
                // TODO check error
                uint64_t a = readMemoryRelative(instr.args[1]);
                uint64_t b = readMemoryRelative(instr.args[0]);
                writeMemoryRelative(instr.args[2], a + b);
                break;
            }
            case EInstructionType::SUB_UINT64:
            {
                uint64_t a = readMemoryRelative(instr.args[1]);
                uint64_t b = readMemoryRelative(instr.args[0]);
                writeMemoryRelative(instr.args[2], a - b);
                break;
            }
            case EInstructionType::REF:
                writeMemoryRelative(instr.args[0], resolveRelativeAddress(instr.args[1]));
                break;
            case EInstructionType::SETREF: {
                state.stack[readMemoryRelative(instr.args[0])] = readMemoryRelative(instr.args[1]);
                break;
            }
            case EInstructionType::GETREF: {

                uint64_t value = state.stack[readMemoryRelative(instr.args[1])];
                writeMemoryRelative(instr.args[0], value);
                break;
            }
            case EInstructionType::COPY_64:
                writeMemoryRelative(instr.args[0], readMemoryRelative(instr.args[1]));
                break;
            case EInstructionType::IF:
                if (readMemoryRelative(instr.args[0]) == 0) {
                    instructionPointer = instr.next; // Skips else
                }
                break;
            case EInstructionType::ELSE:
                instructionPointer = instr.next; // if we encounter else, it means that if was executed
                break;
            case EInstructionType::ENDIF:
                // NOP for runtime
                break;
            case EInstructionType::RANGED_LOOP_ASCENDING:
            {
                if (state.current_loop_state) {
                    state.loop_state_stash.push(*state.current_loop_state);
                }
                TLoopState current_loop_state;
                writeMemoryRelative(instr.redzoneOffset, readMemoryRelative(instr.args[0]));
                writeMemoryRelative(instr.redzoneOffset + 1, readMemoryRelative(instr.args[1]));
                current_loop_state.loop_variable = instr.redzoneOffset;
                current_loop_state.limit = instr.redzoneOffset + 1;
                current_loop_state.loop_start_instruction = instructionPointer;

                state.current_loop_state = current_loop_state;
                if (readMemoryRelative(instr.redzoneOffset) >= readMemoryRelative(instr.redzoneOffset + 1)) {
                    instructionPointer = instr.next;
                    restoreLoopState();
                }
                break;
            }
            case EInstructionType::RANGED_LOOP_ASCENDING_END:
            {
                uint64_t i = readMemoryRelative(state.current_loop_state->loop_variable) + 1;
                writeMemoryRelative(state.current_loop_state->loop_variable, i);
                // Todo check errors
                if (i < readMemoryRelative(state.current_loop_state->limit)) {
                    instructionPointer = state.current_loop_state->loop_start_instruction;
                } else {
                    restoreLoopState();
                }
                break;
            }
            case EInstructionType::CALL:
            {
                uint64_t oldInstructionPointer = instructionPointer;
                uint64_t oldFunctionPointer = functionOffset;
                functionOffset = instr.args[0];
                instructionPointer = 0;
                uint64_t oldFrameStart = state.frameStart;
                const TInstruction &func = currentFunction->bytecode[instructionPointer];
                state.frameStart = resolveRelativeAddress(instr.args[1]);
                writeMemoryRelative(0x0, oldFrameStart);
                writeMemoryRelative(0x1, oldInstructionPointer);
                writeMemoryRelative(0x2, oldFunctionPointer);
                break;
            }
            case EInstructionType::RET:
            {
                functionOffset = readMemoryRelative(0x2);
                instructionPointer = readMemoryRelative(0x1);
                state.frameStart = readMemoryRelative(0x0);
            }
            case EInstructionType::MARK:
            case EInstructionType::UNMARK:
                // NOP for runtime
                break;
            default:
                throw new std::runtime_error("not supported");
                break;
            }
        }
        return state;
    }
};
#endif