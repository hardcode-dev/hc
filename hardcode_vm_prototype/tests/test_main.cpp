#include <tests/modules/test_instructions.h>
#include <tests/modules/test_analysis.h>

int main() {
    test_instructions();
    test_analysis();
    return 0;
}