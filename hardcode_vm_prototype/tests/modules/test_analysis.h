#include <core/instructions.h>
#include <core/program.h>
#include <core/registers.h>
#include <core/executor.h>
#include <core/analyzer.h>
#include <assert.h>
#include <iostream>

void test_empty_analytics() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction f;
    f.bytecode = {
    };
    p.functions.push_back(f);
    assert(ResolveProgram(p));
    auto result = AnalyzeFunction(f);
    std::string expectedResult = "0 + 0 mt";
    std::string actualResult = PrettyPrintFormula(*std::get<NFormula::TFormulaElement*>(result));
    // Will do for now
    assert(expectedResult == actualResult);
}

void test_basic_analytics() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction f;
    f.bytecode = {
        set(0x0, 123),
        set(0x1, 456),
        add(0x0, 0x1, 0x2)
    };
    p.functions.push_back(f);
    assert(ResolveProgram(p));
    auto result = AnalyzeFunction(p.functions[0]);
    std::string expectedResult = {
        "1 + 0 mt + \n"
        "1 + 0 mt + \n"
        "5 + 0 mt"
    };
    std::string actualResult = PrettyPrintFormula(*std::get<NFormula::TFormulaElement*>(result));
    // Will do for now
    assert(expectedResult == actualResult);
}

void test_if_analytics() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction f;
    f.bytecode = {
        set(0x0, 0),
        set(0x1, 1),
        if_instr(0x1),
        set(0x2, 1),
        endif_instr(),
        if_instr(0x0),
        set(0x2, 1),
        else_instr(),
        if_instr(0x1),
        set(0x2, 1),
        endif_instr(),
        endif_instr()
    };
    p.functions.push_back(f);
    assert(ResolveProgram(p));
    auto result = AnalyzeFunction(p.functions[0]);
    std::string expectedResult = \
        "1 + 0 mt + \n"
        "1 + 0 mt + \n"
        "2 + 0 mt + \n"
        "1 + 0 mt + \n"
        "2 + 0 mt + \n"
        "2 + 0 mt + \n"
        "max {\n"
        "1 + 0 mt + \n"
        "2 + 0 mt,\n"
        "2 + 0 mt + \n"
        "1 + 0 mt + \n"
        "2 + 0 mt\n"
        "}";
    std::string actualResult = PrettyPrintFormula(*std::get<NFormula::TFormulaElement*>(result));
    // Will do for now
    assert(expectedResult == actualResult);
}

void test_loop_analytics() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction f;
    f.bytecode = {
        set(0x0, 0),
        set(0x1, 6),
        ranged_ascending(0x0, 0x1),
        copy(0x3, 0x0),
        set(0x2, 0),
        ranged_ascending(0x2, 0x3),
        add(0x2, 0x4, 0x4),
        ranged_ascending_end(),
        ranged_ascending_end(),
    };
    p.functions.push_back(f);
    assert(ResolveProgram(p));
    auto result = AnalyzeFunction(p.functions[0]);
    std::string expectedResult = \
        "1 + 0 mt + \n"
        "1 + 0 mt + \n"
        "max {\n"
        "B - A,\n"
        "0\n"
        "} * {\n"
        "1 + 1 mt + \n"
        "1 + 0 mt + \n"
        "max {\n"
        "B - A,\n"
        "0\n"
        "} * {\n"
        "5 + 0 mt\n"
        "} + \n"
        "9 + 0 mt\n"
        "} + \n"
        "9 + 0 mt";
    std::string actualResult = PrettyPrintFormula(*std::get<NFormula::TFormulaElement*>(result));
    // Will do for now
    assert(expectedResult == actualResult);
}

void test_analysis() {
    #define RUNTEST(x) std::cout << #x << " "; std::flush(std::cout); x(); std::cout << "OK" << std::endl;
    RUNTEST(test_empty_analytics);
    RUNTEST(test_basic_analytics);
    RUNTEST(test_if_analytics);
    RUNTEST(test_loop_analytics);
    #undef RUNTEST
}
