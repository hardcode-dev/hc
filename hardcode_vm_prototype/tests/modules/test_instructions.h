#include <core/instructions.h>
#include <core/program.h>
#include <core/registers.h>
#include <core/executor.h>
#include <assert.h>
#include <iostream>
#include <core/resolver.h>

NVirtualMachine::NCore::TProgram constructSingleFunctionProgram(const std::vector<NVirtualMachine::NCore::TInstruction>& instr) {
    NVirtualMachine::NCore::TProgram result;
    NVirtualMachine::NCore::TFunction func;
    func.bytecode = instr;
    result.functions.push_back(
        func
    );
    return result;
}

void test_set() {
    using namespace NVirtualMachine::NCore;
    TProgram p = constructSingleFunctionProgram({
        set(0x0, 123),
        set(0x1, 456)
    });
    auto state = executeProgram(p);
    assert(state.stack[0x0] == 123);
    assert(state.stack[0x1] == 456);
}

void test_set_copy() {
    using namespace NVirtualMachine::NCore;
    TProgram p = constructSingleFunctionProgram({
        set(0x0, 123),
        set(0x1, 456),
        copy(0x0, 0x1),
    });
    auto state = executeProgram(p);
    assert(state.stack[0x0] == 456);
    assert(state.stack[0x1] == 456);
}

void test_add() {
    using namespace NVirtualMachine::NCore;
    TProgram p = constructSingleFunctionProgram({
        set(0x0, 123),
        set(0x1, 456),
        add(0x0, 0x1, 0x2)
    });
    auto state = executeProgram(p);
    assert(state.stack[0x0] == 123);
    assert(state.stack[0x1] == 456);
    assert(state.stack[0x2] == 579);
}

void test_sub() {
    using namespace NVirtualMachine::NCore;
    TProgram p = constructSingleFunctionProgram({
        set(0x0, 123),
        set(0x1, 456),
        sub(0x0, 0x1, 0x2)
    });
    auto state = executeProgram(p);
    assert(state.stack[0x0] == 123);
    assert(state.stack[0x1] == 456);
    assert(state.stack[0x2] == 333);
}

void test_branching() {
    using namespace NVirtualMachine::NCore;
    TProgram p = constructSingleFunctionProgram({
        set(0x0, 0),
        if_instr(0x0),
        set(0xf0, 1),
        endif_instr(),

        set(0x1, 1),
        if_instr(0x1),
        set(0xf1, 1),
        endif_instr(),

        if_instr(0x1),
        set(0xf2, 1),
        else_instr(),
        set(0xf3, 1),
        endif_instr(),

        if_instr(0x0),
        set(0xf4, 1),
        else_instr(),
        set(0xf5, 1),
        endif_instr(),
    });
    assert(ResolveProgram(p));
    auto state = executeProgram(p);
    assert(state.stack[0xf0] == 0);
    assert(state.stack[0xf1] == 1);
    assert(state.stack[0xf2] == 1);
    assert(state.stack[0xf3] == 0);
    assert(state.stack[0xf4] == 0);
    assert(state.stack[0xf5] == 1);
}

void test_loop() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction main;
    main.bytecode = {
        call(1, 0x0)
    };
    TFunction sub;
    sub.bytecode = {
        func(4),
        set(0x7, 0),
        set(0x8, 6),
        ranged_ascending(0x7, 0x8),
        copy(0xA, 0x5),
        set(0x9, 0),
        ranged_ascending(0x9, 0xA),
        add(0x3, 0xB, 0xB),
        ranged_ascending_end(),
        ranged_ascending_end(),
        ret(),
    };
    p.functions = {main, sub};
    assert(ResolveProgram(p));
    auto state = executeProgram(p);
    std::cout << state.stack[0xA] << std::endl;
    std::flush(std::cout);
    assert(state.stack[0xB] == 20);
}

void test_empty_loop() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction main;
    main.bytecode = {
        call(1, 0x0)
    };
    TFunction sub;
    sub.bytecode = {
        func(2),
        set(0x6, 2),
        set(0x7, 1),
        set(0x8, 1),
        ranged_ascending(0x6, 0x7),
        set(0x8, 0),
        ranged_ascending_end(),
        ret(),
    };
    p.functions = {main, sub};
    assert(ResolveProgram(p));
    auto state = executeProgram(p);
    assert(state.stack[0x6] == 2);
    assert(state.stack[0x7] == 1);
    assert(state.stack[0x8] == 1);
}

void test_nested_functions() {
    using namespace NVirtualMachine::NCore;
    const uint64_t X = 0x3;

    const uint64_t A = X + 0x6;
    const uint64_t B = X + 0x6 + 1;
    const uint64_t R = B + 1 - 0x4;
    const uint64_t C = B + 2 - 0x4;
    TProgram p;
    TFunction main;
    main.bytecode = {
        set(X, 7),
        call(1, 0x0)
    };
    TFunction sqr;
    sqr.bytecode = {
        func(0),
        copy(A, X),
        copy(B, X),
        call(2, 0x4),
        copy(A, R + 0x4),
        ret()
    };

    TFunction multiply;
    multiply.bytecode = {
        func(2),
        set(R, 0),
        set(C, 0),
        ranged_ascending(C, B - 0x4),
        add(A - 0x4, R, R),
        ranged_ascending_end(),
        ret(),
    };
    p.functions = {main, sqr, multiply};
    assert(ResolveProgram(p));
    auto state = executeProgram(p);
    assert(state.stack[A] == 49);
}

void test_references_w_functions() {
    using namespace NVirtualMachine::NCore;
    TProgram p;
    TFunction main;
    main.bytecode = {
        set(0x1, 123),
        ref(0x2 + 0x3, 1),
        call(1, 0x2)
    };
    TFunction set;
    set.bytecode = {
        func(0),
        getref(0x4, 0x3),
        add(0x4, 0x4, 0x4),
        setref(0x3, 0x4),
        ret()
    };
    p.functions = {main, set};
    assert(ResolveProgram(p));
    auto state = executeProgram(p);
    assert(state.stack[1] == 246);
}

void test_instructions() {
    #define RUNTEST(x) std::cout << #x << " "; std::flush(std::cout); x(); std::cout << "OK" << std::endl;;
    RUNTEST(test_set);
    RUNTEST(test_set_copy);
    RUNTEST(test_add);
    RUNTEST(test_sub);
    RUNTEST(test_branching);
    RUNTEST(test_loop);
    RUNTEST(test_empty_loop);
    RUNTEST(test_nested_functions);
    RUNTEST(test_references_w_functions);
    #undef RUNTEST
}
