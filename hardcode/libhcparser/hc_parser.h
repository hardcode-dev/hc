#pragma once

#include "hc_syn.h"
#include "hc_object_stream.h"

#include <stdio.h>


#define HC_PARSER_STRUCT 1
#define HC_PARSER_BINDING 2
#define HC_PARSER_ARRAY 3
#define HC_PARSER_FUNCTION_RETURN_TYPE 4
#define HC_PARSER_FUNCTION_PARAMETERS 5
#define HC_PARSER_FUNCTION_BODY 6
#define HC_PARSER_CLASS 7
#define HC_PARSER_FUNCTION_KIND 8

typedef struct hc_parser_instruction_t {
    int type;
    int source_off;
    int source_size;
    const hc_syn_insn_value_t *value;
} hc_parser_instruction_t;

typedef struct hc_parser_error_t {
    int source_off;
    int source_size;
    const char *message;
} hc_parser_error_t;


extern int hc_parse (
    FILE *fh,
    int target,
    hc_object_stream_t *instruction_stream,
    hc_object_stream_t *error_stream);

extern int hc_parse_buffer (
    const void *data,
    size_t len,
    int target,
    hc_object_stream_t *instruction_stream,
    hc_object_stream_t *error_stream);
