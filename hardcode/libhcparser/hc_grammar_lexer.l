%option prefix="hc_grammar"
%option outfile="hc_grammar_lexer.c"
%option bison-bridge
%option reentrant
%option noyywrap
%option nounput
%option nodefault
%option warn
%{
/* So much for reentrancy+integration with bison */
#define YYADDUCASEPREFIX(v) HC_GRAMMAR ## v
#define YYADDLCASEPREFIX(v) hc_grammar ## v

#define YYSTYPE YYADDUCASEPREFIX (STYPE)

#define YY_USER_INIT							\
     if (!yyg->yy_start)						\
	 yyg->yy_start = 1;						\
									\
     if (!yyin)								\
	 yyin = stdin;							\
									\
     if (!yyout)							\
	 yyout = stdout;						\
									\
     if (!YY_CURRENT_BUFFER) {						\
	 YYADDLCASEPREFIX (ensure_buffer_stack) (yyscanner);		\
	 YY_CURRENT_BUFFER_LVALUE =					\
	     YYADDLCASEPREFIX (_create_buffer) (yyin, YY_BUF_SIZE, yyscanner); \
     }									\
									\
     YYADDLCASEPREFIX (_load_buffer_state) (yyscanner);			\
     return yyextra->initial_token;

#include "hc_parser.h"
#include "hc_grammar.h"
#include "hc_grammar_parser.h"
#include "hc_syn.h"
%}
%option extra-type="parser_extra_t*"

O     [0-7]
D     [0-9]
NZ    [1-9]
L     [a-zA-Z_]
A     [a-zA-Z_0-9]
H     [a-fA-F0-9]
HP    (0[xX])
E     ([Ee][+-]?{D}+)
P     ([Pp][+-]?{D}+)
FS    (f|F|l|L)
IS    (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP    (u|U|L)
SP    (u8|u|U|L)
ES    (\\([\'\"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS    [ \n]
TAB   \t
VTAB  \v
CR    \r
FF    \f

%{
#include <stdio.h>

#define YY_USER_ACTION do {						\
	yyget_extra (yyscanner)->token_off += yyextra->token_size;	\
	yyget_extra (yyscanner)->token_size = yyleng;			\
    } while (0);

static inline int comment (yyscan_t yyscanner);
static inline int unescape_string_literal (hc_syn_string_t *val, const char *s, int len);
static inline void dup_identifier (hc_syn_string_t *val, const char *s, int len);
%}

%%
"/*"                                    { if (comment (yyscanner)) return UNTERMINATED_COMMENT; }
"//".*                                  { }

"void"                                  { return TYPENAME_VOID; }
"bool"                                  { return TYPENAME_BOOL; }
"word"                                  { return TYPENAME_WORD; }
"byte"                                  { return TYPENAME_BYTE; }
"short"                                 { return TYPENAME_SHORT; }
"int"                                   { return TYPENAME_INT; }
"long"                                  { return TYPENAME_LONG; }
"huge"                                  { return TYPENAME_HUGE; }
"uword"                                 { return TYPENAME_UWORD; }
"ubyte"                                 { return TYPENAME_UBYTE; }
"ushort"                                { return TYPENAME_USHORT; }
"uint"                                  { return TYPENAME_UINT; }
"ulong"                                 { return TYPENAME_ULONG; }
"uhuge"                                 { return TYPENAME_UHUGE; }

"half"                                  { return TYPENAME_HALF; }
"float"                                 { return TYPENAME_FLOAT; }
"double"                                { return TYPENAME_DOUBLE; }
"quad"                                  { return TYPENAME_QUAD; }

"string"                                { return TYPENAME_STRING; }

"struct"                                { return TYPENAME_STRUCT; }
"union"                                 { return TYPENAME_UNION; }

"class"                                 { return TYPENAME_CLASS; }
"trait"                                 { return TYPENAME_TRAIT; }
"object"                                { return TYPENAME_OBJECT; }

"function"                              { return KIND_FUNCTION; }
"constructor"                           { return KIND_CONSTRUCTOR; }
"method"                                { return KIND_METHOD; }

"this"                                  { return THIS; }

"mut"                                   { return VARIABLE_QUALIFIER_MUT; }
"in"                                    { return VARIABLE_QUALIFIER_IN; }
"out"                                   { return VARIABLE_QUALIFIER_OUT; }
"inout"                                 { return VARIABLE_QUALIFIER_INOUT; }

"sizeof"				{ return OPERATOR_SIZEOF; }
"extend"				{ return OPERATOR_EXTEND; }

"return"				{ return CONTROL_RETURN; }

"if"					{ return CONTROL_IF; }
"let"					{ return CONTROL_LET; }
"else"					{ return CONTROL_ELSE; }

"switch"				{ return CONTROL_SWITCH; }
"case"					{ return CONTROL_CASE; }
"default"				{ return CONTROL_DEFAULT; }

"goto"					{ return CONTROL_GOTO; }

"loop"                  { return CONTROL_LOOP; }
"for"					{ return CONTROL_FOR; }
"break"					{ return CONTROL_BREAK; }
"continue"				{ return CONTROL_CONTINUE; }

"index"                 { return INDEX_DECLARATOR; }

"indexed"               { return INDEXED_CONSTRAINT_KEYWORD; }
"ranged"                { return RANGED_CONSTRAINT_KEYWORD; }
"constrained"           { return CONSTRAINED_CONSTRAINT_KEYWORD; }

"asc"                   { return ASCENDING_FOR_CONTROL; }
"desc"                  { return DESCENDING_FOR_CONTROL; } 

"#"                     { return LEN_OPERATOR; }

{L}{A}*					{ dup_identifier ((hc_syn_string_t *) &yylval->_string, yytext, yyleng); return PLAIN_IDENTIFIER; }

{HP}{H}+{IS}?				{ yylval->_long = strtoll (yytext, NULL, 10); return INTEGER_LITERAL; }
{NZ}{D}*{IS}?				{ yylval->_long = strtoll (yytext, NULL, 10); return INTEGER_LITERAL; }
"0"{O}*{IS}?				{ yylval->_long = strtoll (yytext, NULL, 10); return INTEGER_LITERAL; }
{CP}?"\'"([^\'\\\n]|{ES})+"\'"		{ yylval->_long = strtoll (yytext, NULL, 10); return INTEGER_LITERAL; }

{D}+{E}{FS}?				{ yylval->_float = strtof (yytext, NULL); return FLOAT_LITERAL; }
{D}*"."{D}+{E}?{FS}?			{ yylval->_float = strtof (yytext, NULL); return FLOAT_LITERAL; }
{D}+"."{E}?{FS}?			{ yylval->_float = strtof (yytext, NULL); return FLOAT_LITERAL; }
{HP}{H}+{P}{FS}?			{ yylval->_float = strtof (yytext, NULL); return FLOAT_LITERAL; }
{HP}{H}*"."{H}+{P}{FS}?			{ yylval->_float = strtof (yytext, NULL); return FLOAT_LITERAL; }
{HP}{H}+"."{P}{FS}?			{ yylval->_float = strtof (yytext, NULL); return FLOAT_LITERAL; }

{D}+{E}					{ yylval->_double = strtod (yytext, NULL); return DOUBLE_LITERAL; }
{D}*"."{D}+{E}?				{ yylval->_double = strtod (yytext, NULL); return DOUBLE_LITERAL; }
{D}+"."{E}?				{ yylval->_double = strtod (yytext, NULL); return DOUBLE_LITERAL; }
{HP}{H}+{P}				{ yylval->_double = strtod (yytext, NULL); return DOUBLE_LITERAL; }
{HP}{H}*"."{H}+{P}			{ yylval->_double = strtod (yytext, NULL); return DOUBLE_LITERAL; }
{HP}{H}+"."{P}				{ yylval->_double = strtod (yytext, NULL); return DOUBLE_LITERAL; }

\"([^\"\\\n]|{ES})*\"			{ return unescape_string_literal ((hc_syn_string_t *) &yylval->_string, yytext, yyleng) ? INVALID_STRING_LITERAL : STRING_LITERAL; }

"<-"					{ return OPERATOR_STREAM_IN; }
"->"					{ return OPERATOR_STREAM_OUT; }
"<<="					{ return OPERATOR_LEFT_SHIFT_MUTATE; }
">>="					{ return OPERATOR_RIGHT_SHIFT_MUTATE; }
"||="					{ return OPERATOR_OR_MUTATE; }
"&&="					{ return OPERATOR_AND_MUTATE; }
"+="					{ return OPERATOR_ADD_MUTATE; }
"-="					{ return OPERATOR_SUBTRACT_MUTATE; }
"*="					{ return OPERATOR_MULTIPLY_MUTATE; }
"/="					{ return OPERATOR_DIVIDE_MUTATE; }
"%="					{ return OPERATOR_MOD_MUTATE; }
"&="					{ return OPERATOR_BIT_AND_MUTATE; }
"^="					{ return OPERATOR_BIT_XOR_MUTATE; }
"|="					{ return OPERATOR_BIT_OR_MUTATE; }
"<<"					{ return OPERATOR_LEFT_SHIFT; }
">>"					{ return OPERATOR_RIGHT_SHIFT; }
"++"					{ return OPERATOR_INCREMENT; }
"--"					{ return OPERATOR_DECREMENT; }
"&&"					{ return OPERATOR_LOGICAL_AND; }
"||"					{ return OPERATOR_LOGICAL_OR; }
"<="					{ return OPERATOR_LESS_EQUALS; }
">="					{ return OPERATOR_GREATER_EQUALS; }
"=="					{ return OPERATOR_EQUALS; }
"!="					{ return OPERATOR_NOT_EQUALS; }
";"					{ return ';'; }
"{"     				{ return '{'; }
"}"     				{ return '}'; }
","					{ return ','; }
":"					{ return ':'; }
"="					{ return '='; }
":="					{ return OPERATOR_MUTATE; }
":*="               { return OPERATOR_MUTATE_GC_REF; }
"("					{ return '('; }
")"					{ return ')'; }
"["	        			{ return '['; }
"]"      				{ return ']'; }
"."					{ return '.'; }
"&"					{ return '&'; }
"!"					{ return '!'; }
"~"					{ return '~'; }
"-"					{ return '-'; }
"+"					{ return '+'; }
"*"					{ return '*'; }
"/"					{ return '/'; }
"%"					{ return '%'; }
"<"					{ return '<'; }
">"					{ return '>'; }
"^"					{ return '^'; }
"|"					{ return '|'; }
"?"					{ return '?'; }

{WS}+                                   { }
{TAB}                                   { return BAD_WS_TAB; }
{VTAB}                                  { return BAD_WS_VTAB; }
{CR}                                    { return BAD_WS_CR; }
{FF}                                    { return BAD_WS_FORM_FEED; }
.                                       { return INVALID_TOKEN; }
%%


static inline int comment (yyscan_t yyscanner)
{
    int c;
    while ((c = input (yyscanner)) != -1) {
        if (c == '*') {
            while ((c = input (yyscanner)) == '*');
            if (c == '/')
                return 0;
            if (c == -1)
                break;
        }
    }
    return -1;
}
static inline int unescape_string_literal (hc_syn_string_t *val, const char *s, int len)
{
    /* TODO: actually unescape string */
    val->data = strndup (s + 1, len - 2);
    val->len = len - 2;
    return 0;
}
static inline void dup_identifier (hc_syn_string_t *val, const char *s, int len)
{
    val->data = strndup (s, len);
    val->len = len;
}
