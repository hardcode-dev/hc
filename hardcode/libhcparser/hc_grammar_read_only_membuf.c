#define _GNU_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/* NOTE: This implementation instead of using fmemopen() is needed due to automatic free of buffer */

typedef struct membuf_cookie_t {
    const void *data;
    size_t len;
    size_t off;
} membuf_cookie_t;


static ssize_t read (void *cookie, char *buf, size_t size)
{
    if (!size)
	return 0;
    size_t len = ((membuf_cookie_t *) cookie)->len;
    size_t off = ((membuf_cookie_t *) cookie)->off;
    if (off >= len)
	return 0;
    size_t available = len - off;
    size_t to_read = (size > available) ? available : size;
    memcpy (buf, ((membuf_cookie_t *) cookie)->data, to_read);
    ((membuf_cookie_t *) cookie)->off += to_read;
    return to_read;
}
static ssize_t write (void *cookie, const char *buf, size_t size)
{
    (void) cookie;
    (void) buf;
    (void) size;
    return 0;
}
static int seek (void *cookie, off64_t *offset, int whence)
{
    size_t len = ((membuf_cookie_t *) cookie)->len;
    off64_t new_offset = *offset;
    if (whence == SEEK_CUR)
	new_offset += ((membuf_cookie_t *) cookie)->off;
    else if (whence == SEEK_END)
	new_offset += len;
    else if (whence != SEEK_SET)
	return -1;
    if (new_offset < 0 || new_offset > (ssize_t) len)
	return -1;
    *offset = new_offset;
    return 0;
}
static int close (void *cookie)
{
    free (cookie);
    return 0;
}

static cookie_io_functions_t membuf_cookie_io_funcs = {
    .read = read,
    .write = write,
    .seek = seek,
    .close = close,
};


FILE *hc_grammar_fopen_read_only_membuf (const void *data, size_t len)
{
    membuf_cookie_t *cookie = malloc (sizeof (membuf_cookie_t));
    cookie->data = data;
    cookie->len = len;
    cookie->off = 0;
    return fopencookie (cookie, "r", membuf_cookie_io_funcs);
}
