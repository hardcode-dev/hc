#pragma once

#include "hc_parser.h"

typedef struct parser_extra_t {
    int token_off;
    int token_size;
    int nerr;
    int initial_token;
    hc_object_stream_t *instruction_stream;
    hc_object_stream_t *error_stream;
} parser_extra_t;
