%define api.prefix {hc_grammar}
%define parse.error verbose
%define parse.trace
%define api.pure true

%defines
%lex-param {yyscan_t scanner}
%parse-param {yyscan_t scanner}

%{
#include "hc_parser.h"
#include "hc_grammar.h"
#include "hc_syn.h"
#include "hc_grammar_read_only_membuf.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>


#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;

#define YY_EXTRA_TYPE parser_extra_t*


/* So much for reentrancy+integration with lexer */
#define YYADDLCASEPREFIX(v) hc_grammar ## v

#define yyget_extra YYADDLCASEPREFIX (get_extra)
#define yylex_init_extra YYADDLCASEPREFIX (lex_init_extra)
#define yyset_in YYADDLCASEPREFIX (set_in)
#define yylex_destroy YYADDLCASEPREFIX (lex_destroy)

#define EMIT_SYN_INSN_VAL(type_,val) do {                               \
        YY_EXTRA_TYPE extra = yyget_extra (scanner);                    \
        hc_parser_instruction_t parser_instruction = {                  \
            .type = (HC_SYN_INSN_ ## type_),                            \
            .source_off = extra->token_off,                             \
            .source_size = extra->token_size,                           \
            .value = (hc_syn_insn_value_t *) (val),                     \
        };                                                              \
        extra->instruction_stream->callback (extra->instruction_stream->user_data, &parser_instruction); \
    } while (0)

#define EMIT_SYN_INSN(type_) do {                                       \
        YY_EXTRA_TYPE extra = yyget_extra (scanner);                    \
        hc_parser_instruction_t parser_instruction = {                  \
            .type = (HC_SYN_INSN_ ## type_),                            \
            .source_off = extra->token_off,                             \
            .source_size = extra->token_size,                           \
            .value = NULL,                                              \
        };                                                              \
        extra->instruction_stream->callback (extra->instruction_stream->user_data, &parser_instruction); \
    } while (0)
%}

%union { // TODO: Rational literals
    intptr_t _word;
    uintptr_t _uword;
    int8_t _byte;
    uint8_t _ubyte;
    int16_t _short;
    uint16_t _ushort;
    int32_t _int;
    uint32_t _uint;
    int64_t _long;
    uint64_t _ulong;
    float _float;
    double _double;
    double _quad;
    hc_syn_string_t _string;
}

%{
extern int yylex_init_extra (YY_EXTRA_TYPE user_defined, yyscan_t *scanner);
extern int yylex_destroy (yyscan_t yyscanner);
extern void yyset_in (FILE *_in_str, yyscan_t yyscanner);
extern YY_EXTRA_TYPE yyget_extra (yyscan_t yyscanner);
extern int yylex (YYSTYPE *yylval_param, yyscan_t yyscanner);

extern void yyerror (yyscan_t yyscanner, const char *message);
%}

%token INVALID_TOKEN "invalid token"
%token BAD_WS_TAB "bad whitespace tab ('\t')"
%token BAD_WS_VTAB "bad whitespace vertical tab ('\v')"
%token BAD_WS_CR "bad whitespace carriage return ('\r')"
%token BAD_WS_FORM_FEED "bad whitespace form feed ('\f')"
%token UNTERMINATED_COMMENT "unterminated comment"
%token INVALID_STRING_LITERAL "invalid string literal"

%token <_long> INTEGER_LITERAL "integer number"
%token <_float> FLOAT_LITERAL "float literal"
%token <_double> DOUBLE_LITERAL "double literal"
%token <_quad> QUAD_LITERAL "quad literal"
%token <_string> STRING_LITERAL "string literal"
%token <_string> PLAIN_IDENTIFIER "plain identifier"

%token TYPENAME_VOID "void"
%token TYPENAME_BOOL "bool"
%token TYPENAME_WORD "word"
%token TYPENAME_BYTE "byte"
%token TYPENAME_SHORT "short"
%token TYPENAME_INT "int"
%token TYPENAME_LONG "long"
%token TYPENAME_HUGE "huge"
%token TYPENAME_UWORD "uword"
%token TYPENAME_UBYTE "ubyte"
%token TYPENAME_USHORT "ushort"
%token TYPENAME_UINT "uint"
%token TYPENAME_ULONG "ulong"
%token TYPENAME_UHUGE "uhuge"

%token TYPENAME_HALF "half"
%token TYPENAME_FLOAT "float"
%token TYPENAME_DOUBLE "double"
%token TYPENAME_QUAD "quad"

%token TYPENAME_STRING "string"

%token TYPENAME_STRUCT "struct"
%token TYPENAME_UNION "union"

%token TYPENAME_CLASS "class"
%token TYPENAME_TRAIT "trait"
%token TYPENAME_OBJECT "object"

%token THIS "this"

%token VARIABLE_QUALIFIER_MUT "mut"
%token VARIABLE_QUALIFIER_IN "in"
%token VARIABLE_QUALIFIER_OUT "out"
%token VARIABLE_QUALIFIER_INOUT "inout"

%token CONTROL_IF "if"
%token CONTROL_ELSE "else"
%token CONTROL_LET "let"

%token CONTROL_SWITCH "switch"
%token CONTROL_CASE "case"
%token CONTROL_DEFAULT "default"

%token CONTROL_LOOP "loop"
%token CONTROL_FOR "for"

%token CONTROL_RETURN "return"
%token CONTROL_GOTO "goto"
%token CONTROL_BREAK "break"
%token CONTROL_CONTINUE "continue"

%token OPERATOR_SIZEOF "sizeof"

%token INDEX_DECLARATOR "index"

%token INDEXED_CONSTRAINT_KEYWORD "indexed"
%token RANGED_CONSTRAINT_KEYWORD "ranged"
%token CONSTRAINED_CONSTRAINT_KEYWORD "constrained"

%token ASCENDING_FOR_CONTROL "asc"
%token DESCENDING_FOR_CONTROL "desc"

%token LEN_OPERATOR "#"

%token '=' "="
%token OPERATOR_MUTATE ":="
%token OPERATOR_MUTATE_GC_REF ":*="
%token OPERATOR_STREAM_IN "<-"
%token OPERATOR_STREAM_OUT "->"
%token OPERATOR_LEFT_SHIFT_MUTATE "<<="
%token OPERATOR_RIGHT_SHIFT_MUTATE ">>="
%token OPERATOR_AND_MUTATE "&&="
%token OPERATOR_OR_MUTATE "||="
%token OPERATOR_ADD_MUTATE "+="
%token OPERATOR_SUBTRACT_MUTATE "-="
%token OPERATOR_MULTIPLY_MUTATE "*="
%token OPERATOR_DIVIDE_MUTATE "/="
%token OPERATOR_MOD_MUTATE "%="
%token OPERATOR_BIT_AND_MUTATE "&="
%token OPERATOR_BIT_XOR_MUTATE "^="
%token OPERATOR_BIT_OR_MUTATE "|="
%token OPERATOR_EXTEND "extend"

%token OPERATOR_LEFT_SHIFT "<<"
%token OPERATOR_RIGHT_SHIFT ">>"
%token '+' "+"
%token '-' "-"
%token '*' "*"
%token '/' "/"
%token '%' "%"
%token OPERATOR_INCREMENT "++"
%token OPERATOR_DECREMENT "--"
%token OPERATOR_LOGICAL_AND "&&"
%token OPERATOR_LOGICAL_OR "||"
%token '<' "<"
%token '>' ">"
%token OPERATOR_LESS_EQUALS "<="
%token OPERATOR_GREATER_EQUALS ">="
%token OPERATOR_EQUALS "=="
%token OPERATOR_NOT_EQUALS "!="
%token OPERATOR_LOGICAL_INVERSE "!"

%token KIND_FUNCTION "function"
%token KIND_CONSTRUCTOR "constructor"
%token KIND_METHOD "method"

%token '&' "&"
%token '^' "^"
%token '|' "|"
%token '~' "~"

%token '?' "?"
%token ':' ":"

%token ';' ";"
%token ',' ","
%token '.' "."

%token '{' "{"
%token '}' "}"
%token '(' "("
%token ')' ")"
%token '[' "["
%token ']' "]"

// Synthetic tokens for solving the shift-reduce conflicts

%token NO_ELSE

%token FOR_RANGE
%token FOR_IDENT

// Associativity (most of it is not needed because of the matryoshkas)
// (higher == less priority)

%right ":="
%right ":*="
%right "="
%right "+="
%right "-="
%right "*="
%right "/="
%right "%="

%left "+"
%left "-"
%left "*"
%left "/"

%left "=="
%left "!="

// The associativity part that actually does something

%nonassoc NO_ELSE
%nonassoc "else"

%nonassoc FOR_IDENT
%nonassoc FOR_RANGE

%left EMPTY_LIST
%left SINGLE_ITEM_LIST
%left FULL_LIST


%token ENTRY_TOKEN_FUNCTION_RETURN_TYPE "function return type section"
%token ENTRY_TOKEN_FUNCTION_PARAMETERS "function parameters section"
%token ENTRY_TOKEN_FUNCTION_BODY "function body section"
%token ENTRY_TOKEN_STRUCT "struct section"
%token ENTRY_TOKEN_BINDING "binding section"
%token ENTRY_TOKEN_ARRAY "array section"
%token ENTRY_TOKEN_CLASS "class section"
%token ENTRY_TOKEN_FUNCTION_KIND "function kind"

%glr-parser

%start entry_point
%%

entry_point:
    ENTRY_TOKEN_STRUCT not_implemented 
    | ENTRY_TOKEN_BINDING not_implemented
    | ENTRY_TOKEN_ARRAY not_implemented
    | ENTRY_TOKEN_FUNCTION_KIND function_kind
    | ENTRY_TOKEN_FUNCTION_RETURN_TYPE function_return_type
    | ENTRY_TOKEN_FUNCTION_PARAMETERS function_parameters
    | ENTRY_TOKEN_FUNCTION_BODY function_body
    | ENTRY_TOKEN_CLASS class
    ;

not_implemented:
    %empty
    ;

function_kind:
    "function" { EMIT_SYN_INSN(KIND_FUNCTION); }
    | "constructor" { EMIT_SYN_INSN(KIND_CONSTRUCTOR); }
    | "method" { EMIT_SYN_INSN(KIND_METHOD); }
    ;

function_return_type:
    type_specification
    ;

plain_identifier:
    PLAIN_IDENTIFIER { EMIT_SYN_INSN_VAL (PLAIN_IDENTIFIER, &$1); }
    ;

nested_identifier:
    plain_identifier 
    | nested_identifier "." plain_identifier { EMIT_SYN_INSN (EXTEND_IDENTIFIER); }
    ;

type_qualifier:
    "mut" { EMIT_SYN_INSN(MUT_QUALIFIER); }
    | "in" { EMIT_SYN_INSN(IN_QUALIFIER); }
    | "out" { EMIT_SYN_INSN(OUT_QUALIFIER); }
    | "inout" { EMIT_SYN_INSN(INOUT_QUALIFIER); }
    ;

reference_qualifier:
    "[" "]" { EMIT_SYN_INSN (REFERENCE_ARRAY); }
    | "[" literal_integer "]" { EMIT_SYN_INSN (REFERENCE_STATIC_ARRAY); }
    | "&" { EMIT_SYN_INSN (REFERENCE_LOCAL); }
    | "*" { EMIT_SYN_INSN (REFERENCE_GC); }
    | "@" { EMIT_SYN_INSN (REFERENCE_FOREST); }
    ;

reference_qualifier_list:
    reference_qualifier
    | reference_qualifier_list reference_qualifier { EMIT_SYN_INSN (STAPLE_REFERENCE); }
    ;

unqualified_type:
    type_specifier reference_qualifier_list
    | type_specifier
    ;

type_specification:
    type_qualifier unqualified_type ":" constraint_annotation
    | unqualified_type ":" constraint_annotation
    | type_qualifier unqualified_type
    | unqualified_type
    ;

function_parameter:
    type_specification nested_identifier
;

function_parameters:
    %empty |
    function_parameter |
    function_parameters "," function_parameter
;

literal_integer:
    INTEGER_LITERAL { EMIT_SYN_INSN_VAL (LITERAL_INTEGER, &$1); }
;

literal_float:
    FLOAT_LITERAL { EMIT_SYN_INSN_VAL (LITERAL_FLOAT, &$1); }
;

literal_double:
    DOUBLE_LITERAL { EMIT_SYN_INSN_VAL (LITERAL_DOUBLE, &$1); }
;

literal_quad:
    QUAD_LITERAL { EMIT_SYN_INSN_VAL (LITERAL_QUAD, &$1); }
;

integral_literal:
    literal_integer
    ;

literal_string:
    STRING_LITERAL { EMIT_SYN_INSN_VAL (LITERAL_STRING, &$1); }
    ;

literal:
    integral_literal
    | literal_float 
    | literal_double 
    | literal_quad 
    | literal_string
    ;

initializer_expression:
    "(" argument_expression_list ")" { EMIT_SYN_INSN(CLASS_OBJECT_INITIALIZER); }
    ;

primary_expression:
    literal
    | plain_identifier
    | initializer_expression
    | THIS { EMIT_SYN_INSN(THIS_ACCESS); }
    | block { EMIT_SYN_INSN (BLOCK_EXPRESSION); }
    | if_construct { EMIT_SYN_INSN (IF_EXPRESSION); }
    | "#" plain_identifier { EMIT_SYN_INSN (LEN_EXPRESSION); }
    | "let" "(" let_declaration_list ")" { EMIT_SYN_INSN (LET_EXPRESSION); }
    ;

prefix_expression:
    primary_expression
    | "+" prefix_expression { EMIT_SYN_INSN(UNARY_PLUS); }
    | "-" prefix_expression { EMIT_SYN_INSN(UNARY_MINUS); }
    | "++" prefix_expression { EMIT_SYN_INSN(PREFIX_INCREMENT); }
    | "--" prefix_expression { EMIT_SYN_INSN(PREFIX_DECREMENT); }
    | type_specification "(" prefix_expression ")" { EMIT_SYN_INSN(CAST); }
    ;

postfix_expression:
    prefix_expression 
    | postfix_expression "[" expression "]" { EMIT_SYN_INSN (ARRAY_ELEMENT_ACCESS); } 
    | postfix_expression "(" argument_expression_list ")" { EMIT_SYN_INSN (FUNCTION_CALL); } 
    | postfix_expression "." PLAIN_IDENTIFIER { EMIT_SYN_INSN_VAL (FIELD_ACCESS, &$3); }
    | postfix_expression "++" { EMIT_SYN_INSN(POSTFIX_INCREMENT); }
    | postfix_expression "--" { EMIT_SYN_INSN(POSTFIX_DECREMENT); }
    ;

argument_expression_list:
    %empty { EMIT_SYN_INSN (EMPTY_EXPRESSION); EMIT_SYN_INSN (FUNCTION_ARGUMENT); } 
    | expression { EMIT_SYN_INSN (FUNCTION_ARGUMENT); } 
    | argument_expression_list "," expression { EMIT_SYN_INSN (STAPLE_FUNCTION_ARGUMENT); }
    ;

multiplicative_expression:
    postfix_expression 
    | multiplicative_expression "*" postfix_expression { EMIT_SYN_INSN (MULTIPLICATION); } 
    | multiplicative_expression "/" postfix_expression { EMIT_SYN_INSN (DIVISION); }
    | multiplicative_expression "%" postfix_expression { EMIT_SYN_INSN (REMAINDER); }
    ;

additive_expression:
    multiplicative_expression 
    | additive_expression "+" multiplicative_expression { EMIT_SYN_INSN (ADDITION); } 
    | additive_expression "-" multiplicative_expression { EMIT_SYN_INSN (SUBTRACTION); }
    ;

comparative_expression:
    additive_expression
    | comparative_expression "<" additive_expression { EMIT_SYN_INSN (LESSER_COMPARE); } 
    | comparative_expression ">" additive_expression { EMIT_SYN_INSN (GREATER_COMPARE); }
    | comparative_expression "<=" additive_expression { EMIT_SYN_INSN (LESSER_EQ_COMPARE); }
    | comparative_expression ">=" additive_expression { EMIT_SYN_INSN (GREATER_EQ_COMPARE); }
    ;

eq_neq_expression:
    comparative_expression
    | eq_neq_expression "==" comparative_expression { EMIT_SYN_INSN (EQUALITY); } 
    | eq_neq_expression "!=" comparative_expression { EMIT_SYN_INSN (NON_EQUALITY); }
    ;

logical_and_expr:
    eq_neq_expression
    | logical_and_expr "&&" eq_neq_expression { EMIT_SYN_INSN(LOGICAL_AND); }
    ;

logical_or_expr:
    logical_and_expr
    | logical_or_expr "||" logical_and_expr { EMIT_SYN_INSN(LOGICAL_OR); }
    ;

assignment_expression:
    logical_or_expr 
    | assignment_expression "=" logical_or_expr { EMIT_SYN_INSN (INITIALIZE); }
    | assignment_expression ":=" logical_or_expr { EMIT_SYN_INSN (MUTATE); }
    | assignment_expression ":*=" logical_or_expr { EMIT_SYN_INSN(MUTATE_GC_REF); }
    ;

mutate_multiplicative:
    assignment_expression
    | mutate_multiplicative "*=" assignment_expression { EMIT_SYN_INSN (MUTATE_MUL); }
    | mutate_multiplicative "/=" assignment_expression { EMIT_SYN_INSN (MUTATE_DIV); }
    ;

mutate_additive:
    mutate_multiplicative 
    | mutate_additive "+=" mutate_multiplicative { EMIT_SYN_INSN (MUTATE_ADD); }
    | mutate_additive "-=" mutate_multiplicative { EMIT_SYN_INSN (MUTATE_SUB); }
    ;

expression:
    mutate_additive
    ;

expression_statement:
    expression ";" { EMIT_SYN_INSN (EXPRESSION_STATEMENT); }
    ;

asc_desc_key:
    "asc" { EMIT_SYN_INSN(ASCENDING); }
    | "desc" { EMIT_SYN_INSN(DESCENDING); }
    ;

for_iteration_clause:
    declaration asc_desc_key nested_identifier { EMIT_SYN_INSN(FOR_ITERATION_CLAUSE_ITERABLE); }
    | declaration asc_desc_key expression "," expression { EMIT_SYN_INSN(FOR_ITERATION_CLAUSE_RANGE); }
    ;

block:
    "{" block_item_list "}" { EMIT_SYN_INSN(BLOCK_END); }
    ;

for_statement:
    "for" "(" for_iteration_clause ")" block_item
    ;

loop_statement:
    "loop" block { EMIT_SYN_INSN(LOOP_END); }
    ;

if_executable:
    block_item { EMIT_SYN_INSN(IF_EXECUTABLE_END); }
    ;

if_construct:
    "if" "(" expression ")" if_executable { EMIT_SYN_INSN(IF_END); } %prec NO_ELSE
    | "if" "(" expression ")" if_executable "else" if_executable { EMIT_SYN_INSN(IF_WITH_ELSE_END); }
    ;

break_statement:
    "break" ";"
    ;

continue_statement:
    "continue" ";"
    ;

return_statement:
    "return" expression ";"
    ;

control_flow_statement:
    for_statement { EMIT_SYN_INSN (CONTROL_FOR); }
    | loop_statement { EMIT_SYN_INSN (CONTROL_LOOP); }
    | if_construct { EMIT_SYN_INSN (CONTROL_IF); }
    | break_statement { EMIT_SYN_INSN (CONTROL_BREAK); }
    | continue_statement { EMIT_SYN_INSN (CONTROL_CONTINUE); }
    | return_statement { EMIT_SYN_INSN (CONTROL_RETURN); }
    ;

let_declaration_list:
    declaration { EMIT_SYN_INSN(LET_DECLARATION); }
    | let_declaration_list "&&" declaration { EMIT_SYN_INSN(LET_DECLARATION); }
    ;

let_executable:
    block_item { EMIT_SYN_INSN (LET_EXECUTABLE); }
    ;

let_statement:
    "let" "(" let_declaration_list ")" let_executable %prec NO_ELSE
    | "let" "(" let_declaration_list ")" let_executable "else" let_executable
    ;

declarator:
    plain_identifier { EMIT_SYN_INSN (DECLARATION); }
    | plain_identifier "=" expression { EMIT_SYN_INSN (DECLARATION_WTIH_INIT); }
    ;

declarator_list:
    declarator 
    | declarator_list "," declarator

declaration:
    type_specification declarator_list
    | "index" "(" plain_identifier ")" declarator_list { EMIT_SYN_INSN (INDEX_DECLARATION); }
    | "index" declarator_list { EMIT_SYN_INSN (INDEX_IMPLICIT_DECLARATION); }
    ;

declaration_statement:
    declaration ";"
    ;

block_item:
    expression_statement
    | control_flow_statement
    | declaration_statement { EMIT_SYN_INSN(DECLARATION_STATEMENT_END); }
    | let_statement { EMIT_SYN_INSN(LET_STATEMENT_END); }
    | block { EMIT_SYN_INSN (BLOCK_STATEMENT_END); }
    ;

block_item_list:
    %empty
    | block_item_list block_item { EMIT_SYN_INSN (STAPLE_BLOCK_ITEMS); }
    ;

function_body:
    block_item_list
    ;

scalar_type_specifier:
    "void" { EMIT_SYN_INSN (TYPENAME_VOID); }
    | "bool" { EMIT_SYN_INSN (TYPENAME_BOOL); }
    | "word" { EMIT_SYN_INSN (TYPENAME_WORD); }
    | "byte" { EMIT_SYN_INSN (TYPENAME_BYTE); }
    | "short" { EMIT_SYN_INSN (TYPENAME_SHORT); }
    | "int" { EMIT_SYN_INSN (TYPENAME_INT); } 
    | "long" { EMIT_SYN_INSN (TYPENAME_LONG); }
    | "huge" { EMIT_SYN_INSN (TYPENAME_HUGE); }
    | "uword" { EMIT_SYN_INSN (TYPENAME_UWORD); }
    | "ubyte" { EMIT_SYN_INSN (TYPENAME_UBYTE); }
    | "ushort" { EMIT_SYN_INSN (TYPENAME_USHORT); }
    | "uint" { EMIT_SYN_INSN (TYPENAME_UINT); }
    | "ulong" { EMIT_SYN_INSN (TYPENAME_ULONG); }
    | "uhuge" { EMIT_SYN_INSN (TYPENAME_UHUGE); }
    | "half" { EMIT_SYN_INSN (TYPENAME_HALF); }
    | "float" { EMIT_SYN_INSN (TYPENAME_FLOAT); }
    | "double" { EMIT_SYN_INSN (TYPENAME_DOUBLE); }
    | "quad" { EMIT_SYN_INSN (TYPENAME_QUAD); }
    | "string" { EMIT_SYN_INSN (TYPENAME_STRING); }
    ;

type_kind_specifier:
    "class"
    ;

named_type_specifier:
    type_kind_specifier nested_identifier
    ;

type_specifier:
    scalar_type_specifier
    | named_type_specifier
    ;

// constraining_expression:
//     expression "<" expression
//     | expression "<=" expression
//     | expression ">" expression
//     | expression ">=" expression
//     | expression "==" expression
//     | expression "!=" expression
//     ;

constraint_annotation:
    "ranged" { EMIT_SYN_INSN(RANGED_FOR_CONSTRAINT); }
    | "ranged" "(" expression "," expression ")" { EMIT_SYN_INSN(RANGED_CONSTRAINT); }
    | "constrained" "(" expression ")" { EMIT_SYN_INSN(CONSTRAINED_CONSTRAINT); }
    ;

class:
    %empty
    | class declaration ";" { EMIT_SYN_INSN(IN_CLASS_DECLARATION); }
    ;

%%


int hc_parse (FILE *fh, int target, hc_object_stream_t *instruction_stream, hc_object_stream_t *error_stream)
{
    int initial_token;
    switch (target) {
    case HC_PARSER_STRUCT:
    	initial_token = ENTRY_TOKEN_STRUCT;
    	break;
    case HC_PARSER_BINDING:
    	initial_token = ENTRY_TOKEN_BINDING;
    	break;
    case HC_PARSER_ARRAY:
    	initial_token = ENTRY_TOKEN_ARRAY;
    	break;
    case HC_PARSER_FUNCTION_RETURN_TYPE:
        initial_token = ENTRY_TOKEN_FUNCTION_RETURN_TYPE;
        break;
    case HC_PARSER_FUNCTION_PARAMETERS:
    	initial_token = ENTRY_TOKEN_FUNCTION_PARAMETERS;
    	break;
    case HC_PARSER_FUNCTION_BODY:
    	initial_token = ENTRY_TOKEN_FUNCTION_BODY;
    	break;
    case HC_PARSER_FUNCTION_KIND:
        initial_token = ENTRY_TOKEN_FUNCTION_KIND;
        break;
    case HC_PARSER_CLASS:
        initial_token = ENTRY_TOKEN_CLASS;
        break;
    default:
	return 0;
    }

    yyscan_t scanner;
    parser_extra_t extra = {
        .token_off = 0,
        .token_size = 0,
        .nerr = 0,
        .initial_token = initial_token,
        .instruction_stream = instruction_stream,
        .error_stream = error_stream,
    };
    if (yylex_init_extra (&extra, &scanner)) {
        fprintf (stderr, "Failed to init lexer\n");
        return 0;
    }
    yyset_in (fh, scanner);
    yydebug = 0;
    do {
        yyparse (scanner);
    } while (!feof (fh) && !extra.nerr);

    yylex_destroy (scanner);

    return !extra.nerr;
}

int hc_parse_buffer (const void *data, size_t len, int target, hc_object_stream_t *instruction_stream, hc_object_stream_t *error_stream)
{
    FILE *fh = hc_grammar_fopen_read_only_membuf ((void*) data, len);
    if (!fh) {
	fprintf (stderr, "Failed to open memory (%p, %d) as file: %s\n", data, (int) len, strerror (errno));
	return 0;
    }

    int ret = hc_parse (fh, target, instruction_stream, error_stream);
    fclose (fh);

    return ret;
}

void yyerror (yyscan_t scanner, const char *message)
{
    YY_EXTRA_TYPE extra = yyget_extra (scanner);
    hc_parser_error_t parser_error = {
        .source_off = extra->token_off,
        .source_size = extra->token_size,
        .message = message,
    };
    extra->error_stream->callback (extra->error_stream->user_data, &parser_error);
    ++(extra->nerr);
}
