#pragma once

#include <stdio.h>


extern FILE *hc_grammar_fopen_read_only_membuf (
    const void *data,
    size_t len);
