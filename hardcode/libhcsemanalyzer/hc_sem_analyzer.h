#pragma once

#include "hc_sem.h"
#include "hc_syn.h"


#define HC_SEM_ANALYZER_FUNCTION_RETURN_TYPE 1
#define HC_SEM_ANALYZER_FUNCTION_PARAMETERS 2
#define HC_SEM_ANALYZER_FUNCTION_BODY 3
#define HC_SEM_ANALYZER_STRUCT 4
#define HC_SEM_ANALYZER_BINDING 5
#define HC_SEM_ANALYZER_ARRAY 6
#define HC_SEM_ANALYZER_CLASS 7
#define HC_SEM_ANALYZER_FUNCTION_KIND 8

typedef const hc_syn_insn_t *(*hc_sem_analyzer_scan_syn_instruction_t) (
    void *user_data);

typedef void (*hc_sem_analyzer_emit_instruction_t) (
    void *user_data,
    int type,
    int source_off,
    int source_size,
    const hc_sem_insn_value_t *value);

typedef void (*hc_sem_analyzer_emit_error_t) (
    void *user_data,
    int source_off,
    int source_size,
    const char *message);


extern int hc_sem_analyze (
    int target,
    void *scan_user_data,
    hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
    void *emit_user_data,
    hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
    void *error_user_data,
    hc_sem_analyzer_emit_error_t error_callback);
