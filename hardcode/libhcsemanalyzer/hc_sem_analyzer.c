#include "hc_sem_analyzer.h"
#include "hc_sem.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
  TODO:
  Variable stack:
  A structure like: 'param "a", param "b", variable "x", variable "y", variable "a", <scope open>, variable "m", variable "n", variable "b"

  // add_variable ()
  // open_scope ()
  // close_scope ()

 */

#define HC_SEM_INSN_TYPE_VOID 0
#define HC_SEM_INSN_TYPE_STRING 1
#define HC_SEM_INSN_TYPE_WORD 2
#define HC_SEM_INSN_TYPE_UWORD 3
#define HC_SEM_INSN_TYPE_BYTE 4
#define HC_SEM_INSN_TYPE_UBYTE 5
#define HC_SEM_INSN_TYPE_SHORT 6
#define HC_SEM_INSN_TYPE_USHORT 7
#define HC_SEM_INSN_TYPE_INT 8
#define HC_SEM_INSN_TYPE_UINT 9
#define HC_SEM_INSN_TYPE_LONG 10
#define HC_SEM_INSN_TYPE_ULONG 11
#define HC_SEM_INSN_TYPE_HUGE 12
#define HC_SEM_INSN_TYPE_UHUGE 13
#define HC_SEM_INSN_TYPE_HALF 14
#define HC_SEM_INSN_TYPE_FLOAT 15
#define HC_SEM_INSN_TYPE_DOUBLE 16
#define HC_SEM_INSN_TYPE_QUAD 17


typedef enum hc_variable_class_t {
    HC_VARIABLE_TYPE_SCOPE_OPEN,
    HC_VARIABLE_TYPE_VARIABLE,
} hc_variable_class_t;

typedef struct hc_variable_t hc_variable_t;

typedef struct hc_variable_t {
    hc_variable_t *prev;
    hc_variable_class_t _class;
    char name[1];
} hc_variable_t;

void hc_variable_stack_clear (hc_variable_t **s)
{
    hc_variable_t *v = *s;
    while (v) {
        hc_variable_t *v_prev = v->prev;
        free (v);
        v = v_prev;
    }
    *s = v;
}

hc_variable_t *hc_variable_stack_open_scope (hc_variable_t **s)
{
    hc_variable_t *v = malloc (sizeof (hc_variable_t));
    v->prev = *s;
    v->_class = HC_VARIABLE_TYPE_SCOPE_OPEN;
    *(v->name) = '\0';
    *s = v;
    return NULL;
}

void hc_variable_stack_close_scope (hc_variable_t **s)
{
    hc_variable_t *v = *s;
    while (v) {
        int scope_open = v->_class == HC_VARIABLE_TYPE_SCOPE_OPEN;
        hc_variable_t *v_prev = v->prev;
        free (v);
        v = v_prev;

        if (scope_open) break;
    }
    *s = v;
}

hc_variable_t *hc_variable_stack_add_variable (hc_variable_t **s, const char *name)
{
    size_t len = strlen (name);
    hc_variable_t *v = malloc (sizeof (hc_variable_t) + len);
    v->prev = *s;
    v->_class = HC_VARIABLE_TYPE_VARIABLE;
    strcpy (v->name, name);
    *s = v;
    return NULL;
}

hc_variable_t *hc_variable_stack_find_variable (hc_variable_t *s, const char *name)
{
    hc_variable_t *v = s;
    while (v) {
	if (v->_class == HC_VARIABLE_TYPE_VARIABLE && !strcmp (v->name, name))
	    break;
	v = v->prev;
    }
    return v;
}

static int hc_sem_analyze_function_return_type (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                                void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                                void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    hc_sem_insn_value_t value;

    const hc_syn_insn_t *syn_insn;
    while ((syn_insn = scan_instruction_callback (scan_user_data))) {
        switch (syn_insn->type) {
        case HC_SYN_INSN_TYPENAME_VOID: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_TYPENAME_VOID, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_TYPENAME_DOUBLE: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_TYPENAME_DOUBLE, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        default: {
            char error_message[128];
            sprintf (error_message, "Unexpected instruction %d", syn_insn->type);
            error_callback (error_user_data, syn_insn->source_off, syn_insn->source_size, error_message);
            return -1;
        }
        }
    }

    return 0;
}

static int hc_sem_analyze_function_parameters (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                               void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                               void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    hc_sem_insn_value_t value;

    const hc_syn_insn_t *syn_insn;
    while ((syn_insn = scan_instruction_callback (scan_user_data))) {
        switch (syn_insn->type) {
        case HC_SYN_INSN_TYPENAME_VOID: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_TYPENAME_VOID, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_TYPENAME_DOUBLE: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_TYPENAME_DOUBLE, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_PLAIN_IDENTIFIER: {
        } break;
        case HC_SYN_INSN_EXTEND_IDENTIFIER: {
        } break;
        case HC_SYN_INSN_REFERENCE_LOCAL: {
        } break;
        default: {
            char error_message[128];
            sprintf (error_message, "Unexpected instruction %d", syn_insn->type);
            error_callback (error_user_data, syn_insn->source_off, syn_insn->source_size, error_message);
            return -1;
        }
        }
    }

/*
[Parameters syntactic RPN:]
 - [PLAIN_IDENTIFIER]        ("math")
 - [PLAIN_IDENTIFIER]        ("dmat4")
 - [EXTEND_IDENTIFIER]
 - [REFERENCE_LOCAL]
 - [PLAIN_IDENTIFIER]        ("m")
*/
    return 0;
}

static int hc_sem_analyze_function_body (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                         void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                         void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    hc_sem_insn_value_t value;

    const hc_syn_insn_t *syn_insn;
    while ((syn_insn = scan_instruction_callback (scan_user_data))) {
        switch (syn_insn->type) {
        case HC_SYN_INSN_PLAIN_IDENTIFIER: {
            value._string.len = syn_insn->value._string.len;
            value._string.data = memcpy (malloc (value._string.len), syn_insn->value._string.data, value._string.len);
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_VARIABLE, syn_insn->source_off, syn_insn->source_size, &value);
        } break;
        case HC_SYN_INSN_FIELD_ACCESS: {
            char error_message[128];
            sprintf (error_message, "HC_SYN_INSN_FIELD_ACCESS not implemented");
            error_callback (error_user_data, 0, 0, error_message);
            return -1;
        } break;
        case HC_SYN_INSN_ADDITION: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_ADDITION, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_SUBTRACTION: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_SUBTRACTION, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_MULTIPLICATION: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_MULTIPLICATION, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_DIVISION: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_DIVISION, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_ARRAY_ELEMENT_ACCESS: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_ARRAY_ELEMENT_ACCESS, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_LITERAL_INTEGER: {
            value._long = syn_insn->value._long;
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_LITERAL_INTEGER, syn_insn->source_off, syn_insn->source_size, &value);
        } break;
        case HC_SYN_INSN_LITERAL_FLOAT: {
            value._float = syn_insn->value._float;
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_LITERAL_FLOAT, syn_insn->source_off, syn_insn->source_size, &value);
        } break;
        case HC_SYN_INSN_LITERAL_DOUBLE: {
            value._double = syn_insn->value._double;
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_LITERAL_DOUBLE, syn_insn->source_off, syn_insn->source_size, &value);
        } break;
        case HC_SYN_INSN_INITIALIZE: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_INITIALIZE, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_MUTATE: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_MUTATE, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_STAPLE_BLOCK_ITEMS: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_STAPLE_BLOCK_ITEMS, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_EMPTY_EXPRESSION: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_EMPTY_EXPRESSION, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_FUNCTION_ARGUMENT: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_FUNCTION_ARGUMENT, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_STAPLE_FUNCTION_ARGUMENT: {
            sem_instruction_callback (emit_user_data, HC_SEM_INSN_STAPLE_FUNCTION_ARGUMENT, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        case HC_SYN_INSN_FUNCTION_CALL: {
            sem_instruction_callback (emit_user_data, HC_SYN_INSN_FUNCTION_CALL, syn_insn->source_off, syn_insn->source_size, NULL);
        } break;
        default: {
            char error_message[128];
            sprintf (error_message, "Unexpected instruction %d", syn_insn->type);
            error_callback (error_user_data, syn_insn->source_off, syn_insn->source_size, error_message);
            return -1;
        }
        }
    }

    return 0;
}

static int hc_sem_analyze_struct (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                  void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                  void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    (void) scan_user_data;
    (void) scan_instruction_callback;
    (void) sem_instruction_callback;

    char error_message[128];
    sprintf (error_message, "Unimplemented target STRUCT");
    error_callback (error_user_data, 0, 0, error_message);
    return -1;
}

static int hc_sem_analyze_binding (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                   void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                   void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    (void) scan_user_data;
    (void) scan_instruction_callback;
    (void) sem_instruction_callback;

    char error_message[128];
    sprintf (error_message, "Unimplemented target BINDING");
    error_callback (error_user_data, 0, 0, error_message);
    return -1;
}

static int hc_sem_analyze_array (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                 void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                 void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    (void) scan_user_data;
    (void) scan_instruction_callback;
    (void) sem_instruction_callback;

    char error_message[128];
    sprintf (error_message, "Unimplemented target ARRAY");
    error_callback (error_user_data, 0, 0, error_message);
    return -1;
}

static int hc_sem_analyze_class (void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                                 void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                                 void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    
}

int hc_sem_analyze (int target,
                    void *scan_user_data, hc_sem_analyzer_scan_syn_instruction_t scan_instruction_callback,
                    void *emit_user_data, hc_sem_analyzer_emit_instruction_t sem_instruction_callback,
                    void *error_user_data, hc_sem_analyzer_emit_error_t error_callback)
{
    switch (target) {
    case HC_SEM_ANALYZER_FUNCTION_RETURN_TYPE:
        return hc_sem_analyze_function_return_type (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    case HC_SEM_ANALYZER_FUNCTION_PARAMETERS:
        return hc_sem_analyze_function_parameters (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    case HC_SEM_ANALYZER_FUNCTION_BODY:
        return hc_sem_analyze_function_body (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    case HC_SEM_ANALYZER_STRUCT:
        return hc_sem_analyze_struct (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    case HC_SEM_ANALYZER_CLASS:
        return hc_sem_analyze_class (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    case HC_SEM_ANALYZER_BINDING:
        return hc_sem_analyze_binding (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    case HC_SEM_ANALYZER_ARRAY:
        return hc_sem_analyze_array (scan_user_data, scan_instruction_callback, emit_user_data, sem_instruction_callback, error_user_data, error_callback);
    }

    char error_message[128];
    sprintf (error_message, "Unknown target %d", target);
    error_callback (emit_user_data, 0, 0, error_message);
    return -1;
}
