#pragma once

typedef void *(*hc_object_stream_read_callback_t) (
    void *user_data,
    void *object);

typedef struct hc_object_stream_t {
    hc_object_stream_read_callback_t callback;
    void *user_data;
} hc_object_stream_t;


static inline hc_object_stream_t hc_object_stream_init (hc_object_stream_read_callback_t callback, void *user_data)
{
    hc_object_stream_t ret = {
        .callback = callback,
        .user_data = user_data,
    };
    return ret;
}

extern void *hc_object_stream_push (
    hc_object_stream_t *s,
    void *object);
