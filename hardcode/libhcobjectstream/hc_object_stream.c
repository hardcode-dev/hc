#include "hc_object_stream.h"


void *hc_object_stream_push (hc_object_stream_t *s, void *object)
{
    return s->callback (s->user_data, object);
}
