#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <libhcvmmodulebuilder/hc_vm_function_builder_type_defs.h>

typedef struct hc_vm_type_t hc_vm_type_t;
typedef struct hc_vm_function_t hc_vm_function_t;

enum {
    HC_VM_EXEC_OK = 0
    , HC_VM_EXEC_LEGACY_ERR = 1
    , HC_VM_EXEC_OUT_OF_MEMORY_ERR = 2
    , HC_VM_EXEC_UNSUPPORTED_INSTR_ERR = 3
    , HC_VM_EXEC_UNIMPLEMENTED_ERR = 4
    , HC_VM_EXEC_NULLPTR_ERR = 5
    , HC_VM_EXEC_OUT_OF_BOUNDS_ERR = 6
    , HC_VM_EXEC_INVALID_WRITE = 7 // Should not occur
    , HC_VM_EXEC_CONTINUE = 8 // Program still wants to run
    , HC_VM_EXEC_ZERO_VAL = 9 // Division or modulo by zero
    , HC_VM_EXEC_TRAP = 10
};

typedef struct hc_vm_executor_flags_t {
    // Fills stack with "."
    bool fill_memory;
    // All allocated memory is zero by default
    bool zero_allocations;
    bool ignore_traps;
} hc_vm_executor_flags_t;

typedef struct hc_vm_executor_state_t {
    void* stack_pointer;
    const hc_vm_function_t* func;
    uint64_t instr_pointer;
    struct hc_vm_executor_state_t* prev;
} hc_vm_executor_state_t;

typedef struct hc_vm_executor_context_t {
    void* stack_start;
    hc_vm_executor_flags_t flags;
    struct hc_vm_executor_state_t* state;
    const void* last_trap_state;
} hc_vm_executor_context_t;

void hc_vm_init_default_executor_flags(hc_vm_executor_flags_t* flags);

typedef struct hc_vm_arg_memory_t {
    const hc_vm_type_t* type_ptr;
    void* rw_memory;
    const void* ro_memory;
} hc_vm_arg_memory_t;

typedef struct hc_vm_native_function_t {
    hc_vm_type_t* signature;
    void (*func) (hc_vm_arg_memory_t);
} hc_vm_native_function_t;

// Starts a function call
uint64_t hc_vm_function_init_execution(
    const hc_vm_function_t* func,
    hc_vm_executor_flags_t* flags,
    hc_vm_arg_memory_t* arg,
    hc_vm_executor_context_t* result);

// Makes one step of execution
// Automatically uninits execution when status is not HC_VM_EXEC_CONTINUE or HC_VM_EXEC_TRAP
uint64_t hc_vm_function_step_execution(hc_vm_executor_context_t* context);
// Calls hc_vm_function_step_execution until completion or until trapped
uint64_t hc_vm_function_drain_execution(hc_vm_executor_context_t* context);
// Use to manually stop execution
uint64_t hc_vm_function_uninit_execution(hc_vm_executor_context_t* context);

// Pass memory to function
uint64_t hc_vm_init_arg_memory_rw(void* buffer, const hc_vm_type_t* type, hc_vm_arg_memory_t* result);
uint64_t hc_vm_init_arg_memory_ro(const void* buffer, const hc_vm_type_t* type, hc_vm_arg_memory_t* result);

// Read memory
uint64_t hc_vm_get_arg_memory_raw(hc_vm_executor_context_t* context, const hc_vm_function_instruction_arg_t* arg, hc_vm_arg_memory_t* result);
uint64_t hc_vm_get_arg_memory(hc_vm_executor_context_t* context, const hc_vm_function_instruction_arg_t* arg, hc_vm_arg_memory_t* result);
