#include "hc_vm_executor.h"
#include <libhcvmmodulebuilder/hc_vm_function_builder.h>
#include <libhcvmtypes/hc_vm_types.h>
#include <string.h>

#define GLUE(x,y) x##y
#define GLUE2(x,y) GLUE(x, y)
#define UNIQUE_VAR GLUE2(temp_, __LINE__)
#define UNWRAP_ERROR(x) { uint64_t UNIQUE_VAR = x; if (UNIQUE_VAR) return UNIQUE_VAR; }

static void* hc_vm_get_stack_start(hc_vm_executor_context_t* context) {
    return context->state;
}

static uint64_t hc_vm_stack_offset_to_pointer_offset(const hc_vm_executor_context_t* context, uint64_t stack_offset, uint64_t* p_offset) {
    uint64_t extra_offset = context->state->func->metadata.red_zone_bytes - hc_vm_word_size;
    if (UINT64_MAX - stack_offset < extra_offset) {
        return 1;
    }
    *p_offset = stack_offset + extra_offset;
    return 0;
}

uint64_t hc_vm_get_arg_memory_raw(
        hc_vm_executor_context_t* context,
        const hc_vm_function_instruction_arg_t* arg,
        hc_vm_arg_memory_t* result) {
    const hc_vm_type_t* type_ptr = arg->type_ptr;
    result->type_ptr = type_ptr;
    result->ro_memory = NULL;
    result->rw_memory = NULL;
    if (!arg->immidiate) {
        if (arg->stack_offset < hc_vm_word_size) {
            return HC_VM_EXEC_NULLPTR_ERR;
        }
        uint64_t offset;
        if (hc_vm_stack_offset_to_pointer_offset(context, arg->stack_offset, &offset)) return 1;
        result->rw_memory = hc_vm_get_stack_start(context) + offset;
        result->ro_memory = result->rw_memory;
    } else if (type_ptr->type != HC_VM_TYPE_POINTER) {
        // cannot write to immidiate value
        result->ro_memory = arg->primitive;
    } else {
        result->ro_memory = arg->raw_memory;
    }
    return HC_VM_EXEC_OK;
}

uint64_t hc_vm_get_arg_memory(
        hc_vm_executor_context_t* context,
        const hc_vm_function_instruction_arg_t* arg,
        hc_vm_arg_memory_t* result) {
    UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, arg, result));
    if (arg->type_ptr->type == HC_VM_TYPE_POINTER) {
        result->type_ptr = result->type_ptr->subtypes[0];
        if (result->rw_memory != NULL) {
            result->rw_memory = *(void**)result->rw_memory;
            result->ro_memory = result->rw_memory;
        } else {
            // Immidate pointers, compiler should monitor usage of these
            result->ro_memory = *(const void**)result->ro_memory;
        }
    }
    return HC_VM_EXEC_OK;
}

#define HC_VM_HANDLE_OP_CASE_INJECT(type)
#define HC_VM_HANDLE_OP_CASE(vm_type, type, bin_op) \
    case vm_type: \
    { \
        type* c_typed = (type*)c.rw_memory; \
        HC_VM_HANDLE_OP_CASE_INJECT(type); \
        *c_typed = (*(type*)a.ro_memory) bin_op (*(type*)b.ro_memory); \
        break; \
    };

#define HC_VM_HANDLE_OP_CASE_DOUBLE(...) HC_VM_HANDLE_OP_CASE(__VA_ARGS__)

// Operation is required to have compatible arguments
// this restriction is enforced by the compiler
#define HC_VM_HANDLE_OP(name, bin_op) \
    static uint64_t perform_##name##_op(hc_vm_arg_memory_t a, hc_vm_arg_memory_t b, hc_vm_arg_memory_t c) { \
        switch (c.type_ptr->type) \
        { \
        HC_VM_HANDLE_OP_CASE_DOUBLE(HC_VM_TYPE_DOUBLE, double, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_BYTE, unsigned char, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_INT, int32_t, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_UINT, uint32_t, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_LONG, int64_t, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_ULONG, uint64_t, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_WORD, long, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_UWORD, unsigned long, bin_op) \
        default: \
            return HC_VM_EXEC_UNIMPLEMENTED_ERR; \
        } \
        return HC_VM_EXEC_OK; \
    }; \
    static uint64_t handle_##name(hc_vm_executor_context_t* context, const hc_vm_function_instruction_t* instr) { \
        hc_vm_arg_memory_t a, b, c; \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a)); \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b)); \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[2], &c)); \
        return perform_##name##_op(a, b, c); \
    }

// TODO detect overflows
HC_VM_HANDLE_OP(add, +)
HC_VM_HANDLE_OP(sub, -)
HC_VM_HANDLE_OP(mul, *)
#undef HC_VM_HANDLE_OP_CASE_INJECT
#define HC_VM_HANDLE_OP_CASE_INJECT(type) \
    if (*(type*)b.ro_memory == (type)0) \
        return HC_VM_EXEC_ZERO_VAL;
HC_VM_HANDLE_OP(div, /)
#undef HC_VM_HANDLE_OP_CASE_DOUBLE
#define HC_VM_HANDLE_OP_CASE_DOUBLE(...)
HC_VM_HANDLE_OP(mod, %)


#define HC_VM_HANDLE_BOOL_OP_CASE(vm_type, type, bin_op) \
    case vm_type: \
    { \
        cond = (*(type*)a.ro_memory) bin_op (*(type*)b.ro_memory); \
        break; \
    };

// Arguments a and b should be compatible, c should be unsigned
#define HC_VM_HANDLE_BOOL_OP(name, bin_op) \
    static uint64_t get_##name##_cond(const hc_vm_arg_memory_t a, const hc_vm_arg_memory_t b, bool* result) { \
        bool cond; \
        switch (a.type_ptr->type) \
        { \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_DOUBLE, double, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_BYTE, unsigned char, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_INT, int32_t, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_UINT, uint32_t, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_LONG, int64_t, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_ULONG, uint64_t, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_WORD, long, bin_op) \
        HC_VM_HANDLE_BOOL_OP_CASE(HC_VM_TYPE_UWORD, unsigned long, bin_op) \
        default: \
            return HC_VM_EXEC_UNIMPLEMENTED_ERR; \
        } \
        *result = cond; \
        return HC_VM_BUILD_OK; \
    }; \
    static uint64_t handle_##name(hc_vm_executor_context_t* context, const hc_vm_function_instruction_t* instr) { \
        hc_vm_arg_memory_t a, b, c; \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a)); \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b)); \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[2], &c)); \
        bool cond; \
        get_##name##_cond(a, b, &cond); \
        int fill_value = 0; \
        if (cond) fill_value = -1; /* Set fill_value to 0xFF... */ \
        memset(c.rw_memory, fill_value, c.type_ptr->size_bytes); \
        return HC_VM_EXEC_OK; \
    }

HC_VM_HANDLE_BOOL_OP(eq, ==)
HC_VM_HANDLE_BOOL_OP(greater, >)
HC_VM_HANDLE_BOOL_OP(greater_eq, >=)
HC_VM_HANDLE_BOOL_OP(less, <)
HC_VM_HANDLE_BOOL_OP(less_eq, <=)

// All arguments should be compatible and unsigned
#define HC_VM_HANDLE_BIT_OP(name, bin_op) \
    static uint64_t handle_##name(hc_vm_executor_context_t* context, const hc_vm_function_instruction_t* instr) { \
        hc_vm_arg_memory_t a, b, c; \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a)); \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b)); \
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[2], &c)); \
        switch (c.type_ptr->type) \
        { \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_BYTE, unsigned char, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_UINT, uint32_t, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_ULONG, uint64_t, bin_op) \
        HC_VM_HANDLE_OP_CASE(HC_VM_TYPE_UWORD, unsigned long, bin_op) \
        default: \
            return HC_VM_EXEC_UNIMPLEMENTED_ERR; \
        } \
        return HC_VM_EXEC_OK; \
    }

HC_VM_HANDLE_BIT_OP(or, |);
HC_VM_HANDLE_BIT_OP(and, &);
HC_VM_HANDLE_BIT_OP(shift_left, <<);
HC_VM_HANDLE_BIT_OP(shift_right, >>);

static uint64_t index_type(
        const hc_vm_arg_memory_t* array,
        uint64_t index,
        const hc_vm_function_instruction_t* instr,
        hc_vm_arg_memory_t* result) {
    const hc_vm_type_t* true_type = array->type_ptr;
    if (true_type->element_count <= index) {
        return HC_VM_EXEC_OUT_OF_BOUNDS_ERR;
    }
    uint64_t offset = 0;
    switch (true_type->type)
    {
    case HC_VM_TYPE_MULTI:
        result->type_ptr = true_type->subtypes[0];
        if (hc_vm_type_array_index_offset(result->type_ptr, index, &offset)) return 1;
        break;
    case HC_VM_TYPE_STRUCT:
        result->type_ptr = true_type->subtypes[index];
        offset = instr->pointer_offset;
        break;
    default:
        return HC_VM_EXEC_UNIMPLEMENTED_ERR;
    }
    if (array->rw_memory != NULL) {
        result->rw_memory = array->rw_memory + offset;
        result->ro_memory = result->rw_memory;
    } else {
        result->rw_memory = NULL;
        result->ro_memory = array->ro_memory + offset;
    }
    return 0;
}

static bool is_buffer_zero(const void* buf, uint64_t size) {
    bool is_zero = true;
    const char* cbuf = (const char*)buf;
    for (uint64_t i = 0; i < size; ++i) {
        is_zero &= cbuf[i] == 0;
    }
    return is_zero;
}

uint64_t hc_vm_function_uninit_execution(hc_vm_executor_context_t* ctx) {
    free(ctx->stack_start);
    return 0;
}

void hc_vm_init_default_executor_flags(hc_vm_executor_flags_t* flags) {
    flags->fill_memory = true;
    flags->zero_allocations = true;
    flags->ignore_traps = false;
}

static uint64_t hc_vm_uninit_stack_frame(hc_vm_executor_context_t* context, bool* is_final) {
    if (context->state->prev == NULL) {
        *is_final = true;
    }
    context->state = context->state->prev;
    return 0;
}

uint64_t hc_vm_init_arg_memory_rw(void* buffer, const hc_vm_type_t* type, hc_vm_arg_memory_t* result) {
    UNWRAP_ERROR(hc_vm_init_arg_memory_ro(buffer, type, result));
    result->rw_memory = buffer;
    return 0;
}

uint64_t hc_vm_init_arg_memory_ro(const void* buffer, const hc_vm_type_t* type, hc_vm_arg_memory_t* result) {
    result->type_ptr = type;
    result->ro_memory = buffer;
    return 0;
}

static uint64_t hc_vm_init_stack_frame(hc_vm_executor_context_t* context, const hc_vm_function_t* func, const hc_vm_arg_memory_t* arg) {
    // Find red zone size
    uint64_t size = func->metadata.red_zone_bytes;
    hc_vm_executor_state_t new_state;
    new_state.func = func;
    new_state.instr_pointer = 0;
    new_state.prev = context->state;

    void* stack_start;
    // Stack pointer is moved to current stack pointer
    if (context->state == NULL) {
        stack_start = context->stack_start;
    } else {
        stack_start = context->state->stack_pointer;
    }
    // Stack pointer is moved to allocate the red zone
    new_state.stack_pointer = stack_start + size;
    // copy new state to stack
    memcpy(stack_start, &new_state, sizeof(hc_vm_executor_state_t));
    // move new context's ptr
    context->state = (hc_vm_executor_state_t*)stack_start;
    // Size is aligned
    if (context->state->func->metadata.signature != NULL) {
        if (arg == NULL) return 1;
        // write argument to stack, size alignment doesnt matter
        memcpy(context->state->stack_pointer, arg->ro_memory, arg->type_ptr->size_bytes);
        // Update stack size
        context->state->stack_pointer += arg->type_ptr->size_bytes;
    }
    return 0;
}

uint64_t hc_vm_function_init_execution(
        const hc_vm_function_t* func,
        hc_vm_executor_flags_t* flags,
        hc_vm_arg_memory_t* arg,
        hc_vm_executor_context_t* result) {
    uint64_t stack_size = func->metadata.required_stack_bytes;
    result->stack_start = malloc(stack_size);
    result->state = NULL;

    if (flags) {
        result->flags = *flags;
    } else {
        hc_vm_init_default_executor_flags(&result->flags);
    }
    if (result->flags.fill_memory) {
        memset(result->stack_start, '.', stack_size);
    }
    hc_vm_init_stack_frame(result, func, arg);
    return 0;
}

uint64_t hc_vm_function_drain_execution(hc_vm_executor_context_t* context) {
    uint64_t status;
    do {
        // Step the program
        status = hc_vm_function_step_execution(context);
    } while(status == HC_VM_EXEC_CONTINUE);
    return status;
}

uint64_t hc_vm_function_step_execution_impl(hc_vm_executor_context_t* context) {
    uint64_t* instr_pointer = &context->state->instr_pointer;
    hc_vm_executor_state_t* ptr = context->state;
    const hc_vm_function_instruction_t* instr = &ptr->func->instructions[*instr_pointer];
    {
        // Zero out the memory for untrusted functions
        uint64_t offset;
        if (hc_vm_stack_offset_to_pointer_offset(context, instr->stack_depth, &offset)) return 1;
        // pointer offset is not compensating for zero word
        offset += hc_vm_word_size;
        void* new_stack_pointer = hc_vm_get_stack_start(context) + offset;
        if (context->flags.zero_allocations && ptr->stack_pointer < new_stack_pointer) {
            memset(ptr->stack_pointer, 0, new_stack_pointer - ptr->stack_pointer);
        }
        ptr->stack_pointer = new_stack_pointer;
    }
    context->last_trap_state = NULL;
    switch (instr->instr)
    {
    case HC_VM_INSTR_TRAP:
        if (!context->flags.ignore_traps) {
            hc_vm_arg_memory_t a;
            UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &a));
            context->last_trap_state = (const void*)a.ro_memory;
            return HC_VM_EXEC_TRAP;
        }
        break;
    case HC_VM_INSTR_PUSH:
        break;
    case HC_VM_INSTR_EXTEND:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        memset(b.rw_memory, 0, b.type_ptr->size_bytes);
        // Copy all the data
        memcpy(b.rw_memory, a.ro_memory, a.type_ptr->size_bytes);
        break;
    }
    case HC_VM_INSTR_SET:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        // Copy all the data
        memcpy(a.rw_memory, b.ro_memory, a.type_ptr->size_bytes);
        break;
    }
    case HC_VM_INSTR_SETPTRINDIR:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        // Copy all the data
        memcpy(b.rw_memory, a.ro_memory, b.type_ptr->size_bytes);
        break;
    }
    case HC_VM_INSTR_DEREFPTR:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[1], &b));
        // Copy all the data
        memcpy(b.rw_memory, a.ro_memory, b.type_ptr->size_bytes);
        break;
    }
    case HC_VM_INSTR_SETZPTR:
    {
        hc_vm_arg_memory_t a;
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &a));
        // Copy all the data
        memset(a.rw_memory, 0, a.type_ptr->size_bytes);
        break;
    }
    case HC_VM_INSTR_ADD:
        UNWRAP_ERROR(handle_add(context, instr));
        break;
    case HC_VM_INSTR_SUB:
        UNWRAP_ERROR(handle_sub(context, instr));
        break;
    case HC_VM_INSTR_MUL:
        UNWRAP_ERROR(handle_mul(context, instr));
        break;
    case HC_VM_INSTR_DIV:
        UNWRAP_ERROR(handle_div(context, instr));
        break;
    case HC_VM_INSTR_MOD:
        UNWRAP_ERROR(handle_mod(context, instr));
        break;
    case HC_VM_INSTR_EQ:
        UNWRAP_ERROR(handle_eq(context, instr));
        break;
    case HC_VM_INSTR_LESS:
        UNWRAP_ERROR(handle_less(context, instr));
        break;
    case HC_VM_INSTR_LESS_EQ:
        UNWRAP_ERROR(handle_less_eq(context, instr));
        break;
    case HC_VM_INSTR_GREATER:
        UNWRAP_ERROR(handle_greater(context, instr));
        break;
    case HC_VM_INSTR_GREATER_EQ:
        UNWRAP_ERROR(handle_greater_eq(context, instr));
        break;
    case HC_VM_INSTR_OR:
        UNWRAP_ERROR(handle_or(context, instr));
        break;
    case HC_VM_INSTR_AND:
        UNWRAP_ERROR(handle_and(context, instr));
        break;
    case HC_VM_INSTR_BSR:
        UNWRAP_ERROR(handle_shift_right(context, instr));
        break;
    case HC_VM_INSTR_BSL:
        UNWRAP_ERROR(handle_shift_left(context, instr));
        break;
    case HC_VM_INSTR_LOOP:
    {
        break;
    }
    case HC_VM_INSTR_LOOP_ASC:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        bool is_loop_done;
        // Want to iterate if equal
        UNWRAP_ERROR(get_greater_cond(a, b, &is_loop_done));
        if (is_loop_done) {
            *instr_pointer = instr->next_instruction;
        }
        break;
    }
    case HC_VM_INSTR_BREAK:
    {
        const hc_vm_function_instruction_t* loop_start = &ptr->func->instructions[instr->next_instruction];
        uint64_t loop_end_idx = loop_start->next_instruction;
        *instr_pointer = loop_end_idx;
        break;
    }
    case HC_VM_INSTR_CONTINUE:
    {
        const hc_vm_function_instruction_t* loop_start = &ptr->func->instructions[instr->next_instruction];
        uint64_t loop_end_idx = loop_start->next_instruction;
        *instr_pointer = loop_end_idx - 1;
        break;

    }
    case HC_VM_INSTR_LOOP_END:
    {
        *instr_pointer = instr->next_instruction;
        break;
    }
    case HC_VM_INSTR_LOOP_ASC_END:
    {
        const hc_vm_function_instruction_t* loop_start = &ptr->func->instructions[instr->next_instruction];
        hc_vm_arg_memory_t a, b, step;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &loop_start->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &loop_start->args[1], &b));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &loop_start->args[2], &step));
        bool is_loop_done;
        // stop if equal
        UNWRAP_ERROR(get_greater_eq_cond(a, b, &is_loop_done));
        if (!is_loop_done) {
            UNWRAP_ERROR(perform_add_op(a, step, a));
            *instr_pointer = instr->next_instruction;
        }
        break;
    }
    case HC_VM_INSTR_NOT:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        unsigned char* b_memory =  (unsigned char*)b.rw_memory;
        const unsigned char* a_memory =  (const unsigned char*)a.ro_memory;
        for (size_t i = 0; i < a.type_ptr->size_bytes; ++i) {
            b_memory[i] = ~a_memory[i];
        }
        break;
    }
    case HC_VM_INSTR_IFNZ:
    {
        hc_vm_arg_memory_t a;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        if (is_buffer_zero(a.ro_memory, a.type_ptr->size_bytes)) {
            *instr_pointer = instr->next_instruction;
        }
        break;
    }
    case HC_VM_INSTR_ISNZPTR:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        int is_nz_ptr = !is_buffer_zero(a.ro_memory, a.type_ptr->size_bytes);
        if (is_nz_ptr) is_nz_ptr = -1;
        memset(b.rw_memory, is_nz_ptr, b.type_ptr->size_bytes);
        break;
    }
    case HC_VM_INSTR_ELSE:
    {
        *instr_pointer = instr->next_instruction;
        break;
    }
    case HC_VM_INSTR_ENDIF:
        break;
    case HC_VM_INSTR_REF:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[1], &b));
        void** a_ptr = (void**)a.rw_memory;
        *a_ptr = b.rw_memory;
        a.ro_memory = a.rw_memory;
        break;
    }
    case HC_VM_INSTR_ARR_INDEX:
    // fall though
    case HC_VM_INSTR_STRUCT_INDEX:
    {
        hc_vm_arg_memory_t a, b, c;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[2], &c));
        hc_vm_arg_memory_t t;
        UNWRAP_ERROR(index_type(&a, *(uint64_t*)b.ro_memory, instr, &t));
        void** rwm = (void**)c.rw_memory;
        *rwm = t.rw_memory;
        const void** rm = (const void**)c.ro_memory;
        *rm = t.ro_memory;
        break;
    }
    case HC_VM_INSTR_ARR_LENGTH:
    {
        hc_vm_arg_memory_t a, b;
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[0], &a));
        UNWRAP_ERROR(hc_vm_get_arg_memory(context, &instr->args[1], &b));
        memcpy(b.rw_memory, &a.type_ptr->element_count, sizeof(uint64_t));
        break;
    }
    case HC_VM_INSTR_CALL: {
        hc_vm_arg_memory_t func, arg;
        hc_vm_arg_memory_t* arg_ptr = &arg;
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &func));
        const hc_vm_function_t* func_ptr = (const hc_vm_function_t*)func.ro_memory;
        if (func_ptr->metadata.signature != NULL) {
            UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[1], arg_ptr));
        } else {
            arg_ptr = NULL;
        }
        // hc_vm_function_perform_call(context_ptr, func_ptr, arg_ptr, false);
        hc_vm_init_stack_frame(context, func_ptr, arg_ptr);
        break;
    }
    case HC_VM_INSTR_NATIVE_CALL: {
        hc_vm_arg_memory_t func, arg;
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[0], &func));
        UNWRAP_ERROR(hc_vm_get_arg_memory_raw(context, &instr->args[1], &arg));
        const hc_vm_native_function_t* func_ptr = (const hc_vm_native_function_t*)func.ro_memory;
        func_ptr->func(arg);
        break;
    }
    case HC_VM_INSTR_RETURN: {
        bool final = false;
        UNWRAP_ERROR(hc_vm_uninit_stack_frame(context, &final));
        // No stack frames left, finish execution
        if (final) {
            return HC_VM_EXEC_OK;
        }
        break;
    }
    case HC_VM_INSTR_SCOPE:
        break;
    case HC_VM_INSTR_SCOPE_END:
        break;
    default:
        return HC_VM_EXEC_UNSUPPORTED_INSTR_ERR;
    }
    return HC_VM_EXEC_CONTINUE;
}

uint64_t hc_vm_function_step_execution(hc_vm_executor_context_t* context) {
    uint64_t* instr_pointer = &context->state->instr_pointer;
    uint64_t status = hc_vm_function_step_execution_impl(context);
    if (status != HC_VM_EXEC_CONTINUE && status != HC_VM_EXEC_TRAP) {
        hc_vm_function_uninit_execution(context);
    } else {
        ++(*instr_pointer); // Increase the instruction pointer of the CURRENT (before the call) function
    }
    return status;
}
