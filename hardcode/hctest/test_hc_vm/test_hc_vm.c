#include <libhcvmmodulebuilder/hc_vm_function_builder.h>
#include <libhcvmexecutor/hc_vm_executor.h>
#include <munit/munit.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#define ASSERT_OK(x) munit_assert_ullong(x, ==, 0)
#define ASSERT_TRAP(x) munit_assert_ullong(x, ==, HC_VM_EXEC_TRAP)
#define ASSERT_ERROR(x) munit_assert_ullong(x, !=, 0)
#define UNUSED(x) (void)(x)
#define ASSERT_INT_EQ(loc, type, val) {\
    hc_vm_arg_memory_t target; \
    hc_vm_get_arg_memory(&context, &loc, &target); \
    munit_assert_##type(*(const type##_t*)target.ro_memory, ==, val); \
  }

static const char* dummy_trap_info = "This trap has no useful info";

MunitResult simple_vm_test(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);
    hc_vm_function_t *module;
    ASSERT_OK(hc_vm_function_create(&module, NULL, NULL));
    munit_assert_ptr(module, !=, NULL);
    hc_vm_function_instruction_arg_t c;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_uint, &c));
    hc_vm_function_instruction_arg_t a;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_uint, &a));
    hc_vm_function_instruction_arg_t a_val;
    uint64_t x = 321;
    hc_vm_function_data_immidiate(&x, &a_val, &hc_vm_type_uint);
    ASSERT_OK(hc_vm_function_instr_set(module, &a_val, &a));
    hc_vm_function_instruction_arg_t b;
    uint64_t y = 123;
    hc_vm_function_data_immidiate(&y, &b, &hc_vm_type_uint);
    ASSERT_OK(hc_vm_function_instr_add(module, &a, &b, &c));
    ASSERT_OK(hc_vm_function_instr_trap(module, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(module));

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(module, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));
    munit_assert_ptr_equal(context.last_trap_state, dummy_trap_info);

    hc_vm_arg_memory_t c_memory;
    hc_vm_get_arg_memory_raw(&context, &c, &c_memory);
    munit_assert_uint32(*(uint64_t*)c_memory.ro_memory, ==, 444);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(module);

    return MUNIT_OK;
}

MunitResult vm_test_pointer_and_type(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);
    hc_vm_function_t *module;
    ASSERT_OK(hc_vm_function_create(&module, NULL, NULL));
    munit_assert_ptr(module, !=, NULL);
    hc_vm_function_instruction_arg_t a;
    // Push uint to stack
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_uint, &a));
    // Error int is not compatible with uint
    hc_vm_function_instruction_arg_t a_val;
    uint64_t v = 321;
    hc_vm_function_data_immidiate(&v, &a_val, &hc_vm_type_int);
    ASSERT_ERROR(hc_vm_function_instr_set(module, &a_val, &a));
    hc_vm_function_instruction_arg_t a_pointer;
    hc_vm_type_t uint_ptr;
    hc_vm_type_init_pointer(&hc_vm_type_uint, &uint_ptr, false);
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &uint_ptr, &a_pointer));
    ASSERT_OK(hc_vm_function_instr_ref(module, &a, &a_pointer));
    // Error int is not compatiple with *uint
    ASSERT_ERROR(hc_vm_function_instr_set(module, &a_val, &a_pointer));
    uint64_t v1 = 123;
    // Ok uint is compatible with *uint
    hc_vm_function_instruction_arg_t a_correct_val;
    hc_vm_function_data_immidiate(&v1, &a_correct_val, &hc_vm_type_uint);
    ASSERT_OK(hc_vm_function_instr_set(module, &a_correct_val, &a_pointer));
    ASSERT_OK(hc_vm_function_instr_add(module, &a_pointer, &a_pointer, &a_pointer));
    ASSERT_OK(hc_vm_function_instr_trap(module, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(module));

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(module, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    hc_vm_arg_memory_t a_memory;
    hc_vm_get_arg_memory_raw(&context, &a, &a_memory);
    munit_assert_uint32(*(uint64_t*)a_memory.ro_memory, ==, 246);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(module);

    return MUNIT_OK;
}

MunitResult vm_test_branching(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);
    uint64_t zero_v = 0;
    hc_vm_function_instruction_arg_t imm_zero;
    hc_vm_function_data_immidiate(&zero_v, &imm_zero, &hc_vm_type_ulong);

    hc_vm_function_t *module;
    ASSERT_OK(hc_vm_function_create(&module, NULL, NULL));
    munit_assert_ptr(module, !=, NULL);
    hc_vm_function_instruction_arg_t a;
    // Push uint to stack
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_ulong, &a));
    hc_vm_function_instruction_arg_t a_pointer;
    hc_vm_type_t ulong_ptr;
    hc_vm_type_init_pointer(&hc_vm_type_ulong, &ulong_ptr, false);
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &ulong_ptr, &a_pointer));
    ASSERT_OK(hc_vm_function_instr_ref(module, &a, &a_pointer));
    uint64_t v1 = 123;
    hc_vm_function_instruction_arg_t a_correct_val;
    hc_vm_function_data_immidiate(&v1, &a_correct_val, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_set(module, &a_correct_val, &a_pointer));
    uint64_t v2 = 321;
    hc_vm_function_instruction_arg_t other;
    hc_vm_function_data_immidiate(&v2, &other, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t b;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_ulong, &b));
    ASSERT_OK(hc_vm_function_instr_set(module, &a_pointer, &b));
    ASSERT_OK(hc_vm_function_instr_ifnz(module, &a));
    {
      hc_vm_function_instruction_arg_t temp;
      ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_ulong, &temp));
    }
    ASSERT_OK(hc_vm_function_instr_add(module, &b, &a_pointer, &b));
    ASSERT_OK(hc_vm_function_instr_else(module));
    ASSERT_OK(hc_vm_function_instr_set(module, &other, &b));
    ASSERT_OK(hc_vm_function_instr_endif(module));
    hc_vm_function_instruction_arg_t c;
    // Automatically set to zero by strict mode
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_ulong, &c));
    hc_vm_function_instruction_arg_t az;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &hc_vm_type_byte, &az));
    ASSERT_OK(hc_vm_function_instr_eq(module, &a, &imm_zero, &az));
    ASSERT_OK(hc_vm_function_instr_ifnz(module, &az));
    ASSERT_OK(hc_vm_function_instr_else(module));
    ASSERT_OK(hc_vm_function_instr_set(module, &other, &c));
    ASSERT_OK(hc_vm_function_instr_endif(module));
    ASSERT_OK(hc_vm_function_instr_trap(module, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(module));

    const hc_vm_function_metadata_t* metadata = hc_vm_function_extract_metadata(module);

    // Amount of allocated memory in actually 37 bytes
    // but scopes allow us not to allocate extra 4 bytes
    munit_assert_uint32(metadata->required_stack_bytes, ==, 33 + metadata->red_zone_bytes);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(module, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    hc_vm_arg_memory_t b_memory;
    hc_vm_get_arg_memory_raw(&context, &b, &b_memory);
    munit_assert_uint32(*(uint64_t*)b_memory.ro_memory, ==, 246);
    hc_vm_arg_memory_t c_memory;
    hc_vm_get_arg_memory_raw(&context, &c, &c_memory);
    munit_assert_uint32(*(uint64_t*)c_memory.ro_memory, ==, 321);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(module);

    return MUNIT_OK;
}

MunitResult vm_test_array_indexing(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);
    hc_vm_function_t *module;
    ASSERT_OK(hc_vm_function_create(&module, NULL, NULL));
    munit_assert_ptr(module, !=, NULL);
    hc_vm_type_t multi_u32_tp;
    hc_vm_type_init_multi(&hc_vm_type_uint, 7, &multi_u32_tp);
    hc_vm_type_t u32_ptr_tp;
    hc_vm_type_init_pointer(&hc_vm_type_uint, &u32_ptr_tp, false);
    munit_assert_uint64(multi_u32_tp.size_bytes, ==, 28);
    hc_vm_function_instruction_arg_t multi_u32;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &multi_u32_tp, &multi_u32));
    uint64_t v1 = 0;
    hc_vm_function_instruction_arg_t zero_ul;
    hc_vm_function_data_immidiate(&v1, &zero_ul, &hc_vm_type_ulong);
    uint64_t v2 = 1;
    hc_vm_function_instruction_arg_t one_u32;
    hc_vm_function_data_immidiate(&v2, &one_u32, &hc_vm_type_uint);
    hc_vm_function_instruction_arg_t one_ul;
    hc_vm_function_data_immidiate(&v2, &one_ul, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t a;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &u32_ptr_tp, &a));
    ASSERT_OK(hc_vm_function_instr_arr_index(module, &multi_u32, &one_ul, &a));
    ASSERT_OK(hc_vm_function_instr_set(module, &one_u32, &a));
    ASSERT_OK(hc_vm_function_instr_trap(module, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(module));

    const hc_vm_function_metadata_t* metadata = hc_vm_function_extract_metadata(module);
    // uint is tightly packed
    munit_assert_uint64(metadata->required_stack_bytes, ==, 40 + metadata->red_zone_bytes);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(module, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    hc_vm_arg_memory_t multi;
    hc_vm_get_arg_memory_raw(&context, &multi_u32, &multi);
    const uint32_t* buf = (const uint32_t*)multi.ro_memory;
    // Zero due to strict mode
    munit_assert_uint32(buf[0], ==, 0);
    // One due to set
    munit_assert_uint32(buf[1], ==, 1);
    // Zero due to padding and strict mode
    munit_assert_uint32(buf[2], ==, 0);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(module);

    return MUNIT_OK;
}

MunitResult vm_test_struct_indexing(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);
    hc_vm_function_t *module;
    ASSERT_OK(hc_vm_function_create(&module, NULL, NULL));
    munit_assert_ptr(module, !=, NULL);
    hc_vm_type_t struct_tp;
    const hc_vm_type_t* struct_type_list[] = {
      &hc_vm_type_byte,
      &hc_vm_type_byte,
      &hc_vm_type_uint,
      &hc_vm_type_uint,
      &hc_vm_type_byte,
    };
    hc_vm_type_init_struct((const hc_vm_type_t**)&struct_type_list, 5, &struct_tp);
    hc_vm_type_t u32_ptr_tp;
    hc_vm_type_init_pointer(&hc_vm_type_uint, &u32_ptr_tp, false);
    munit_assert_uint64(struct_tp.size_bytes, ==, 13);
    hc_vm_function_instruction_arg_t struct_val;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &struct_tp, &struct_val));
    uint64_t v1 = 0;
    hc_vm_function_instruction_arg_t zero_ul;
    hc_vm_function_data_immidiate(&v1, &zero_ul, &hc_vm_type_ulong);
    uint64_t v2 = 2;
    hc_vm_function_instruction_arg_t two_u32;
    hc_vm_function_data_immidiate(&v2, &two_u32, &hc_vm_type_uint);
    hc_vm_function_instruction_arg_t two_ul;
    hc_vm_function_data_immidiate(&v2, &two_ul, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t a;
    ASSERT_OK(hc_vm_function_instr_push_stack(module, &u32_ptr_tp, &a));
    ASSERT_OK(hc_vm_function_instr_struct_index(module, &struct_val, &two_ul, &a));
    ASSERT_OK(hc_vm_function_instr_set(module, &two_u32, &a));
    ASSERT_OK(hc_vm_function_instr_trap(module, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(module));

    const hc_vm_function_metadata_t* metadata = hc_vm_function_extract_metadata(module);
    munit_assert_uint64(metadata->required_stack_bytes, ==, 24 + metadata->red_zone_bytes);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(module, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    hc_vm_arg_memory_t target;
    hc_vm_get_arg_memory(&context, &a, &target);
    munit_assert_uint32(*(const uint32_t*)target.ro_memory, ==, 2);

    hc_vm_arg_memory_t struct_buf;
    hc_vm_get_arg_memory(&context, &struct_val, &struct_buf);
    const uint32_t* struct_memory = (const uint32_t*)struct_buf.ro_memory;
    munit_assert_uint32(struct_memory[0], ==, 0);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(module);

    return MUNIT_OK;
}

MunitResult vm_test_call(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);
    hc_vm_type_t ul_ptr_tp;
    hc_vm_type_init_pointer(&hc_vm_type_ulong, &ul_ptr_tp, false);
    hc_vm_function_t *sqr;
    hc_vm_function_instruction_arg_t arg;
    ASSERT_OK(hc_vm_function_create(&sqr, &ul_ptr_tp, &arg));
    munit_assert_ptr(sqr, !=, NULL);
    ASSERT_OK(hc_vm_function_instr_mul(sqr, &arg, &arg, &arg));
    ASSERT_OK(hc_vm_function_finalize(sqr));

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);
    uint64_t v1 = 2;
    hc_vm_function_instruction_arg_t two_ul;
    hc_vm_function_data_immidiate(&v1, &two_ul, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t a;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &a));
    ASSERT_OK(hc_vm_function_instr_set(main, &two_ul, &a));
    hc_vm_function_instruction_arg_t b;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ul_ptr_tp, &b));
    ASSERT_OK(hc_vm_function_instr_ref(main, &a, &b));
    ASSERT_OK(hc_vm_function_instr_call(main, sqr, &b));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    hc_vm_arg_memory_t target;
    hc_vm_get_arg_memory(&context, &a, &target);
    munit_assert_uint32(*(const uint32_t*)target.ro_memory, ==, 4);

    ASSERT_OK(hc_vm_function_drain_execution(&context));


    hc_vm_function_destroy(main);
    hc_vm_function_destroy(sqr);
    return MUNIT_OK;
}

MunitResult vm_test_simple_operations(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);
    uint64_t v1 = 1;
    hc_vm_function_instruction_arg_t one_ul;
    hc_vm_function_data_immidiate(&v1, &one_ul, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t one_eq_one;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_eq_one));
    // Test equality
    ASSERT_OK(hc_vm_function_instr_eq(main, &one_ul, &one_ul, &one_eq_one));
    hc_vm_function_instruction_arg_t one_shl_one;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_shl_one));
    ASSERT_OK(hc_vm_function_instr_shift_left(main, &one_ul, &one_ul, &one_shl_one));
    hc_vm_function_instruction_arg_t one_less_two;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_less_two));
    ASSERT_OK(hc_vm_function_instr_less(main, &one_ul, &one_shl_one, &one_less_two));
    hc_vm_function_instruction_arg_t one_greater_two;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_greater_two));
    ASSERT_OK(hc_vm_function_instr_greater(main, &one_ul, &one_shl_one, &one_greater_two));
    hc_vm_function_instruction_arg_t one_or_two;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_or_two));
    ASSERT_OK(hc_vm_function_instr_or(main, &one_ul, &one_shl_one, &one_or_two));
    hc_vm_function_instruction_arg_t one_and_two;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_and_two));
    ASSERT_OK(hc_vm_function_instr_and(main, &one_ul, &one_shl_one, &one_and_two));
    hc_vm_function_instruction_arg_t three_shr_one;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &three_shr_one));
    ASSERT_OK(hc_vm_function_instr_shift_right(main, &one_or_two, &one_ul, &three_shr_one));
    hc_vm_function_instruction_arg_t not_three;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &not_three));
    ASSERT_OK(hc_vm_function_instr_not(main, &one_or_two, &not_three));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    // 0xFF...
    ASSERT_INT_EQ(one_eq_one, uint64, -1ull);
    ASSERT_INT_EQ(one_shl_one, uint64, 2);
    // 0xFF...
    ASSERT_INT_EQ(one_less_two, uint64, -1ull);
    ASSERT_INT_EQ(one_greater_two, uint64, 0);
    ASSERT_INT_EQ(one_or_two, uint64, 3);
    ASSERT_INT_EQ(one_and_two, uint64, 0);
    ASSERT_INT_EQ(three_shr_one, uint64, 1);
    ASSERT_INT_EQ(not_three, uint64, ~3ull);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(main);

    return MUNIT_OK;
}

MunitResult vm_test_pointer_safety(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    hc_vm_type_t ulong_array;
    hc_vm_type_init_multi(&hc_vm_type_ulong, 1, &ulong_array);

    hc_vm_type_t ulong_struct;
    const hc_vm_type_t* subtypes[] = {
      &hc_vm_type_ulong,
    };
    hc_vm_type_init_struct((const hc_vm_type_t**)subtypes, 1, &ulong_struct);

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);
    hc_vm_function_instruction_arg_t is_ptrz;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &is_ptrz));
    uint64_t v0 = 0;
    hc_vm_function_instruction_arg_t zero_ul;
    hc_vm_function_data_immidiate(&v0, &zero_ul, &hc_vm_type_ulong);
    hc_vm_type_t ul_ptr_tp;
    hc_vm_type_init_pointer(&hc_vm_type_ulong, &ul_ptr_tp, false);
    hc_vm_function_instruction_arg_t ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ul_ptr_tp, &ptr));
    ASSERT_OK(hc_vm_function_instr_setzptr(main, &ptr));
    ASSERT_OK(hc_vm_function_instr_isnzptr(main, &ptr, &is_ptrz));
    ASSERT_OK(hc_vm_function_instr_not(main, &is_ptrz, &is_ptrz));
    ASSERT_OK(hc_vm_function_instr_ifnz(main, &is_ptrz));
    hc_vm_function_instruction_arg_t raw;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &raw));
    hc_vm_function_instruction_arg_t arr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_array, &arr));
    hc_vm_function_instruction_arg_t struct_;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_struct, &struct_));
    // Pointer outlives the value
    ASSERT_ERROR(hc_vm_function_instr_ref(main, &raw, &ptr));
    ASSERT_ERROR(hc_vm_function_instr_arr_index(main, &arr, &zero_ul, &ptr));
    ASSERT_ERROR(hc_vm_function_instr_struct_index(main, &struct_, &zero_ul, &ptr));
    ASSERT_OK(hc_vm_function_instr_endif(main));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    hc_vm_executor_context_t context;
    hc_vm_executor_flags_t flags;
    hc_vm_init_default_executor_flags(&flags);
    flags.zero_allocations = false;
    hc_vm_function_init_execution(main, &flags, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    ASSERT_INT_EQ(is_ptrz, uint64, ~(0ul));

    ASSERT_OK(hc_vm_function_drain_execution(&context));


    hc_vm_function_destroy(main);
    return MUNIT_OK;
}

MunitResult vm_test_loop(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);
    uint64_t zero = 0;
    hc_vm_function_instruction_arg_t zero_imm;
    hc_vm_function_data_immidiate(&zero, &zero_imm, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t acc;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &acc));
    ASSERT_OK(hc_vm_function_instr_set(main, &zero_imm, &acc));
    uint64_t upper_limit_val = 40;
    hc_vm_function_instruction_arg_t upper_limit_imm;
    hc_vm_function_data_immidiate(&upper_limit_val, &upper_limit_imm, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t upper_limit;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &upper_limit));
    ASSERT_OK(hc_vm_function_instr_set(main, &upper_limit_imm, &upper_limit));
    hc_vm_function_instruction_arg_t idx_i;
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &acc, &upper_limit, &idx_i));
    // Not allowed to modify the loop index
    ASSERT_ERROR(hc_vm_function_instr_set(main, &zero_imm, &idx_i));
    hc_vm_function_instruction_arg_t idx_j;
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &idx_i, &upper_limit, &idx_j));
    ASSERT_OK(hc_vm_function_instr_add(main, &acc, &idx_j, &acc));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));

    hc_vm_function_instruction_arg_t one_element_loop;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &one_element_loop));
    ASSERT_OK(hc_vm_function_instr_set(main, &zero_imm, &one_element_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &upper_limit, &upper_limit, &idx_i));
    ASSERT_OK(hc_vm_function_instr_set(main, &upper_limit, &one_element_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));

    hc_vm_function_instruction_arg_t zero_element_loop;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &zero_element_loop));
    ASSERT_OK(hc_vm_function_instr_set(main, &zero_imm, &zero_element_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &upper_limit, &zero_imm, &idx_i));
    ASSERT_OK(hc_vm_function_instr_set(main, &upper_limit, &zero_element_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));


    hc_vm_function_instruction_arg_t continue_loop;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &continue_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &zero_imm, &upper_limit, &idx_i));
    {
      hc_vm_function_instruction_arg_t is_idx_i_one;
      ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &is_idx_i_one));
      uint64_t one = 1;
      hc_vm_function_instruction_arg_t one_imm;
      hc_vm_function_data_immidiate(&one, &one_imm, &hc_vm_type_ulong);
      ASSERT_OK(hc_vm_function_instr_eq(main, &idx_i, &one_imm, &is_idx_i_one));
      ASSERT_OK(hc_vm_function_instr_ifnz(main, &is_idx_i_one));
      ASSERT_OK(hc_vm_function_instr_continue(main));
      ASSERT_OK(hc_vm_function_instr_endif(main));
    }
    ASSERT_OK(hc_vm_function_instr_add(main, &continue_loop, &idx_i, &continue_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));

    hc_vm_function_instruction_arg_t break_loop;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &break_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &zero_imm, &upper_limit, &idx_i));
    {
      hc_vm_function_instruction_arg_t is_idx_i_three;
      ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &is_idx_i_three));
      uint64_t three = 3;
      hc_vm_function_instruction_arg_t three_imm;
      hc_vm_function_data_immidiate(&three, &three_imm, &hc_vm_type_ulong);
      ASSERT_OK(hc_vm_function_instr_eq(main, &idx_i, &three_imm, &is_idx_i_three));
      ASSERT_OK(hc_vm_function_instr_ifnz(main, &is_idx_i_three));
      ASSERT_OK(hc_vm_function_instr_break(main));
      ASSERT_OK(hc_vm_function_instr_endif(main));
    }
    ASSERT_OK(hc_vm_function_instr_add(main, &break_loop, &idx_i, &break_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));


    uint64_t ulong_max_val = UINT64_MAX;
    hc_vm_function_instruction_arg_t ulong_max_imm;
    hc_vm_function_data_immidiate(&ulong_max_val, &ulong_max_imm, &hc_vm_type_ulong);
    uint64_t ulong_max_m1_val = UINT64_MAX - 1;
    hc_vm_function_instruction_arg_t ulong_max_m1_imm;
    hc_vm_function_data_immidiate(&ulong_max_m1_val, &ulong_max_m1_imm, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t ulong_max_loop;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &ulong_max_loop));
    ASSERT_OK(hc_vm_function_instr_set(main, &zero_imm, &ulong_max_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &ulong_max_m1_imm, &ulong_max_imm, &idx_i));
    ASSERT_OK(hc_vm_function_instr_set(main, &upper_limit, &ulong_max_loop));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    const hc_vm_function_metadata_t* meta = hc_vm_function_extract_metadata(main);
    munit_assert_true(meta->finite_run_time);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    /* Python
      acc = 0
      for i in range(acc, 41):
        for j in range(i, 41):
          acc += j
    */
    ASSERT_INT_EQ(acc, uint64, 22960);
    ASSERT_INT_EQ(one_element_loop, uint64, upper_limit_val);
    ASSERT_INT_EQ(zero_element_loop, uint64, 0);
    ASSERT_INT_EQ(ulong_max_loop, uint64, upper_limit_val);
    ASSERT_INT_EQ(continue_loop, uint64, 819);
    ASSERT_INT_EQ(break_loop, uint64, 3);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(main);

    return MUNIT_OK;
}

MunitResult vm_test_collatz_conjecture(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    hc_vm_function_t *collatz;
    ASSERT_OK(hc_vm_function_create(&collatz, NULL, NULL));
    munit_assert_ptr(collatz, !=, NULL);

    uint64_t arg_raw = 40;
    hc_vm_function_instruction_arg_t arg_imm;
    hc_vm_function_data_immidiate(&arg_raw, &arg_imm, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t n;
    ASSERT_OK(hc_vm_function_instr_push_stack(collatz, &hc_vm_type_ulong, &n));
    ASSERT_OK(hc_vm_function_instr_set(collatz, &arg_imm, &n));
    hc_vm_function_instruction_arg_t cnt;
    ASSERT_OK(hc_vm_function_instr_push_stack(collatz, &hc_vm_type_ulong, &cnt));
    uint64_t zero_raw = 0;
    hc_vm_function_instruction_arg_t zero_imm;
    hc_vm_function_data_immidiate(&zero_raw, &zero_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_set(collatz, &zero_imm, &cnt));
    ASSERT_OK(hc_vm_function_instr_loop(collatz));
    uint64_t one_raw = 1;
    hc_vm_function_instruction_arg_t one_imm;
    hc_vm_function_data_immidiate(&one_raw, &one_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_add(collatz, &cnt, &one_imm, &cnt));
    hc_vm_function_instruction_arg_t is_one;
    ASSERT_OK(hc_vm_function_instr_push_stack(collatz, &hc_vm_type_ulong, &is_one));
    ASSERT_OK(hc_vm_function_instr_less_eq(collatz, &n, &one_imm, &is_one));
    ASSERT_OK(hc_vm_function_instr_ifnz(collatz, &is_one));
    ASSERT_OK(hc_vm_function_instr_break(collatz));
    ASSERT_OK(hc_vm_function_instr_endif(collatz));

    hc_vm_function_instruction_arg_t is_even;
    ASSERT_OK(hc_vm_function_instr_push_stack(collatz, &hc_vm_type_ulong, &is_even));
    uint64_t two_raw = 2;
    hc_vm_function_instruction_arg_t two_imm;
    hc_vm_function_data_immidiate(&two_raw, &two_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_mod(collatz, &n, &two_imm, &is_even));
    ASSERT_OK(hc_vm_function_instr_eq(collatz, &is_even, &zero_imm, &is_even));
    ASSERT_OK(hc_vm_function_instr_ifnz(collatz, &is_even));
    ASSERT_OK(hc_vm_function_instr_shift_right(collatz, &n, &one_imm, &n));
    ASSERT_OK(hc_vm_function_instr_else(collatz));
    uint64_t three_raw = 3;
    hc_vm_function_instruction_arg_t three_imm;
    hc_vm_function_data_immidiate(&three_raw, &three_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_mul(collatz, &n, &three_imm, &n));
    ASSERT_OK(hc_vm_function_instr_add(collatz, &n, &one_imm, &n));
    ASSERT_OK(hc_vm_function_instr_endif(collatz));
    ASSERT_OK(hc_vm_function_instr_loop_end(collatz));
    ASSERT_OK(hc_vm_function_instr_trap(collatz, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(collatz));

    const hc_vm_function_metadata_t* meta = hc_vm_function_extract_metadata(collatz);
    munit_assert_false(meta->finite_run_time);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(collatz, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    ASSERT_INT_EQ(cnt, uint64, 9);

    ASSERT_OK(hc_vm_function_drain_execution(&context));



    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);

    ASSERT_OK(hc_vm_function_instr_call(main, collatz, NULL));
    ASSERT_OK(hc_vm_function_finalize(main));

    meta = hc_vm_function_extract_metadata(main);
    munit_assert_false(meta->finite_run_time);

    hc_vm_executor_flags_t flags;
    hc_vm_init_default_executor_flags(&flags);
    flags.ignore_traps = true;

    hc_vm_function_init_execution(main, &flags, NULL, &context);
    ASSERT_OK(hc_vm_function_drain_execution(&context));


    hc_vm_function_destroy(collatz);
    hc_vm_function_destroy(main);
    return MUNIT_OK;
}

MunitResult vm_test_extend(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    char* number = "123";

    hc_vm_type_t str;
    hc_vm_type_init_multi(&hc_vm_type_byte, 3, &str);

    hc_vm_type_t chr_ptr;
    hc_vm_type_init_pointer(&hc_vm_type_byte, &chr_ptr, false);

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);

    hc_vm_function_instruction_arg_t str_imm;
    hc_vm_function_data_immidiate(number, &str_imm, &str);
    hc_vm_function_instruction_arg_t sum;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &sum));
    uint64_t zero_raw = 0;
    hc_vm_function_instruction_arg_t zero_imm;
    hc_vm_function_data_immidiate(&zero_raw, &zero_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_set(main, &zero_imm, &sum));

    hc_vm_function_instruction_arg_t str_len;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &str_len));
    uint64_t one_raw = 1;
    hc_vm_function_instruction_arg_t one_imm;
    hc_vm_function_data_immidiate(&one_raw, &one_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_arr_length(main, &str_imm, &str_len));
    ASSERT_OK(hc_vm_function_instr_sub(main, &str_len, &one_imm, &str_len));
    hc_vm_function_instruction_arg_t idx_i;
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &zero_imm, &str_len, &idx_i));
    hc_vm_function_instruction_arg_t chr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &chr_ptr, &chr));
    ASSERT_OK(hc_vm_function_instr_arr_index(main, &str_imm, &idx_i, &chr));
    uint64_t zero_chr_raw = '0';
    hc_vm_function_instruction_arg_t zero_chr_imm;
    hc_vm_function_data_immidiate(&zero_chr_raw, &zero_chr_imm, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t temp;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &temp));
    ASSERT_OK(hc_vm_function_instr_extend(main, &chr, &temp));
    ASSERT_OK(hc_vm_function_instr_sub(main, &temp, &zero_chr_imm, &temp));
    ASSERT_OK(hc_vm_function_instr_add(main, &sum, &temp, &sum));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    const hc_vm_function_metadata_t* meta = hc_vm_function_extract_metadata(main);
    munit_assert_true(meta->finite_run_time);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, NULL, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    ASSERT_INT_EQ(sum, uint64, 6);

    ASSERT_OK(hc_vm_function_drain_execution(&context));


    hc_vm_function_destroy(main);
    return MUNIT_OK;
}

MunitResult vm_test_out_of_scope(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);
    uint64_t v0 = 0;
    hc_vm_function_instruction_arg_t zero_ul;
    hc_vm_function_data_immidiate(&v0, &zero_ul, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_ifnz(main, &zero_ul));
    hc_vm_function_instruction_arg_t raw;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &raw));
    ASSERT_OK(hc_vm_function_instr_endif(main));
    // Use out of scope
    ASSERT_ERROR(hc_vm_function_instr_set(main, &raw, &zero_ul));
    ASSERT_OK(hc_vm_function_instr_ifnz(main, &zero_ul));
    // Use in the scope of the same depth is still out of scope
    ASSERT_ERROR(hc_vm_function_instr_set(main, &raw, &zero_ul));
    ASSERT_OK(hc_vm_function_instr_endif(main));
    ASSERT_OK(hc_vm_function_finalize(main));

    hc_vm_function_destroy(main);
    return MUNIT_OK;
}

void read_byte_from_fd(hc_vm_arg_memory_t mem) {
  void* arg_struct = *(void**)mem.ro_memory;
  const hc_vm_type_t* arg_struct_tp = mem.type_ptr->subtypes[0];
  uint64_t return_value_offset = 0;
  hc_vm_type_struct_offset(arg_struct_tp, 1, &return_value_offset);
  uint64_t read_value_offset = 0;
  hc_vm_type_struct_offset(arg_struct_tp, 2, &read_value_offset);
  struct {
    const int* fd;
    uint64_t* return_value;
    char* read_char;
  } argument = {
    *(int**)arg_struct,
    (uint64_t*)(arg_struct + return_value_offset),
    (char*)(arg_struct + read_value_offset),
  };
  *argument.return_value = read(*argument.fd, (void*)argument.read_char, 1);
}

MunitResult vm_test_native_call(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    int fd = memfd_create("dummy_file", 0);

    write(fd, "123", 3);
    lseek(fd, 0, SEEK_SET);

    hc_vm_type_t ulong_ptr;
    hc_vm_type_init_pointer(&hc_vm_type_ulong, &ulong_ptr, false);
    hc_vm_type_t chr_ptr;
    hc_vm_type_init_pointer(&hc_vm_type_byte, &chr_ptr, false);
    hc_vm_type_t fd_ptr;
    hc_vm_type_init_pointer(&hc_vm_type_user_data, &fd_ptr, false);
    hc_vm_type_t fd_ptr_ptr;
    hc_vm_type_init_pointer(&fd_ptr, &fd_ptr_ptr, false);

    hc_vm_type_t struct_tp;
    const hc_vm_type_t* struct_type_list[] = {
      &fd_ptr,
      &hc_vm_type_ulong,
      &hc_vm_type_byte,
    };
    hc_vm_type_init_struct((const hc_vm_type_t**)&struct_type_list, 3, &struct_tp);
    hc_vm_type_t struct_ptr_tp;
    hc_vm_type_init_pointer(&struct_tp, &struct_ptr_tp, false);

    hc_vm_native_function_t read_nf = {
      &struct_ptr_tp,
      read_byte_from_fd
    };

    hc_vm_function_instruction_arg_t main_arg;

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, &fd_ptr, &main_arg));
    munit_assert_ptr(main, !=, NULL);

    hc_vm_function_instruction_arg_t sum;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &sum));
    uint64_t zero_raw = 0;
    hc_vm_function_instruction_arg_t zero_imm;
    hc_vm_function_data_immidiate(&zero_raw, &zero_imm, &hc_vm_type_ulong);
    ASSERT_OK(hc_vm_function_instr_set(main, &zero_imm, &sum));

    uint64_t max_len_raw = 10;
    hc_vm_function_instruction_arg_t max_len_imm;
    hc_vm_function_data_immidiate(&max_len_raw, &max_len_imm, &hc_vm_type_ulong);

    uint64_t one_raw = 1;
    hc_vm_function_instruction_arg_t one_imm;
    hc_vm_function_data_immidiate(&one_raw, &one_imm, &hc_vm_type_ulong);

    uint64_t two_raw = 2;
    hc_vm_function_instruction_arg_t two_imm;
    hc_vm_function_data_immidiate(&two_raw, &two_imm, &hc_vm_type_ulong);

    hc_vm_function_instruction_arg_t idx_i;
    ASSERT_OK(hc_vm_function_instr_loop_ascending(main, &zero_imm, &max_len_imm, &idx_i));
    hc_vm_function_instruction_arg_t read_arg;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &struct_tp, &read_arg));
    hc_vm_function_instruction_arg_t arg_fd_ptr_ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &fd_ptr_ptr, &arg_fd_ptr_ptr));
    ASSERT_OK(hc_vm_function_instr_struct_index(main, &read_arg, &zero_imm, &arg_fd_ptr_ptr));
    ASSERT_OK(hc_vm_function_instr_setptrindir(main, &main_arg, &arg_fd_ptr_ptr));

    hc_vm_function_instruction_arg_t read_arg_ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &struct_ptr_tp, &read_arg_ptr));
    ASSERT_OK(hc_vm_function_instr_ref(main, &read_arg, &read_arg_ptr));
    ASSERT_OK(hc_vm_function_instr_native_call(main, &read_nf, &read_arg_ptr));
    hc_vm_function_instruction_arg_t return_value;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_ptr, &return_value));
    ASSERT_OK(hc_vm_function_instr_struct_index(main, &read_arg, &one_imm, &return_value));
    ASSERT_OK(hc_vm_function_instr_ifnz(main, &return_value));
    hc_vm_function_instruction_arg_t chr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &chr_ptr, &chr));
    ASSERT_OK(hc_vm_function_instr_struct_index(main, &read_arg, &two_imm, &chr));

    uint64_t zero_chr_raw = '0';
    hc_vm_function_instruction_arg_t zero_chr_imm;
    hc_vm_function_data_immidiate(&zero_chr_raw, &zero_chr_imm, &hc_vm_type_ulong);
    hc_vm_function_instruction_arg_t temp;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_ulong, &temp));
    ASSERT_OK(hc_vm_function_instr_extend(main, &chr, &temp));
    ASSERT_OK(hc_vm_function_instr_sub(main, &temp, &zero_chr_imm, &temp));
    ASSERT_OK(hc_vm_function_instr_add(main, &sum, &temp, &sum));
    ASSERT_OK(hc_vm_function_instr_else(main));
    ASSERT_OK(hc_vm_function_instr_break(main));
    ASSERT_OK(hc_vm_function_instr_endif(main));
    ASSERT_OK(hc_vm_function_instr_loop_ascending_end(main));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    const hc_vm_function_metadata_t* meta = hc_vm_function_extract_metadata(main);
    munit_assert_true(meta->finite_run_time);

    int* fd_buf = &fd;
    hc_vm_arg_memory_t mem;
    hc_vm_init_arg_memory_ro(&fd_buf, &fd_ptr, &mem);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, &mem, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    ASSERT_INT_EQ(sum, uint64, 6);

    ASSERT_OK(hc_vm_function_drain_execution(&context));


    hc_vm_function_destroy(main);
    close(fd);
    return MUNIT_OK;
}

MunitResult vm_test_pointer_rules(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    hc_vm_type_t ulong_ptr;
    ASSERT_OK(hc_vm_type_init_pointer(&hc_vm_type_ulong, &ulong_ptr, false));
    hc_vm_type_t ulong_ptr_ptr;
    ASSERT_OK(hc_vm_type_init_pointer(&ulong_ptr, &ulong_ptr_ptr, false));
    // Cannot create depth 3 pointers
    hc_vm_type_t ulong_ptr_ptr_ptr;
    ASSERT_ERROR(hc_vm_type_init_pointer(&ulong_ptr_ptr, &ulong_ptr_ptr_ptr, false));

    hc_vm_type_t struct_tp;
    const hc_vm_type_t* struct_type_list[] = {
      &hc_vm_type_ulong,
      &ulong_ptr,
      &ulong_ptr,
    };
    hc_vm_type_init_struct((const hc_vm_type_t**)&struct_type_list, 3, &struct_tp);
    hc_vm_type_t struct_ptr_tp;
    hc_vm_type_init_pointer(&struct_tp, &struct_ptr_tp, false);

    hc_vm_function_instruction_arg_t main_arg;

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, &struct_ptr_tp, &main_arg));
    munit_assert_ptr(main, !=, NULL);

    hc_vm_function_instruction_arg_t a_ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_ptr, &a_ptr));

    uint64_t zero_raw = 0;
    hc_vm_function_instruction_arg_t zero_imm;
    hc_vm_function_data_immidiate(&zero_raw, &zero_imm, &hc_vm_type_ulong);

    uint64_t one_raw = 1;
    hc_vm_function_instruction_arg_t one_imm;
    hc_vm_function_data_immidiate(&one_raw, &one_imm, &hc_vm_type_ulong);

    uint64_t two_raw = 2;
    hc_vm_function_instruction_arg_t two_imm;
    hc_vm_function_data_immidiate(&two_raw, &two_imm, &hc_vm_type_ulong);

    ASSERT_OK(hc_vm_function_instr_struct_index(main, &main_arg, &zero_imm, &a_ptr));
    ASSERT_OK(hc_vm_function_instr_set(main, &one_imm, &a_ptr));

    hc_vm_function_instruction_arg_t b_ptr_ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_ptr_ptr, &b_ptr_ptr));
    ASSERT_OK(hc_vm_function_instr_struct_index(main, &main_arg, &one_imm, &b_ptr_ptr));

    // Invalid because local pointer escapes
    ASSERT_ERROR(hc_vm_function_instr_setptrindir(main, &a_ptr, &b_ptr_ptr));

    hc_vm_function_instruction_arg_t c_ptr_ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_ptr_ptr, &c_ptr_ptr));
    ASSERT_OK(hc_vm_function_instr_struct_index(main, &main_arg, &two_imm, &c_ptr_ptr));

    hc_vm_function_instruction_arg_t c_ptr;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &ulong_ptr, &c_ptr));
    ASSERT_OK(hc_vm_function_instr_derefptr(main, &c_ptr_ptr, &c_ptr));

    // Invalid since external pointers have unknown lifetimes
    ASSERT_ERROR(hc_vm_function_instr_setptrindir(main, &c_ptr, &b_ptr_ptr));
    ASSERT_OK(hc_vm_function_instr_trap(main, (void*)dummy_trap_info));
    ASSERT_OK(hc_vm_function_finalize(main));

    const hc_vm_function_metadata_t* meta = hc_vm_function_extract_metadata(main);
    munit_assert_true(meta->finite_run_time);

    unsigned char buf[struct_tp.size_bytes];
    memset(buf, 0, struct_tp.size_bytes);
    void** buf_ptr = &buf;
    hc_vm_arg_memory_t mem;
    hc_vm_init_arg_memory_rw((void*)&buf_ptr, &struct_ptr_tp, &mem);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, &mem, &context);

    ASSERT_TRAP(hc_vm_function_drain_execution(&context));

    munit_assert_uint64(*(uint64_t*)buf, ==, 1);
    uint64_t ptr_offset;
    hc_vm_type_struct_offset(&struct_tp, 1, &ptr_offset);
    munit_assert_ptr_equal(*(void**)(buf + ptr_offset), NULL);
    hc_vm_type_struct_offset(&struct_tp, 2, &ptr_offset);
    munit_assert_ptr_equal(*(void**)(buf + ptr_offset), NULL);

    ASSERT_OK(hc_vm_function_drain_execution(&context));

    hc_vm_function_destroy(main);
    return MUNIT_OK;
}

MunitResult vm_test_division_safety(const MunitParameter params[], void* user_data_or_fixture) {
    UNUSED(params);
    UNUSED(user_data_or_fixture);

    uint64_t zero_raw_double = 0;
    hc_vm_function_instruction_arg_t zero_double_imm;
    hc_vm_function_data_immidiate(&zero_raw_double, &zero_double_imm, &hc_vm_type_double);

    hc_vm_function_t *main;
    ASSERT_OK(hc_vm_function_create(&main, NULL, NULL));
    munit_assert_ptr(main, !=, NULL);

    hc_vm_function_instruction_arg_t result;
    ASSERT_OK(hc_vm_function_instr_push_stack(main, &hc_vm_type_double, &result));
    ASSERT_OK(hc_vm_function_instr_div(main, &zero_double_imm, &zero_double_imm, &result));
    // Not defined for doubles
    ASSERT_ERROR(hc_vm_function_instr_mod(main, &zero_double_imm, &zero_double_imm, &result));
    ASSERT_OK(hc_vm_function_finalize(main));

    const hc_vm_function_metadata_t* meta = hc_vm_function_extract_metadata(main);
    munit_assert_true(meta->finite_run_time);

    hc_vm_executor_context_t context;
    hc_vm_function_init_execution(main, NULL, NULL, &context);

    munit_assert_uint64(hc_vm_function_drain_execution(&context), ==, HC_VM_EXEC_ZERO_VAL);


    hc_vm_function_destroy(main);
    return MUNIT_OK;
}

static MunitTest hc_vm_tests[] = {
  {
    "/simple", /* name */
    simple_vm_test, /* test */
    NULL, /* setup */
    NULL, /* tear_down */
    MUNIT_TEST_OPTION_NONE, /* options */
    NULL /* parameters */
  },
  {
    "/simple_operations_check",
    vm_test_simple_operations,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/pointer_safety_check",
    vm_test_pointer_safety,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/type_check",
    vm_test_pointer_and_type,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/branching_check",
    vm_test_branching,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/struct_indexing_check",
    vm_test_struct_indexing,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/array_indexing_check",
    vm_test_array_indexing,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/call_check",
    vm_test_call,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/loop_check",
    vm_test_loop,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/collatz_conjecture",
    vm_test_collatz_conjecture,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/extend_check",
    vm_test_extend,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/out_of_scope_use_check",
    vm_test_out_of_scope,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/native_call_check",
    vm_test_native_call,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/pointer_rules_check",
    vm_test_pointer_rules,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    "/division_safety_check",
    vm_test_division_safety,
    NULL,
    NULL,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  /* Mark the end of the array with an entry where the test
   * function is NULL */
  { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

MunitSuite hc_vm_test_suite = {
    "/vm", /* name */
    hc_vm_tests, /* tests */
    NULL, /* suites */
    1, /* iterations */
    MUNIT_SUITE_OPTION_NONE /* options */
};