#define MUNIT_ENABLE_ASSERT_ALIASES
#include <munit.h>

MunitResult dummy_test(const MunitParameter params[], void* user_data_or_fixture) {
    return MUNIT_OK;
}

static MunitTest tests[] = {
  {
    "/dummy-test", /* name */
    dummy_test, /* test */
    NULL, /* setup */
    NULL, /* tear_down */
    MUNIT_TEST_OPTION_NONE, /* options */
    NULL /* parameters */
  },
  /* Mark the end of the array with an entry where the test
   * function is NULL */
  { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

extern MunitSuite hc_vm_test_suite;

int main(int argc, char** argv) {
    const MunitSuite suites[] = {
      {
          "/dummy-suite", /* name */
          tests, /* tests */
          NULL, /* suites */
          1, /* iterations */
          MUNIT_SUITE_OPTION_NONE /* options */
      },
      hc_vm_test_suite,
      { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
    };

    const MunitSuite main_suite = {
      "/hctest", /* name */
      NULL, /* tests */
      suites, /* suites */
      1, /* iterations */
      MUNIT_SUITE_OPTION_NONE /* options */
    };

    return munit_suite_main(&main_suite, NULL, argc, argv);;
}