#include "hc_vm_types.h"
#include <stdio.h>

const uint64_t hc_vm_word_size = 8;
const uint64_t hc_vm_max_aligment = 8;

uint64_t hc_vm_type_align(uint64_t *end_offset, uint64_t aligment) {
    uint64_t left_over = (*end_offset) % aligment;
    if (left_over == 0) {
        return 0;
    }
    uint64_t extra = aligment - left_over;
    if (UINT64_MAX - (*end_offset) < extra) {
        return 1;
    }
    *end_offset += extra;
    return 0;
}

// Define basic types, so they can get reused
const hc_vm_type_t hc_vm_type_uint = {
    HC_VM_TYPE_UINT
    , 4 /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 4 /* aligment */
};

const hc_vm_type_t hc_vm_type_int = {
    HC_VM_TYPE_INT
    , 4 /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 4 /* aligment */
};

const hc_vm_type_t hc_vm_type_ulong = {
    HC_VM_TYPE_ULONG
    , 8 /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 8 /* aligment */
};

const hc_vm_type_t hc_vm_type_long = {
    HC_VM_TYPE_LONG
    , 8 /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 8 /* aligment */
};

const hc_vm_type_t hc_vm_type_uword = {
    HC_VM_TYPE_UWORD
    , hc_vm_word_size /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , hc_vm_word_size /* aligment */
};

const hc_vm_type_t hc_vm_type_word = {
    HC_VM_TYPE_WORD
    , hc_vm_word_size /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , hc_vm_word_size /* aligment */
};

const hc_vm_type_t hc_vm_type_double = {
    HC_VM_TYPE_DOUBLE
    , 8 /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 8 /* aligment */
};

const hc_vm_type_t hc_vm_type_byte = {
    HC_VM_TYPE_BYTE
    , 1 /* size */
    , 1 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 1 /* aligment */
};

const hc_vm_type_t hc_vm_type_user_data = {
    HC_VM_TYPE_USER_DATA
    , 0 /* size */
    , 0 /* element_count */
    , NULL /* subtypes */
    , NULL /* stored_tp */
    , true /* is_global_compatible */
    , 0 /* aligment */
};

static uint64_t hc_vm_type_struct_offset_impl(const hc_vm_type_t** subtypes, uint64_t idx, uint64_t *offset) {
    uint64_t subsize = subtypes[idx]->size_bytes;
    uint64_t type_align = subtypes[idx]->aligment_bytes;
    if (hc_vm_type_align(offset, type_align)) return 1;
    if (UINT64_MAX - *offset < subsize) {
        // Size overflow
        return 1;
    }
    *offset += subsize;
    return 0;
}

uint64_t hc_vm_type_get_deref(const hc_vm_type_t* source_type, const hc_vm_type_t** rw) {
    if (source_type->type == HC_VM_TYPE_POINTER) {
        *rw = source_type->subtypes[0];
    } else {
        *rw = source_type;
    }
    return 0;
}

uint64_t hc_vm_assert_types_compatible(const hc_vm_type_t* a, const hc_vm_type_t* b) {
    const hc_vm_type_t* a_deref = NULL;
    const hc_vm_type_t* b_deref = NULL;
    hc_vm_type_get_deref(a, &a_deref);
    hc_vm_type_get_deref(b, &b_deref);
    if (a_deref == b_deref) {
        return 0;
    } else {
        return 1;
    }
}

uint64_t hc_vm_type_init_pointer(const hc_vm_type_t* desired_type, hc_vm_type_t* result, bool is_global_compatible) {
    if (desired_type == NULL || result == NULL) {
        // Missing pointers
        return 1;
    }
    if (desired_type->type == HC_VM_TYPE_POINTER && desired_type->subtypes[0]->type == HC_VM_TYPE_POINTER) {
        // Pointer is too deep
        return 1;
    }
    *result = (hc_vm_type_t){
        HC_VM_TYPE_POINTER,
        hc_vm_word_size,
        1,
        &result->stored_tp,
        desired_type,
        is_global_compatible,
        hc_vm_word_size
    };
    return 0;
}

uint64_t hc_vm_type_init_multi(const hc_vm_type_t* desired_type, uint64_t element_count, hc_vm_type_t* result) {
    if (desired_type == NULL || result == NULL) {
        // Missing pointers
        return 1;
    }
    if (desired_type->size_bytes == 0) return 1;
    uint64_t real_size = 0;
    if (hc_vm_type_array_index_offset(desired_type, 1, &real_size)) return 1;
    if (UINT64_MAX / element_count < real_size) {
        // Size overflow
        return 1;
    }
    *result = (hc_vm_type_t){
        HC_VM_TYPE_MULTI,
        real_size * element_count,
        element_count,
        &result->stored_tp,
        desired_type,
        desired_type->is_global_compatible,
        desired_type->aligment_bytes
    };
    return 0;
}

uint64_t hc_vm_type_init_dyn_array(const hc_vm_type_t* desired_type, hc_vm_type_t* result) {
    if (desired_type == NULL || result == NULL) {
        // Missing pointers
        return 1;
    }
    if (desired_type->size_bytes == 0) return 1;
    *result = (hc_vm_type_t){
        HC_VM_TYPE_DYN_ARRAY,
        0, /* fake */
        0, /* fake */
        &result->stored_tp,
        desired_type,
        desired_type->is_global_compatible,
        0 /*fake*/
    };
    return 0;
}

uint64_t hc_vm_type_init_struct(const hc_vm_type_t** desired_struct, uint64_t element_count, hc_vm_type_t* result) {
    if (desired_struct == NULL || result == NULL) {
        // Missing pointers
        return 1;
    }
    uint64_t size_bytes = 0;
    bool is_global_compatible = true;
    uint64_t max_align = 1;
    for(uint64_t t = 0; t < element_count; ++t) {
        if (desired_struct[t]->size_bytes == 0) return 1;
        if (hc_vm_type_struct_offset_impl(desired_struct, t, &size_bytes)) return 1;
        uint64_t type_align = desired_struct[t]->aligment_bytes;
        if (max_align < type_align) max_align = type_align;
        is_global_compatible &= desired_struct[t]->is_global_compatible;
    }
    *result = (hc_vm_type_t){
        HC_VM_TYPE_STRUCT,
        size_bytes,
        element_count,
        desired_struct,
        NULL,
        is_global_compatible,
        max_align
    };
    return 0;
}

uint64_t hc_vm_type_array_index_offset(const hc_vm_type_t* element_type, uint64_t idx, uint64_t *offset) {
    uint64_t real_size = element_type->size_bytes;
    if (hc_vm_type_align(&real_size, element_type->aligment_bytes)) return 1;
    if (UINT64_MAX / real_size < idx) return 1;
    *offset = real_size * idx;
    return 0;
}

uint64_t hc_vm_type_struct_offset(const hc_vm_type_t* st, uint64_t idx, uint64_t *offset) {
    *offset = 0;
    for(uint64_t c_idx = 0; c_idx <= idx; ++c_idx) {
        if (hc_vm_type_struct_offset_impl(st->subtypes, c_idx, offset)) return 1;
    }
    *offset -= st->subtypes[idx]->size_bytes;
    return 0;
}
