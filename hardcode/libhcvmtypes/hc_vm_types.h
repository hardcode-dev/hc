#pragma once
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

enum {
    HC_VM_TYPE_BYTE,
    HC_VM_TYPE_UINT,
    HC_VM_TYPE_ULONG,
    HC_VM_TYPE_UWORD,
    HC_VM_TYPE_MAX_UNSIGNED_TYPES,
    HC_VM_TYPE_INT,
    HC_VM_TYPE_LONG,
    HC_VM_TYPE_WORD,
    HC_VM_TYPE_DOUBLE,
    HC_VM_TYPE_MAX_PRIMITIVE_TYPES,
    HC_VM_TYPE_USER_DATA,
    HC_VM_TYPE_POINTER,
    HC_VM_TYPE_MULTI, // static array of fixed size
    HC_VM_TYPE_STRUCT,
    HC_VM_TYPE_DYN_ARRAY,
    HC_VM_TYPE_MAX,
};

typedef struct hc_vm_type_t {
    uint8_t type;
    uint64_t size_bytes;
    uint64_t element_count;
    const struct hc_vm_type_t** subtypes;
    // Intrusive storage for HC_VM_POINTER
    const struct hc_vm_type_t* stored_tp;
    bool is_global_compatible;
    uint64_t aligment_bytes;
} hc_vm_type_t;

extern const uint64_t hc_vm_word_size;
extern const uint64_t hc_vm_max_aligment;

// Declare basic types, so they can get reused
extern const hc_vm_type_t hc_vm_type_byte;
extern const hc_vm_type_t hc_vm_type_uint;
extern const hc_vm_type_t hc_vm_type_int;
extern const hc_vm_type_t hc_vm_type_long;
extern const hc_vm_type_t hc_vm_type_ulong;
extern const hc_vm_type_t hc_vm_type_word;
extern const hc_vm_type_t hc_vm_type_uword;
extern const hc_vm_type_t hc_vm_type_double;
extern const hc_vm_type_t hc_vm_type_user_data;

// Gets type of the derefenced value, if value is not a pointer, same type is returned
uint64_t hc_vm_type_get_deref(const hc_vm_type_t* source_type, const hc_vm_type_t** rw);

// Two types are compatible if they write to the same type
uint64_t hc_vm_assert_types_compatible(const hc_vm_type_t* a, const hc_vm_type_t* b);

uint64_t hc_vm_type_init_pointer(const hc_vm_type_t* desired_type, hc_vm_type_t* result, bool is_global_compatible);

uint64_t hc_vm_type_init_multi(const hc_vm_type_t* desired_type, uint64_t element_count, hc_vm_type_t* result);

uint64_t hc_vm_type_init_dyn_array(const hc_vm_type_t* desired_type, hc_vm_type_t* result);

uint64_t hc_vm_type_init_struct(const hc_vm_type_t** desired_struct, uint64_t element_count, hc_vm_type_t* result);

// return error, result is in end_offset
uint64_t hc_vm_type_align(uint64_t *end_offset, uint64_t aligment);

uint64_t hc_vm_type_array_index_offset(const hc_vm_type_t* element_type, uint64_t idx, uint64_t *offset);
uint64_t hc_vm_type_struct_offset(const hc_vm_type_t* st, uint64_t idx, uint64_t *offset);
