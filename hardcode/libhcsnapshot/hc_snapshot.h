#pragma once

#include <stddef.h>

typedef struct hc_snapshot_t hc_snapshot_t;

typedef struct hc_module_t hc_module_t;

extern hc_snapshot_t *hc_snapshot_load (const char *path, char *error_message, size_t max_error_message_len);

extern void hc_snapshot_destroy (hc_snapshot_t *s);

extern hc_module_t *hc_snapshot_root (hc_snapshot_t *s);

extern int hc_snapshot_parse (hc_snapshot_t *s);

extern int hc_snapshot_collect_symbols (hc_snapshot_t *s);

extern int hc_snapshot_sem_analyze (hc_snapshot_t *s);
