#include "hc_snapshot.h"

#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_snapshot_internal.h"

#include "hc_parser.h"

#include <stdio.h>
#include <stdlib.h>

typedef struct hc_module_map_t hc_module_map_t;

static int parse_self_and_children (const char *name, hc_module_t *module, void *ud);

int hc_snapshot_parse (hc_snapshot_t *s) { return parse_self_and_children (".", s->root, NULL); }

static void *add_ir_instruction_to_stack (hc_syn_rpn_t *syn_rpn, hc_parser_instruction_t *instruction)
{
    hc_syn_rpn_move_instruction (syn_rpn, instruction->type, instruction->source_off, instruction->source_size, instruction->value);
    return NULL;
}

static void *syn_error_callback (void *ud, hc_parser_error_t *error)
{
    (void)ud;

    fprintf (stderr, "Failed to parse: %s at byte range [%d..%d]\n", error->message, error->source_off, error->source_off + error->source_size - 1);
    return NULL;
}

static int parse_section (hc_source_section_t *section, int section_type, hc_syn_rpn_t **dest_syn_rpn, char *error_message, size_t max_error_message_len)
{
    if (!section->data)
        return 0;

    hc_syn_rpn_t *syn_rpn = hc_syn_rpn_create ();
    hc_object_stream_t syn_rpn_stream = hc_object_stream_init ((void *)add_ir_instruction_to_stack, syn_rpn);
    hc_object_stream_t syn_error_stream = hc_object_stream_init ((void *)syn_error_callback, NULL);

    if (!hc_parse_buffer (section->data, section->len, section_type, &syn_rpn_stream, &syn_error_stream)) {
        // TODO: Collect parse errors and give it here
        snprintf (error_message, max_error_message_len, "Failed to parse '%.*s'", (int)section->len, (char *)section->data);
        hc_syn_rpn_destroy (syn_rpn);
        return -1;
    }
    *dest_syn_rpn = syn_rpn;

    return 0;
}

static int parse_self (const char *name, hc_module_t *module)
{
    (void)name;

    hc_module_source_t *source = hc_module_source (module);
    hc_module_syn_t *syn = hc_module_syn (module);

    char error_message[512];

    if (parse_section (&source->_struct, HC_PARSER_STRUCT, &syn->_struct, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to parse struct: %s\n", error_message);
        return -1;
    }
    if (parse_section (&source->_class, HC_PARSER_CLASS, &syn->_class, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to parse class: %s\n", error_message);
        return -1;
    }
    /* if (parse_section (&source->_bind, HC_PARSER_BINDING, &syn->_bind, error_message, sizeof (error_message))) { */
    /*     fprintf (stderr, "Failed to parse binding: %s\n", error_message); */
    /*     return -1; */
    /* } */
    if (parse_section (&source->function_return_type, HC_PARSER_FUNCTION_RETURN_TYPE, &syn->function_return_type, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to parse function return type: %s\n", error_message);
        return -1;
    }
    if (parse_section (&source->function_parameters, HC_PARSER_FUNCTION_PARAMETERS, &syn->function_parameters, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to parse function parameters: %s\n", error_message);
        return -1;
    }
    if (parse_section (&source->function_body, HC_PARSER_FUNCTION_BODY, &syn->function_body, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to parse function body: %s\n", error_message);
        return -1;
    }
    if (parse_section (&source->function_kind, HC_PARSER_FUNCTION_KIND, &syn->function_kind, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to parse function kind: %s\n", error_message);
        return -1;
    }

    return 0;
}

static int parse_self_and_children (const char *name, hc_module_t *module, void *ud)
{
    (void)ud;

    if (parse_self (name, module))
        return -1;

    return hc_module_map_foreach (hc_module_children (module), parse_self_and_children, NULL);
}
