#pragma once

typedef struct hc_module_t hc_module_t;

typedef struct hc_snapshot_t {
    hc_module_t *root;
} hc_snapshot_t;
