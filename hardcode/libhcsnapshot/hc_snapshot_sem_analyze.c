#include "hc_snapshot.h"

#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_sem_analyzer.h"
#include "hc_snapshot_internal.h"

#include <stdio.h>
#include <stdlib.h>

typedef struct hc_module_map_t hc_module_map_t;

typedef struct analyze_error_ud_t {
    int target;
} analyze_error_ud_t;

static const char *analyzer_target_str(int target) {
    switch (target) {
    // case HC_SEM_ANALYZER_STRUCT:
    //     return "struct";
    // case HC_SEM_ANALYZER_BINDING:
    //     return "binding";
    case HC_SEM_ANALYZER_FUNCTION_KIND:
        return "function kind";
    case HC_SEM_ANALYZER_FUNCTION_RETURN_TYPE:
        return "function return type";
    case HC_SEM_ANALYZER_FUNCTION_PARAMETERS:
        return "function parameters";
    case HC_SEM_ANALYZER_FUNCTION_BODY:
        return "function body";
    case HC_SEM_ANALYZER_CLASS:
        return "class";
    default:
        return "unknown";
    }
}

static int analyze_self_and_children(const char *name, hc_module_t *module, void *ud);

int hc_snapshot_sem_analyze(hc_snapshot_t *s) {
    /* { */
    /*     analyze_ud_t analyze_ud = { .target = HC_SEM_ANALYZER_STRUCT }; */
    /*     if (analyze_self_and_children (".", s->root, &analyze_ud)) */
    /*         return -1; */
    /* } */
    /* { */
    /*     analyze_ud_t analyze_ud = { .target = HC_SEM_ANALYZER_BINDING }; */
    /*     if (analyze_self_and_children (".", s->root, &analyze_ud)) */
    /*         return -1; */
    /* } */
    {
        analyze_error_ud_t analyze_error_ud = {.target = HC_SEM_ANALYZER_FUNCTION_KIND};
        if (analyze_self_and_children(".", s->root, &analyze_error_ud))
            return -1;
    }
    {
        analyze_error_ud_t analyze_error_ud = {.target = HC_SEM_ANALYZER_FUNCTION_RETURN_TYPE};
        if (analyze_self_and_children(".", s->root, &analyze_error_ud))
            return -1;
    }
    {
        analyze_error_ud_t analyze_error_ud = {.target = HC_SEM_ANALYZER_FUNCTION_PARAMETERS};
        if (analyze_self_and_children(".", s->root, &analyze_error_ud))
            return -1;
    }
    {
        analyze_error_ud_t analyze_error_ud = {.target = HC_SEM_ANALYZER_CLASS};
        if (analyze_self_and_children(".", s->root, &analyze_error_ud))
            return -1;
    }
    {
        analyze_error_ud_t analyze_error_ud = {.target = HC_SEM_ANALYZER_FUNCTION_BODY};
        if (analyze_self_and_children(".", s->root, &analyze_error_ud))
            return -1;
    }

    return 0;
}

typedef struct hc_syn_insn_scanner_t {
    const hc_syn_insn_t *insn;
    const hc_syn_insn_t *einsn;
} hc_syn_insn_scanner_t;

static void hc_syn_insn_scanner_init(hc_syn_insn_scanner_t *scanner, const hc_syn_rpn_t *syn_rpn) {
    const hc_syn_insn_t *insn = hc_syn_rpn_get_instructions((hc_syn_rpn_t *)syn_rpn);
    scanner->insn = insn;
    scanner->einsn = insn + hc_syn_rpn_get_instruction_count(syn_rpn);
}

static const hc_syn_insn_t *hc_syn_insn_scan(hc_syn_insn_scanner_t *scanner) {
    if (scanner->insn == scanner->einsn)
        return NULL;
    return scanner->insn++;
}

static void add_sem_instruction_to_rpn(hc_sem_rpn_t *sem_rpn, int type, int token_off, int token_size, const hc_sem_insn_value_t *value) { hc_sem_rpn_move_instruction(sem_rpn, type, token_off, token_size, value); }

static void sem_error_callback(void *ud, int source_off, int source_size, const char *message) {
    analyze_error_ud_t *analyze_ud = ud;
    fprintf(stderr, "Failed to analyze %s: %s at byte range [%d..%d]\n", analyzer_target_str(analyze_ud->target), message, source_off, source_off + source_size - 1);
}

static hc_sem_rpn_t *sem_analyze(int target, hc_syn_rpn_t *syn_rpn) {
    hc_syn_insn_scanner_t syn_insn_scanner;
    hc_syn_insn_scanner_init(&syn_insn_scanner, syn_rpn);

    hc_sem_rpn_t *sem_rpn = hc_sem_rpn_create();

    analyze_error_ud_t analyze_error_ud = {.target = target};

    if (hc_sem_analyze(target, &syn_insn_scanner, (hc_sem_analyzer_scan_syn_instruction_t)hc_syn_insn_scan, sem_rpn, (hc_sem_analyzer_emit_instruction_t)add_sem_instruction_to_rpn, &analyze_error_ud, sem_error_callback)) {
        fprintf(stderr, "FAIL:(\n");
        return NULL;
    }
    return sem_rpn;
}
static int analyze_self(const char *name, hc_module_t *module, int target) {
    (void)name;
    hc_module_syn_t *syn = hc_module_syn(module);
    hc_module_sem_t *sem = hc_module_sem(module);
    switch (target) {
    // case HC_SEM_ANALYZER_STRUCT: {
    //     if (syn->_struct) {
    //         hc_sem_rpn_t *sem_rpn = sem_analyze(target, syn->_struct);
    //         if (!sem_rpn)
    //             return -1;
    //         sem->_struct = sem_rpn;
    //     }
    // } break;
    // case HC_SEM_ANALYZER_BINDING: {
    //     if (syn->_bind) {
    //         hc_sem_rpn_t *sem_rpn = sem_analyze(target, syn->_bind);
    //         if (!sem_rpn)
    //             return -1;
    //         sem->_bind = sem_rpn;
    //     }
    // } break;
    case HC_SEM_ANALYZER_FUNCTION_KIND: {
        
    } break;
    case HC_SEM_ANALYZER_FUNCTION_RETURN_TYPE: {
        if (syn->function_return_type) {
            hc_sem_rpn_t *sem_rpn = sem_analyze(target, syn->function_return_type);
            if (!sem_rpn)
                return -1;
            sem->function_return_type = sem_rpn;
        }
    } break;
    case HC_SEM_ANALYZER_FUNCTION_PARAMETERS: {
        if (syn->function_parameters) {
            hc_sem_rpn_t *sem_rpn = sem_analyze(target, syn->function_parameters);
            if (!sem_rpn)
                return -1;
            sem->function_parameters = sem_rpn;
        }
    } break;
    case HC_SEM_ANALYZER_FUNCTION_BODY: {
        if (syn->function_body) {
            hc_sem_rpn_t *sem_rpn = sem_analyze(target, syn->function_body);
            if (!sem_rpn)
                return -1;
            sem->function_body = sem_rpn;
        }
    } break;
    default: {
        fprintf(stderr, "Unsupported target %d for sem analyzer\n", target);
    }
        return -1;
    }
    return 0;
}
static int analyze_self_and_children(const char *name, hc_module_t *module, void *ud) {
    analyze_error_ud_t *analyze_error_ud = ud;
    if (analyze_self(name, module, analyze_error_ud->target))
        return -1;
    return hc_module_map_foreach(hc_module_children(module), analyze_self_and_children, ud);
}
