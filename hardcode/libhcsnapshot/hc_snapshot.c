#include "hc_snapshot.h"

#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_snapshot_internal.h"
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define strnequalliteral(str, size, literal) ((size) == (sizeof (literal) - sizeof ("")) && !memcmp ((str), (literal), (size)))

static int hc_module_load_children (hc_module_t *module, const char *children_dname, char *error_message, size_t max_error_message_len);

hc_snapshot_t *hc_snapshot_load (const char *path, char *error_message, size_t max_error_message_len)
{
    printf ("Loading snapshot %s \n", path);

    hc_module_t *root = hc_module_create ();
    if (hc_module_load_children (root, path, error_message, max_error_message_len)) {
        printf ("FAILED AND DETROYING\n");
        hc_module_destroy (root);
        return NULL;
    }

    hc_snapshot_t *s = malloc (sizeof (hc_snapshot_t));
    printf ("Malloc-ed snapshot %s \n", path);
    s->root = root;
    return s;
}

void hc_snapshot_destroy (hc_snapshot_t *s)
{
    if (!s)
        return;
    hc_module_destroy (s->root);
    free (s);
}

hc_module_t *hc_snapshot_root (hc_snapshot_t *s) { return s->root; }

static int module_name_is_valid (const char *name)
{
    char c = *(name++);
    if (c != '_' && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z'))
        return 0;

    while ((c = *(name++)))
        if (c != '_' && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
            return 0;
    return 1;
}

static char hc_module_load_description (hc_module_t *module, const char *description_fname)
{
    hc_module_source_t *module_source = hc_module_source (module);
    int fd = open (description_fname, O_RDONLY);

    if (fd == -1) {
        fprintf (stderr, "ERROR: Failed to open file '%s': %s\n", description_fname, strerror (errno));
        return 0;
    }

    struct stat st;
    if (fstat (fd, &st)) {
        fprintf (stderr, "ERROR: Failed to open file '%s': %s\n", description_fname, strerror (errno));
        close (fd);
        return 0;
    }

    size_t size = st.st_size;
    char *data = mmap (NULL, size, PROT_READ, MAP_PRIVATE | MAP_FILE, fd, 0);
    if (data == MAP_FAILED) {
        fprintf (stderr, "ERROR: Failed to map file '%s': %s\n", description_fname, strerror (errno));
        close (fd);
        return 0;
    }

    const char *edata = data + size;
    hc_source_section_t *section = NULL;

    while (data < edata) {
        if (section) {
            // Parse section content
            const char *content = data;
            while (data < edata) {
                if (*data == '@')
                    break;
                ++data;
            }

            const char *econtent = data;
            if (data >= edata) {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected EOF\nDidn't find the end of the section!\n", description_fname);
                goto failure;
            }

            ++data;

            if (data >= edata) {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected EOF\nSection end is incomplete!\n", description_fname);
                goto failure;
            }
            if (*data != '@') {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected character\nSection end is malformed!\n", description_fname);
                goto failure;
            }

            ++data;
            // Why should there be an additional \n after a @@??
            if (data >= edata) {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected EOF\nEOF right after the end of the section!\n", description_fname);
                goto failure;
            }
            if (*data != '\n') {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected character\nNo newline after the end of the section!\n", description_fname);
                goto failure;
            }

            free (section->data);
            if (!(section->data = malloc (section->len = econtent - content + 1))) {
                fprintf (stderr, "ERROR: Failed to allocate memory\n");
                goto failure;
            }
            ((char *)section->data)[section->len - 1] = '\0';
            memcpy (section->data, content, --(section->len));
            section = NULL;
        } else {
            // Parse section name
            if (*data == '\n') {
                ++data;
                continue;
            }
            if (*data != '@') {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected character\nReason: Didn't find the starting `@`\n", description_fname);
                goto failure;
            }
            ++data;
            const char *name = data;
            while (data < edata) {
                if (*data == '@')
                    break;
                ++data;
            }
            if (data >= edata) {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected EOF\nReason: Didn't find the section name\n", description_fname);
                goto failure;
            }

            int name_len = data - name;
            if (strnequalliteral (name, name_len, "function_return_type")) {
                section = &module_source->function_return_type;
            } else if (strnequalliteral (name, name_len, "function_return_type_baked")) {
                section = &module_source->function_return_type_baked;
            } else if (strnequalliteral (name, name_len, "function_parameters")) {
                section = &module_source->function_parameters;
            } else if (strnequalliteral (name, name_len, "function_parameters_baked")) {
                section = &module_source->function_parameters_baked;
            } else if (strnequalliteral (name, name_len, "function_body")) {
                section = &module_source->function_body;
            } else if (strnequalliteral (name, name_len, "class")) {
                section = &module_source->_class;
            } else if (strnequalliteral (name, name_len, "class_baked")) {
                section = &module_source->_class_baked;
            } else if (strnequalliteral (name, name_len, "trait")) {
                section = &module_source->_trait;
            } else if (strnequalliteral (name, name_len, "trait_baked")) {
                section = &module_source->_trait_baked;
            } else if (strnequalliteral (name, name_len, "struct")) {
                section = &module_source->_struct;
            } else if (strnequalliteral (name, name_len, "struct_baked")) {
                section = &module_source->_struct_baked;
            } else if (strnequalliteral (name, name_len, "record")) {
                section = &module_source->_record;
            } else if (strnequalliteral (name, name_len, "record_baked")) {
                section = &module_source->_record_baked;
            } else if (strnequalliteral (name, name_len, "array")) {
                section = &module_source->_array;
            } else if (strnequalliteral (name, name_len, "array_baked")) {
                section = &module_source->_array_baked;
            } else if (strnequalliteral (name, name_len, "bind")) {
                section = &module_source->_bind;
            } else if (strnequalliteral (name, name_len, "bind_name")) {
                section = &module_source->_bind_name;
            } else if (strnequalliteral (name, name_len, "bind_baked")) {
                section = &module_source->_bind_baked;
            } else if (strnequalliteral (name, name_len, "function_kind")) {
                section = &module_source->function_kind;
            } else {
                char parsed_section_name[100];
                parsed_section_name[0] = '\0';
                if (name_len < 100) {
                    memcpy (parsed_section_name, name, name_len);
                    parsed_section_name[name_len] = '\0';
                }
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected character\nCouldn't parse the name of the section: %s\n", description_fname, parsed_section_name);
                goto failure;
            }

            ++data;

            if (data >= edata) {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected EOF\nEOF right past section name!\n", description_fname);
                goto failure;
            }
            if (*data != '\n') {
                fprintf (stderr, "ERROR: Failed to parse description at '%s': unexpected character\nNo newline past section name\n", description_fname);
                goto failure;
            }

            ++data;
        }
    }
    munmap (data, size);
    close (fd);
    return 1;

failure:
    munmap (data, size);
    close (fd);
    // TODO: Clear module source
    return 0;
}
static int hc_module_load_children (hc_module_t *module, const char *children_dname, char *error_message, size_t max_error_message_len)
{
    DIR *dh = opendir (children_dname);
    if (!dh) {
        snprintf (error_message, max_error_message_len, "Failed to open directory '%s': %s", children_dname, strerror (errno));
        return -1;
    }
    struct dirent *entry;
    while ((entry = ((errno = 0), readdir (dh)))) {
        const char *name = entry->d_name;
        if (*name == '.' && (name[1] == '\0' || (name[1] == '.' && name[2] == '\0')))
            continue;
        char *dot_pos = strchr (name, '.');
        char child_module_name[512];
        char is_container = dot_pos == NULL;
        if (dot_pos) {
            if (strcmp (dot_pos, ".hc")) {
                snprintf (error_message, max_error_message_len, "Unexpected module filename '%s' extension ('.hc' expected)", entry->d_name);
                closedir (dh);
                return -1;
            }
            size_t name_part_len = dot_pos - name;
            memcpy (child_module_name, name, name_part_len);
            child_module_name[name_part_len] = '\0';
        } else {
            strcpy (child_module_name, name);
        }
        if (strlen (child_module_name) > 255) {
            snprintf (error_message, max_error_message_len, "Too long module name '%s'", child_module_name);
            closedir (dh);
            return -1;
        }
        if (!module_name_is_valid (child_module_name)) {
            snprintf (error_message, max_error_message_len, "Invalid module name '%s'", child_module_name);
            closedir (dh);
            return -1;
        }
        hc_module_map_t *module_children = hc_module_children (module);
        hc_module_t *child_module = hc_module_map_find (module_children, child_module_name);
        if (!child_module) {
            child_module = hc_module_create ();
            hc_module_map_replace (module_children, child_module_name, child_module);
        }
        char entry_fname[512];
        snprintf (entry_fname, sizeof (entry_fname), "%s/%s", children_dname, entry->d_name);
        if (is_container)
            hc_module_load_children (child_module, entry_fname, error_message, max_error_message_len);
        else
            hc_module_load_description (child_module, entry_fname);
    }
    if (errno) {
        snprintf (error_message, max_error_message_len, "Failed to read directory '%s': %s", children_dname, strerror (errno));
        closedir (dh);
        return -1;
    }
    closedir (dh);
    return 0;
}
