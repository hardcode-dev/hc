#include "hc_sem_rpn.h"
#include "hc_sem.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct hc_sem_rpn_t {
    hc_sem_insn_t *instructions;
    int count;
    int reserved;
} hc_sem_rpn_t;


hc_sem_rpn_t *hc_sem_rpn_create ()
{
    hc_sem_rpn_t *s = malloc (sizeof (hc_sem_rpn_t));
    if (!s)
	return NULL;
    s->instructions = malloc (sizeof (hc_sem_insn_t)*(s->reserved = 16));
    s->count = 0;
    return s;
}
void hc_sem_rpn_destroy (hc_sem_rpn_t *s)
{
    if (!s) return;
    hc_sem_insn_t *insn = s->instructions;
    hc_sem_insn_t *einsn = insn + s->count;
    for (; insn < einsn; ++insn)
	if (HC_SEM_INSN_VALUE_TYPE (insn->type) == HC_SEM_INSN_TYPE_STRING)
	    free (insn->value._string.data);
    free (s->instructions);
    free (s);
}
hc_sem_insn_t *hc_sem_rpn_get_instructions (hc_sem_rpn_t *s)
{
    return s->instructions;
}
int hc_sem_rpn_get_instruction_count (const hc_sem_rpn_t *s)
{
    return s->count;
}
int hc_sem_rpn_move_instruction (hc_sem_rpn_t *s, int type, int token_off, int token_size, const hc_sem_insn_value_t *value)
{
    if (s->count == s->reserved) {
	if (!(s->instructions = realloc (s->instructions, sizeof (hc_sem_insn_t)*(s->reserved <<= 1))))
	    return 0;
    }
    hc_sem_insn_t *insn = s->instructions + s->count++;
    insn->type = type;
    insn->source_off = token_off;
    insn->source_size = token_size;
    if (HC_SEM_INSN_VALUE_TYPE (type) != HC_SEM_INSN_TYPE_VOID)
	insn->value = *value;
    return 1;
}
