#pragma once

#include "hc_sem.h"

typedef struct hc_sem_rpn_t hc_sem_rpn_t;

extern hc_sem_rpn_t *hc_sem_rpn_create ();

extern void hc_sem_rpn_destroy (
    hc_sem_rpn_t *s);

extern hc_sem_insn_t *hc_sem_rpn_get_instructions (
    hc_sem_rpn_t *s);

extern int hc_sem_rpn_get_instruction_count (
    const hc_sem_rpn_t *s);

extern int hc_sem_rpn_move_instruction (
    hc_sem_rpn_t *s,
    int type,
    int token_off,
    int token_size,
    const hc_sem_insn_value_t *value);
