#define HC_SYN_INSN_TYPE_VOID 0
#define HC_SYN_INSN_TYPE_BOOL 1
#define HC_SYN_INSN_TYPE_STRING 2
#define HC_SYN_INSN_TYPE_WORD 3
#define HC_SYN_INSN_TYPE_UWORD 4
#define HC_SYN_INSN_TYPE_BYTE 5
#define HC_SYN_INSN_TYPE_UBYTE 6
#define HC_SYN_INSN_TYPE_SHORT 7
#define HC_SYN_INSN_TYPE_USHORT 8
#define HC_SYN_INSN_TYPE_INT 9
#define HC_SYN_INSN_TYPE_UINT 10
#define HC_SYN_INSN_TYPE_LONG 11
#define HC_SYN_INSN_TYPE_ULONG 12
#define HC_SYN_INSN_TYPE_HUGE 13
#define HC_SYN_INSN_TYPE_UHUGE 14
#define HC_SYN_INSN_TYPE_HALF 15
#define HC_SYN_INSN_TYPE_FLOAT 16
#define HC_SYN_INSN_TYPE_DOUBLE 17
#define HC_SYN_INSN_TYPE_QUAD 18
