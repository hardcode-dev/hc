#pragma once

#include <stdint.h>

#include "hc_syn_types.h"

typedef struct hc_syn_string_t {
    void *data;
    int len;
} hc_syn_string_t;

typedef struct hc_syn_insn_value_t {
    union {
        intptr_t _word;
        uintptr_t _uword;
        int8_t _byte;
        uint8_t _ubyte;
        int16_t _short;
        uint16_t _ushort;
        int32_t _int;
        uint32_t _uint;
        int64_t _long;
        uint64_t _ulong;
        float _float;
        double _double;
        double _quad;
        hc_syn_string_t _string;
    };
} hc_syn_insn_value_t;

typedef struct hc_syn_insn_t {
    int type;
    int source_off;
    int source_size;
    hc_syn_insn_value_t value;
} hc_syn_insn_t;

#include "hc_syn_instructions.h"

enum {
#define DEFINE_ENUM(INSN_NAME, noop1) HC_SYN_INSN_INDEX_##INSN_NAME,
    HC_SYN_DEFINE_INSTRUCTIONS(DEFINE_ENUM)
#undef DEFINE_ENUM
};

enum {
#define DEFINE_ENUM(INSN_NAME, INSN_VALUE_TYPE) HC_SYN_INSN_##INSN_NAME = HC_SYN_INSN_INDEX_##INSN_NAME | ((HC_SYN_INSN_TYPE_##INSN_VALUE_TYPE) << 24),
    HC_SYN_DEFINE_INSTRUCTIONS(DEFINE_ENUM)
#undef DEFINE_ENUM
};

#define HC_SYN_INSN_VALUE_TYPE(insn) ((insn) >> 24)
#define HC_SYN_INSN_INDEX(insn) ((insn)&0xffffff)
