#pragma once

#include "hc_sem_rpn.h"
#include "hc_syn_rpn.h"

#include <stddef.h>

typedef struct hc_module_t hc_module_t;
typedef struct hc_module_map_t hc_module_map_t;

typedef struct hc_source_section_t {
    void *data;
    size_t len;
} hc_source_section_t;

typedef struct hc_module_source_t {
    hc_source_section_t _class;
    hc_source_section_t _class_baked;
    hc_source_section_t _trait;
    hc_source_section_t _trait_baked;
    hc_source_section_t _struct;
    hc_source_section_t _struct_baked;
    hc_source_section_t _record;
    hc_source_section_t _record_baked;
    hc_source_section_t _bind;
    hc_source_section_t _bind_name;
    hc_source_section_t _bind_baked;
    hc_source_section_t _array;
    hc_source_section_t _array_baked;
    struct {                                            // TODO: Make it a function prototype
        hc_source_section_t function_return_type;       // TODO: Make it an array or map
        hc_source_section_t function_return_type_baked; // TODO: Make it an array or map
        hc_source_section_t function_parameters;        // TODO: Make it an array or map
        hc_source_section_t function_parameters_baked;  // TODO: Make it an array or map
        hc_source_section_t function_body;              // TODO: Make it an array or map
        hc_source_section_t function_kind;
    };
} hc_module_source_t;

typedef struct hc_module_syn_t {
    hc_syn_rpn_t *_class;
    hc_syn_rpn_t *_trait;
    hc_syn_rpn_t *_struct;
    hc_syn_rpn_t *_record;
    hc_syn_rpn_t *_bind;
    hc_syn_rpn_t *_array;
    struct {                                // TODO: Make it a function prototype
        hc_syn_rpn_t *function_return_type; // TODO: Make it an array or map
        hc_syn_rpn_t *function_parameters;  // TODO: Make it an array or map
        hc_syn_rpn_t *function_body;        // TODO: Make it an array or map
        hc_syn_rpn_t *function_kind;        // What do these mean?
    };
} hc_module_syn_t;

typedef struct hc_module_sem_t {
    hc_sem_rpn_t *_class;
    hc_sem_rpn_t *_trait;
    hc_sem_rpn_t *_struct;
    hc_sem_rpn_t *_record;
    hc_sem_rpn_t *_bind;
    hc_sem_rpn_t *_array;
    struct {                                // TODO: Make it a function prototype
        hc_sem_rpn_t *function_return_type; // TODO: Make it an array or map
        hc_sem_rpn_t *function_parameters;  // TODO: Make it an array or map
        hc_sem_rpn_t *function_body;        // TODO: Make it an array or map
        hc_sem_rpn_t *function_kind;
    };
} hc_module_sem_t;

extern hc_module_t *hc_module_create();

extern void hc_module_destroy(hc_module_t *s);

extern hc_module_source_t *hc_module_source(hc_module_t *s);

extern hc_module_syn_t *hc_module_syn(hc_module_t *s);

extern hc_module_sem_t *hc_module_sem(hc_module_t *s);

extern hc_module_map_t *hc_module_children(hc_module_t *s);
