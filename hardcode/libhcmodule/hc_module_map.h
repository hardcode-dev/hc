#pragma once

typedef struct hc_module_t hc_module_t;
typedef struct hc_module_map_t hc_module_map_t;

typedef int (*hc_module_map_module_callback_t) (
    const char *name,
    hc_module_t *module,
    void *user_data);

extern hc_module_map_t *hc_module_map_create ();

extern void hc_module_map_destroy (
    hc_module_map_t *s);

extern hc_module_t *hc_module_map_find (
    hc_module_map_t *s,
    const char *name);

extern int hc_module_map_replace (
    hc_module_map_t *s,
    const char *name,
    hc_module_t *module);

extern hc_module_t *hc_module_map_take (
    hc_module_map_t *s,
    const char *name);

extern int hc_module_map_foreach (
    hc_module_map_t *s,
    hc_module_map_module_callback_t callback,
    void *user_data);
