#include "hc_module.h"

#include "hc_module_map.h"
#include "hc_sem_rpn.h"
#include "hc_syn_rpn.h"

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct hc_module_t {
    hc_module_source_t *source;
    hc_module_syn_t *syn;
    hc_module_sem_t *sem;
    hc_module_map_t *children;
} hc_module_t;

static void hc_source_section_free (hc_source_section_t *s) { free (s->data); }

static hc_module_source_t *hc_module_source_create ()
{
    hc_module_source_t *s = malloc (sizeof (hc_module_source_t));
    memset (s, 0, sizeof (hc_module_source_t));
    return s;
}

static void hc_module_source_destroy (hc_module_source_t *s)
{
    if (!s)
        return;
    hc_source_section_free (&s->_class);
    hc_source_section_free (&s->_class_baked);
    hc_source_section_free (&s->_trait);
    hc_source_section_free (&s->_trait_baked);
    hc_source_section_free (&s->_struct);
    hc_source_section_free (&s->_struct_baked);
    hc_source_section_free (&s->_record);
    hc_source_section_free (&s->_record_baked);
    hc_source_section_free (&s->_bind);
    hc_source_section_free (&s->_bind_name);
    hc_source_section_free (&s->_bind_baked);
    hc_source_section_free (&s->_array);
    hc_source_section_free (&s->_array_baked);
    hc_source_section_free (&s->function_return_type);
    hc_source_section_free (&s->function_return_type_baked);
    hc_source_section_free (&s->function_parameters);
    hc_source_section_free (&s->function_parameters_baked);
    hc_source_section_free (&s->function_body);
    hc_source_section_free (&s->function_kind);
    free (s);
}

static hc_module_syn_t *hc_module_syn_create ()
{
    hc_module_syn_t *s = malloc (sizeof (hc_module_syn_t));
    memset (s, 0, sizeof (hc_module_syn_t));
    return s;
}

static void hc_module_syn_destroy (hc_module_syn_t *s)
{
    if (!s)
        return;
    hc_syn_rpn_destroy (s->_class);
    hc_syn_rpn_destroy (s->_trait);
    hc_syn_rpn_destroy (s->_struct);
    hc_syn_rpn_destroy (s->_record);
    hc_syn_rpn_destroy (s->_bind);
    hc_syn_rpn_destroy (s->_array);
    hc_syn_rpn_destroy (s->function_return_type);
    hc_syn_rpn_destroy (s->function_parameters);
    hc_syn_rpn_destroy (s->function_body);
    hc_syn_rpn_destroy (s->function_kind);
    free (s);
}

static hc_module_sem_t *hc_module_sem_create ()
{
    hc_module_sem_t *s = malloc (sizeof (hc_module_sem_t));
    memset (s, 0, sizeof (hc_module_sem_t));
    return s;
}

static void hc_module_sem_destroy (hc_module_sem_t *s)
{
    if (!s)
        return;
    hc_sem_rpn_destroy (s->_class);
    hc_sem_rpn_destroy (s->_trait);
    hc_sem_rpn_destroy (s->_struct);
    hc_sem_rpn_destroy (s->_record);
    hc_sem_rpn_destroy (s->_bind);
    hc_sem_rpn_destroy (s->_array);
    hc_sem_rpn_destroy (s->function_return_type);
    hc_sem_rpn_destroy (s->function_parameters);
    hc_sem_rpn_destroy (s->function_body);
    hc_sem_rpn_destroy (s->function_kind);
    free (s);
}

hc_module_t *hc_module_create ()
{
    hc_module_t *s = malloc (sizeof (hc_module_t));
    s->source = NULL;
    s->syn = NULL;
    s->sem = NULL;
    s->children = NULL;
    return s;
}

void hc_module_destroy (hc_module_t *s)
{
    if (!s)
        return;
    hc_module_source_destroy (s->source);
    hc_module_syn_destroy (s->syn);
    hc_module_sem_destroy (s->sem);
    hc_module_map_destroy (s->children);
    free (s);
}

hc_module_source_t *hc_module_source (hc_module_t *s)
{
    if (!s->source)
        s->source = hc_module_source_create ();
    return s->source;
}

hc_module_syn_t *hc_module_syn (hc_module_t *s)
{
    if (!s->syn)
        s->syn = hc_module_syn_create ();
    return s->syn;
}

hc_module_sem_t *hc_module_sem (hc_module_t *s)
{
    if (!s->sem)
        s->sem = hc_module_sem_create ();
    return s->sem;
}

hc_module_map_t *hc_module_children (hc_module_t *s)
{
    if (!s->children)
        s->children = hc_module_map_create ();
    return s->children;
}
