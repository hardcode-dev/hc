#include "hc_module_map.h"
#include "hc_module.h"

#include <glib.h>
#include <stdlib.h>
#include <string.h>

typedef struct node_t node_t;

typedef struct node_t {
    hc_module_t *module;
    char name[1];
} node_t;

typedef struct hc_module_map_t {
    node_t *root;
    node_t **nodes;
    size_t count;
    size_t allocated;
} hc_module_map_t;

static void check_extra_space(hc_module_map_t *s) {
    if (!s->allocated) {
        s->allocated = 16;
        s->nodes = malloc(sizeof(node_t *) * s->allocated);
        return;
    }

    if (s->count < s->allocated)
        return;

    s->allocated *= 2;
    s->nodes = realloc(s->nodes, sizeof(node_t *) * s->allocated);
}

static void check_shrink_space(hc_module_map_t *s) {
    if (s->allocated <= 16 || s->allocated <= s->count * 2)
        return;

    s->allocated /= 2;
    s->nodes = realloc(s->nodes, sizeof(node_t *) * s->allocated);
}

static hc_module_t *module_map_take_node(hc_module_map_t *s, size_t i) {
    node_t *node = s->nodes[i];
    hc_module_t *module = node->module;
    free(node);
    memmove(s->nodes + i, s->nodes + (i + 1), sizeof(node_t *) * (s->count - i - 1));
    s->count--;
    check_shrink_space(s);
    return module;
}

static node_t *node_new(const char *name, hc_module_t *module) {
    size_t name_len = strlen(name);
    node_t *node = malloc(sizeof(node_t) + name_len);
    node->module = module;
    strcpy(node->name, name);
    return node;
}

hc_module_map_t *hc_module_map_create() {
    hc_module_map_t *s = malloc(sizeof(hc_module_map_t));
    s->root = NULL;
    s->nodes = NULL;
    s->count = 0;
    s->allocated = 0;
    return s;
}

void hc_module_map_destroy(hc_module_map_t *s) {
    if (!s)
        return;
    size_t i;
    for (i = 0; i < s->count; ++i) {
        node_t *node = s->nodes[i];
        hc_module_destroy(node->module);
        free(node);
    }
    free(s->nodes);
    free(s);
}

hc_module_t *hc_module_map_find(hc_module_map_t *s, const char *name) {
    if (!s->count)
        return NULL;
    size_t l = 0;
    size_t r = s->count - 1;
    {
        int res = strcmp(name, s->nodes[l]->name);
        if (!res)
            return s->nodes[l]->module;
        if (res < 0)
            return NULL;
    }
    {
        int res = strcmp(name, s->nodes[r]->name);
        if (!res)
            return s->nodes[r]->module;
        if (res > 0)
            return NULL;
    }
    while ((l + 1) < r) {
        size_t m = (l + r) / 2;
        int res = strcmp(name, s->nodes[m]->name);
        if (!res)
            return s->nodes[m]->module;
        if (res < 0)
            r = m;
        else
            l = m;
    }
    if (!strcmp(name, s->nodes[l]->name))
        return s->nodes[l]->module;
    if ((r != l) && !strcmp(name, s->nodes[r]->name))
        return s->nodes[r]->module;
    return NULL;
}

int hc_module_map_replace(hc_module_map_t *s, const char *name, hc_module_t *module) {
    if (!s->count) {
        check_extra_space(s);
        s->count = 1;
        s->nodes[0] = node_new(name, module);
        return 0;
    }

    size_t l = 0;
    size_t r = s->count - 1;
    {
        int res = strcmp(name, s->nodes[l]->name);
        if (!res) {
            node_t *node = s->nodes[l];
            hc_module_destroy(node->module);
            node->module = module;
            return 1;
        }
        if (res < 0) {
            check_extra_space(s);
            memmove(s->nodes + 1, s->nodes, sizeof(node_t *) * s->count);
            s->nodes[0] = node_new(name, module);
            s->count++;
            return 0;
        }
    }
    {
        int res = strcmp(name, s->nodes[r]->name);
        if (!res) {
            node_t *node = s->nodes[r];
            hc_module_destroy(node->module);
            node->module = module;
            return 1;
        }
        if (res > 0) {
            check_extra_space(s);
            s->nodes[s->count++] = node_new(name, module);
            return 0;
        }
    }
    while ((l + 1) < r) {
        size_t m = (l + r) / 2;
        int res = strcmp(name, s->nodes[m]->name);
        if (!res) {
            node_t *node = s->nodes[m];
            hc_module_destroy(node->module);
            node->module = module;
            return 1;
        }
        if (res < 0)
            r = m;
        else
            l = m;
    }
    if (!strcmp(name, s->nodes[l]->name)) {
        node_t *node = s->nodes[l];
        hc_module_destroy(node->module);
        node->module = module;
        return 1;
    }
    if ((r != l) && !strcmp(name, s->nodes[r]->name)) {
        node_t *node = s->nodes[r];
        hc_module_destroy(node->module);
        node->module = module;
        return 1;
    }

    check_extra_space(s);
    memmove(s->nodes + (r + 1), s->nodes + r, sizeof(node_t *) * (s->count - r));
    s->nodes[r] = node_new(name, module);
    s->count++;
    return 0;
}

hc_module_t *hc_module_map_take(hc_module_map_t *s, const char *name) {
    if (!s->count)
        return NULL;
    size_t l = 0;
    size_t r = s->count - 1;
    {
        int res = strcmp(name, s->nodes[l]->name);
        if (!res)
            return module_map_take_node(s, l);
        if (res < 0)
            return NULL;
    }
    {
        int res = strcmp(name, s->nodes[r]->name);
        if (!res)
            return module_map_take_node(s, r);
        if (res > 0)
            return NULL;
    }
    while ((l + 1) < r) {
        size_t m = (l + r) / 2;
        int res = strcmp(name, s->nodes[m]->name);
        if (!res)
            return module_map_take_node(s, m);
        if (res < 0)
            r = m;
        else
            l = m;
    }
    if (!strcmp(name, s->nodes[l]->name))
        return module_map_take_node(s, l);
    if ((r != l) && !strcmp(name, s->nodes[r]->name))
        return module_map_take_node(s, r);
    return NULL;
}

int hc_module_map_remove(hc_module_map_t *s, const char *name) {
    hc_module_t *module = hc_module_map_take(s, name);
    if (module) {
        hc_module_destroy(module);
        return 1;
    }
    return 0;
}

int hc_module_map_foreach(hc_module_map_t *s, hc_module_map_module_callback_t callback, void *user_data) {
    size_t i;
    for (i = 0; i < s->count; ++i) {
        node_t *node = s->nodes[i];
        if (callback(node->name, node->module, user_data))
            return -1;
    }
    return 0;
}
