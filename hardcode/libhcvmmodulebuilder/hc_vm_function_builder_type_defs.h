#pragma once

#include "hc_vm_function_builder.h"

enum {
    HC_VM_MAX_SCOPE_DEPTH = 32
};

enum {
#define EXTRACT_INTRUCTION_DATA(name, ...) HC_VM_INSTR_##name,
#include <instr_defs.txt>
#undef EXTRACT_INTRUCTION_DATA
};

typedef struct hc_vm_function_instruction_t {
    uint64_t instr;
    hc_vm_function_instruction_arg_t args[3];
    // Metadata calculated for the executor
    // used for branching instructions
    uint64_t next_instruction;
    // used for memory allocation
    uint64_t stack_depth;
    // offset for indexing commands
    uint64_t pointer_offset;
} hc_vm_function_instruction_t;

typedef struct hc_vm_function_scope_t {
    uint64_t stack_depth;
    uint64_t first_instr;
    uint64_t uid;
} hc_vm_function_scope_t;

typedef struct hc_vm_function_build_context_t {
    hc_vm_function_scope_t scopes[HC_VM_MAX_SCOPE_DEPTH];
    uint64_t last_scope;
    uint64_t next_uid;
} hc_vm_function_build_context_t;

typedef struct hc_vm_function_stored_type_t {
    hc_vm_type_t data;
    struct hc_vm_function_stored_type_t* next;
} hc_vm_function_stored_type_t;

typedef struct hc_vm_function_t {
    hc_vm_function_instruction_t *instructions;
    uint64_t instr_count;
    hc_vm_function_metadata_t metadata;
    hc_vm_function_build_context_t* build_context;
    hc_vm_function_stored_type_t* type_storage;
} hc_vm_function_t;
