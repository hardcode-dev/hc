#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <libhcvmtypes/hc_vm_types.h>

typedef struct hc_vm_function_t hc_vm_function_t;
typedef struct hc_vm_data_immidiate_t hc_vm_data_immidiate_t;

// Builder is in an undefined state after an error
// Only thing that can be done at this point is finilize and destroy
// (Depending on the error it may be safe-ish to proceed anyway)
enum {
    HC_VM_BUILD_OK = 0
    , HC_VM_BUILD_LEGACY_ERR = 1
    , HC_VM_BUILD_SCOPE_TOO_DEEP_ERR = 2
    , HC_VM_BUILD_UNCLOSED_SCOPE_ERR = 3
    , HC_VM_BUILD_INCORRECT_SCOPE_ERR = 4
    , HC_VM_BUILD_STACK_SIZE_ERR = 5
    , HC_VM_BUILD_INVALID_TYPE_ERR = 6
    , HC_VM_BUILD_NON_IMMIDIATE_VALUE = 7
    , HC_VM_BUILD_SIGNATURE_MISMATCH = 8
    , HC_VM_BUILD_UNSAFE_POINTER_OP = 9
    , HC_VM_BUILD_LOOP_EXPECTED = 10
    , HC_VM_BUILD_MEMORY_OUT_OF_SCOPE = 11 // Builder is broken after this error, execution will yield undefined results
};

typedef struct hc_vm_function_instruction_arg_t {
    // Underlying type
    const hc_vm_type_t* type_ptr;
    union {
        uint64_t stack_offset;
        // stores primitive types
        unsigned char primitive[sizeof(uint64_t)];
        // stores pointer to raw memory
        void* raw_memory;
    };
    bool immidiate;
    bool user_read_only; // used exclusively for loops to gurantee run time
    uint64_t min_subscope; // tracks the longest memory lifetime double pointer points to
    uint64_t scope_idx;
    uint64_t scope_uid;
} hc_vm_function_instruction_arg_t;

typedef struct hc_vm_function_metadata_t {
    const hc_vm_type_t* signature;
    uint64_t required_stack_bytes;
    hc_vm_type_t ti_struct;
    uint64_t red_zone_bytes;
    bool finite_run_time;
} hc_vm_function_metadata_t;

const hc_vm_function_metadata_t* hc_vm_function_extract_metadata(const hc_vm_function_t* function);

uint64_t hc_vm_function_create(
    hc_vm_function_t** module,
    const hc_vm_type_t* signature,
    hc_vm_function_instruction_arg_t* arg_pointer);

uint64_t hc_vm_function_finalize(hc_vm_function_t* module);
void hc_vm_function_destroy(hc_vm_function_t* module);

void hc_vm_function_data_immidiate(void* buffer, hc_vm_function_instruction_arg_t* data, const hc_vm_type_t* type);
void hc_vm_function_wrap_user_data(hc_vm_function_t* func, void* user_data, hc_vm_function_instruction_arg_t* data);

// Pushed `type` to stack, resulting pointer location is returned
uint64_t hc_vm_function_instr_push_stack(hc_vm_function_t* module, const hc_vm_type_t* type, hc_vm_function_instruction_arg_t* location);
// Create a pointer to memory location target and store it in `pointer`
uint64_t hc_vm_function_instr_ref(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* target,
    hc_vm_function_instruction_arg_t* pointer);

// sets a given memory location to the value at a different location
uint64_t hc_vm_function_instr_set(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* data,
    const hc_vm_function_instruction_arg_t* address);

// sets a given double pointer to a given pointer value
uint64_t hc_vm_function_instr_setptrindir(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* ptr,
    hc_vm_function_instruction_arg_t* double_ptr);

// removes one level of indirection from a pointer
uint64_t hc_vm_function_instr_derefptr(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* ptr,
    const hc_vm_function_instruction_arg_t* deref_value);


uint64_t hc_vm_function_instr_extend(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* from,
    const hc_vm_function_instruction_arg_t* to);

uint64_t hc_vm_function_instr_setzptr(
    hc_vm_function_t* module,
    hc_vm_function_instruction_arg_t* address);

#define SIMPLE_INSTRUCTION(name) uint64_t hc_vm_function_instr_##name( \
    hc_vm_function_t* module, \
    const hc_vm_function_instruction_arg_t* a, \
    const hc_vm_function_instruction_arg_t* b, \
    const hc_vm_function_instruction_arg_t* dest)

SIMPLE_INSTRUCTION(add);
SIMPLE_INSTRUCTION(sub);
SIMPLE_INSTRUCTION(mul);
SIMPLE_INSTRUCTION(div);
SIMPLE_INSTRUCTION(mod);
SIMPLE_INSTRUCTION(and);
SIMPLE_INSTRUCTION(or);

uint64_t hc_vm_function_instr_not(
            hc_vm_function_t *module,
            const hc_vm_function_instruction_arg_t *a,
            const hc_vm_function_instruction_arg_t *dest);

SIMPLE_INSTRUCTION(shift_left);
SIMPLE_INSTRUCTION(shift_right);
SIMPLE_INSTRUCTION(greater);
SIMPLE_INSTRUCTION(greater_eq);
SIMPLE_INSTRUCTION(less);
SIMPLE_INSTRUCTION(less_eq);
SIMPLE_INSTRUCTION(eq);

#undef SIMPLE_INSTRUCTION

// Checks if a is zero
uint64_t hc_vm_function_instr_ifnz(hc_vm_function_t* module, const hc_vm_function_instruction_arg_t *a);
// Checks if ptr is zero
uint64_t hc_vm_function_instr_isnzptr(
        hc_vm_function_t* module,
        const hc_vm_function_instruction_arg_t* ptr,
        const hc_vm_function_instruction_arg_t* result);
uint64_t hc_vm_function_instr_else(hc_vm_function_t* module);
uint64_t hc_vm_function_instr_endif(hc_vm_function_t* module);

uint64_t hc_vm_function_instr_return(hc_vm_function_t* module);
uint64_t hc_vm_function_instr_call(
    hc_vm_function_t* module,
    const hc_vm_function_t* other_function,
    const hc_vm_function_instruction_arg_t* arg);

typedef struct hc_vm_native_function_t hc_vm_native_function_t;

uint64_t hc_vm_function_instr_native_call(
    hc_vm_function_t* func,
    const hc_vm_native_function_t* native_func,
    const hc_vm_function_instruction_arg_t* arg);

uint64_t hc_vm_function_instr_arr_length(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* arr,
    const hc_vm_function_instruction_arg_t* length);

uint64_t hc_vm_function_instr_arr_index(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* arr,
    const hc_vm_function_instruction_arg_t* idx,
    hc_vm_function_instruction_arg_t* target);

uint64_t hc_vm_function_instr_struct_index(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t* st,
    const hc_vm_function_instruction_arg_t* idx,
    hc_vm_function_instruction_arg_t* target);

uint64_t hc_vm_function_instr_trap(hc_vm_function_t *func, void* data);

// Ascending loop over values in [start, limit]
uint64_t hc_vm_function_instr_loop_ascending(
    hc_vm_function_t* module,
    const hc_vm_function_instruction_arg_t *start,
    const hc_vm_function_instruction_arg_t *limit,
    hc_vm_function_instruction_arg_t *idx_pointer);
uint64_t hc_vm_function_instr_loop_ascending_end(hc_vm_function_t* module);

uint64_t hc_vm_function_instr_loop(hc_vm_function_t* func);
uint64_t hc_vm_function_instr_loop_end(hc_vm_function_t* func);

uint64_t hc_vm_function_instr_continue(hc_vm_function_t* func);
uint64_t hc_vm_function_instr_break(hc_vm_function_t* func);
