#include "hc_vm_function_builder.h"
#include "hc_vm_function_builder_type_defs.h"
#include <libhcvmexecutor/hc_vm_executor.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ASSERT_WRITABLE(x) if(x->immidiate || x->user_read_only) { return 1; }
#define IS_POINTER(x) (x->type_ptr->type == HC_VM_TYPE_POINTER)
#define ASSERT_POINTER(x) if (!IS_POINTER(x)) { return 1; }
#define ASSERT_DATA(x) if (IS_POINTER(x)) { return 1; }
#define GLUE(x,y) x##y
#define GLUE2(x,y) GLUE(x, y)
#define UNIQUE_VAR GLUE2(temp_, __LINE__)
#define UNWRAP_ERROR(x) { uint64_t UNIQUE_VAR = x; if (UNIQUE_VAR) return UNIQUE_VAR; }
#define ALLOC_STACK_MEMORY(module, bytes) if (hc_vm_allocate_stack(module, bytes)) { return 1; }

#define INSTRUCTION_ALLOC_BLOCKS 4

typedef struct hc_vm_instruction_metadata_t {
    char* name;
    uint64_t arg_count;
    uint64_t compute_complexity;
    uint64_t memory_complexity;
} hc_vm_instruction_metadata_t;

const hc_vm_function_metadata_t* hc_vm_function_extract_metadata(const hc_vm_function_t* function) {
    return &function->metadata;
}

static hc_vm_function_scope_t* hc_vm_last_scope(hc_vm_function_t* module) {
    hc_vm_function_scope_t* scopes = module->build_context->scopes;
    return &scopes[module->build_context->last_scope];
}

static uint64_t hc_vm_stack_depth(hc_vm_function_t* module) {
    return hc_vm_last_scope(module)->stack_depth;
}

static uint64_t hc_vm_current_stack_offset_raw(hc_vm_function_t* module) {
    uint64_t stack_depth = hc_vm_last_scope(module)->stack_depth;
    if (UINT64_MAX - stack_depth < hc_vm_word_size) {
        return 0;
    }
    return stack_depth;
}

static uint64_t hc_vm_current_stack_offset(hc_vm_function_t* module) {
    // Offset the address space by one word to detect pointers
    return hc_vm_current_stack_offset_raw(module) + hc_vm_word_size;
}

static uint64_t hc_vm_add_scope(hc_vm_function_t* module) {
    hc_vm_function_scope_t* scope = hc_vm_last_scope(module);
    hc_vm_function_build_context_t* bc = module->build_context;
    bc->last_scope++;
    if (bc->last_scope >= HC_VM_MAX_SCOPE_DEPTH) {
        return HC_VM_BUILD_SCOPE_TOO_DEEP_ERR;
    }
    hc_vm_function_scope_t* new_scope = hc_vm_last_scope(module);
    // overflow is possible, but unlikely
    new_scope->uid = bc->next_uid++;
    new_scope->first_instr = module->instr_count - 1;
    new_scope->stack_depth = scope->stack_depth;
    return 0;
}

static uint64_t hc_vm_remove_scope(hc_vm_function_t* module) {
    module->build_context->last_scope--;
    return 0;
}

static uint64_t hc_vm_allocate_stack(hc_vm_function_t* module, const uint64_t bytes) {
    hc_vm_function_scope_t* scope = hc_vm_last_scope(module);
    if (UINT64_MAX - scope->stack_depth < bytes) {
        return HC_VM_BUILD_STACK_SIZE_ERR;
    }
    scope->stack_depth += bytes;
    // Update required memory
    if (module->metadata.required_stack_bytes < scope->stack_depth) {
        module->metadata.required_stack_bytes = scope->stack_depth;
    }
    return 0;
}

static void hc_vm_append_type_to_storage(hc_vm_function_t* func, hc_vm_type_t** type) {
    hc_vm_function_stored_type_t* new_storage = malloc(sizeof(hc_vm_function_stored_type_t));
    new_storage->next = func->type_storage;
    func->type_storage = new_storage;
    *type = &new_storage->data;
}

uint64_t hc_vm_function_create(
        hc_vm_function_t** p,
        const hc_vm_type_t* signature,
        hc_vm_function_instruction_arg_t* arg_pointer) {
    *p = malloc(sizeof(hc_vm_function_t));
    hc_vm_function_t* module = *p;
    module->instructions = NULL;
    module->instr_count = 0;
    module->metadata.signature = signature;
    module->metadata.required_stack_bytes = 0;
    module->metadata.finite_run_time = true;
    module->type_storage = NULL;
    // init build context
    module->build_context = malloc(sizeof(hc_vm_function_build_context_t));
    module->build_context->last_scope = 0;
    module->build_context->next_uid = 1;
    // init function scope
    module->build_context->scopes[0] = (hc_vm_function_scope_t){
        0 /* stack depth */
        , 0 /* first instruction */
        , 0 /* uid */
    };
    if (arg_pointer != NULL) {
        arg_pointer->immidiate = false;
        arg_pointer->user_read_only = false;
        arg_pointer->stack_offset = hc_vm_current_stack_offset(module);
        arg_pointer->type_ptr = module->metadata.signature;
        ALLOC_STACK_MEMORY(module, arg_pointer->type_ptr->size_bytes);
        arg_pointer->scope_idx = 0;
        arg_pointer->scope_uid = 0;
        arg_pointer->min_subscope = 0;
    }
    // separate args and other variables
    hc_vm_add_scope(module);
    return 0;
}

void hc_vm_function_destroy(hc_vm_function_t* module) {
    if (module->instructions != NULL) {
        free(module->instructions);
    }
    module->instructions = NULL;
    module->instr_count = 0;
    while(module->type_storage != NULL) {
        hc_vm_function_stored_type_t* old_storage = module->type_storage;
        module->type_storage = old_storage->next;
        free(old_storage);
    }
    free(module);
}

static hc_vm_function_instruction_t *hc_vm_function_last_instruction(hc_vm_function_t* function) {
    return &(function->instructions[function->instr_count - 1]);
}

static hc_vm_function_instruction_t *hc_vm_function_append_instruction(hc_vm_function_t* module) {
    ++module->instr_count;
    if ((module->instr_count & (INSTRUCTION_ALLOC_BLOCKS - 1)) == 1) {
        module->instructions = reallocarray(module->instructions, module->instr_count + (INSTRUCTION_ALLOC_BLOCKS),
            sizeof(hc_vm_function_instruction_t));
    }
    hc_vm_function_instruction_t *last_instruction = &(module->instructions[module->instr_count - 1]);
    last_instruction->next_instruction = module->instr_count - 1;
    last_instruction->stack_depth = hc_vm_stack_depth(module);
    last_instruction->pointer_offset = 0;
    return last_instruction;
}

void hc_vm_function_data_immidiate(void* bytes, hc_vm_function_instruction_arg_t *arg, const hc_vm_type_t* type) {
    arg->type_ptr = type;
    memcpy(arg->primitive, bytes, type->size_bytes);
    arg->immidiate = true;
    arg->user_read_only = true;
    arg->scope_idx = 0;
    arg->scope_uid = 0;
    arg->min_subscope = 0;
}

void hc_vm_function_wrap_user_data(hc_vm_function_t* func, void* user_data, hc_vm_function_instruction_arg_t* data) {
    hc_vm_type_t* type;
    hc_vm_append_type_to_storage(func, &type);
    hc_vm_type_init_pointer(&hc_vm_type_user_data, type, true);
    hc_vm_function_data_immidiate((void*)&user_data, data, type);
}

static uint64_t hc_vm_copy_function_instr_arg(
        const hc_vm_function_t* func,
        hc_vm_function_instruction_arg_t* dest,
        const hc_vm_function_instruction_arg_t* src) {
    if (src == NULL) {
        return HC_VM_BUILD_OK;
    }
    bool found = false;
    uint64_t scope_idx = func->build_context->last_scope;
    while(scope_idx > 0) {
        if (func->build_context->scopes[scope_idx].uid == src->scope_uid) {
            found = true;
            break;
        }
        --scope_idx;
    }
    if (func->build_context->scopes[0].uid == src->scope_uid) {
        found = true;
    }
    if (!found) {
        return HC_VM_BUILD_MEMORY_OUT_OF_SCOPE;
    }
    *dest = *src;
    return HC_VM_BUILD_OK;
}

static uint64_t hc_vm_function_pack_instruction(
        hc_vm_function_t* func,
        uint64_t type,
        const hc_vm_function_instruction_arg_t* a,
        const hc_vm_function_instruction_arg_t* b,
        const hc_vm_function_instruction_arg_t* c) {
    hc_vm_function_instruction_t *instr = hc_vm_function_append_instruction(func);
    instr->instr = type;
    UNWRAP_ERROR(hc_vm_copy_function_instr_arg(func, &instr->args[0], a));
    UNWRAP_ERROR(hc_vm_copy_function_instr_arg(func, &instr->args[1], b));
    UNWRAP_ERROR(hc_vm_copy_function_instr_arg(func, &instr->args[2], c));
    return HC_VM_BUILD_OK;
}

uint64_t hc_vm_function_instr_push_stack(hc_vm_function_t* module, const hc_vm_type_t* type, hc_vm_function_instruction_arg_t* direct_location) {
    if (direct_location == NULL || type->size_bytes == 0) return 1;
    uint64_t raw_stack_offset = hc_vm_current_stack_offset_raw(module);
    uint64_t new_stack_offset = raw_stack_offset;
    // align stack if required
    UNWRAP_ERROR(hc_vm_type_align(&new_stack_offset, type->aligment_bytes));
    ALLOC_STACK_MEMORY(module, new_stack_offset - raw_stack_offset);
    // allocate type
    uint64_t stack_offset = hc_vm_current_stack_offset(module);
    direct_location->type_ptr = type;
    direct_location->stack_offset = stack_offset;
    direct_location->immidiate = false;
    direct_location->scope_idx = module->build_context->last_scope;
    direct_location->user_read_only = false;
    const hc_vm_function_scope_t* scope = hc_vm_last_scope(module);
    direct_location->scope_uid = scope->uid;
    // Pushed memory always points to atleast same scope
    direct_location->min_subscope = direct_location->scope_idx;
    ALLOC_STACK_MEMORY(module, type->size_bytes);
    UNWRAP_ERROR(hc_vm_function_pack_instruction(module, HC_VM_INSTR_PUSH, direct_location, NULL, NULL));
    return 0;
}

static uint64_t hc_vm_check_pointer_safety(const hc_vm_function_instruction_arg_t* source, const hc_vm_function_instruction_arg_t* ref_target) {
    // Writing pointers to scope 0 is forbidden since those are external
    if (source->scope_idx > ref_target->min_subscope || (ref_target->min_subscope == 0)) return HC_VM_BUILD_UNSAFE_POINTER_OP;
    return HC_VM_BUILD_OK;
}

static uint64_t hc_vm_update_double_pointer_ref_scope(hc_vm_function_instruction_arg_t* target, uint64_t ref_scope) {
    if (target->type_ptr->type == HC_VM_TYPE_POINTER && target->type_ptr->subtypes[0]->type == HC_VM_TYPE_POINTER) {
        if (target->min_subscope > ref_scope) {
            target->min_subscope = ref_scope;
        }
    }
    return HC_VM_BUILD_OK;
}

uint64_t hc_vm_function_instr_ref(
        hc_vm_function_t* module,
        const hc_vm_function_instruction_arg_t* target,
        hc_vm_function_instruction_arg_t* pointer) {
    if (target == NULL || pointer == NULL) {
        return 1;
    }
    ASSERT_POINTER(pointer);
    UNWRAP_ERROR(hc_vm_assert_types_compatible(pointer->type_ptr, target->type_ptr));
    UNWRAP_ERROR(hc_vm_check_pointer_safety(target, pointer));
    UNWRAP_ERROR(hc_vm_update_double_pointer_ref_scope(pointer, target->scope_idx));
    UNWRAP_ERROR(hc_vm_function_pack_instruction(module, HC_VM_INSTR_REF, pointer, target, NULL));
    return 0;
}

uint64_t hc_vm_function_instr_set(hc_vm_function_t *module, const hc_vm_function_instruction_arg_t *data, const hc_vm_function_instruction_arg_t *address) {
    ASSERT_WRITABLE(address);
    UNWRAP_ERROR(hc_vm_assert_types_compatible(address->type_ptr, data->type_ptr));
    const hc_vm_type_t* true_type;
    hc_vm_type_get_deref(address->type_ptr, &true_type);
    // No pointer writes are allowed use SETPTRINDIR or DEREFPTR
    // No complex struture writes are allowed
    if (true_type->type > HC_VM_TYPE_MAX_PRIMITIVE_TYPES) return HC_VM_BUILD_INVALID_TYPE_ERR;
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_SET, address, data, NULL);
}

uint64_t hc_vm_function_instr_setptrindir(
        hc_vm_function_t* module,
        const hc_vm_function_instruction_arg_t* ptr,
        hc_vm_function_instruction_arg_t* double_ptr) {
    ASSERT_WRITABLE(double_ptr);
    const hc_vm_type_t* true_type;
    hc_vm_type_get_deref(double_ptr->type_ptr, &true_type);
    if (true_type != ptr->type_ptr) return HC_VM_BUILD_INVALID_TYPE_ERR;
    UNWRAP_ERROR(hc_vm_check_pointer_safety(ptr, double_ptr));
    UNWRAP_ERROR(hc_vm_update_double_pointer_ref_scope(double_ptr, ptr->scope_idx));
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_SETPTRINDIR, ptr, double_ptr, NULL);
}

uint64_t hc_vm_function_instr_derefptr(
        hc_vm_function_t* module,
        const hc_vm_function_instruction_arg_t* ptr,
        const hc_vm_function_instruction_arg_t* deref_value) {
    ASSERT_WRITABLE(deref_value);
    const hc_vm_type_t* true_type;
    hc_vm_type_get_deref(ptr->type_ptr, &true_type);
    if (true_type != deref_value->type_ptr) return HC_VM_BUILD_INVALID_TYPE_ERR;
    // if we are writing a pointer, then we must check safety
    if (deref_value->type_ptr->type == HC_VM_TYPE_POINTER) {
        UNWRAP_ERROR(hc_vm_check_pointer_safety(ptr, deref_value));
    }
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_DEREFPTR, ptr, deref_value, NULL);
}

uint64_t hc_vm_function_instr_extend(
        hc_vm_function_t* module,
        const hc_vm_function_instruction_arg_t* from,
        const hc_vm_function_instruction_arg_t* to) {
    ASSERT_WRITABLE(to);
    const hc_vm_type_t* from_true_type;
    hc_vm_type_get_deref(from->type_ptr, &from_true_type);
    if (from_true_type->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return HC_VM_BUILD_INVALID_TYPE_ERR;
    const hc_vm_type_t* to_true_type;
    hc_vm_type_get_deref(to->type_ptr, &to_true_type);
    if (to_true_type->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return HC_VM_BUILD_INVALID_TYPE_ERR;
    if (from_true_type->size_bytes >= to_true_type->size_bytes) return HC_VM_BUILD_INVALID_TYPE_ERR;
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_EXTEND, from, to, NULL);
}

uint64_t hc_vm_function_instr_setzptr(hc_vm_function_t* module, hc_vm_function_instruction_arg_t* address) {
    ASSERT_WRITABLE(address);
    if (address->type_ptr->type != HC_VM_TYPE_POINTER) return HC_VM_BUILD_INVALID_TYPE_ERR;
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_SETZPTR, address, NULL, NULL);
}

#define SIMPLE_INSTRUCTION_INJECT(real_type)
#define SIMPLE_INSTRUCTION(name, instr_type) \
    uint64_t hc_vm_function_instr_##name( \
            hc_vm_function_t *module, \
            const hc_vm_function_instruction_arg_t *a, \
            const hc_vm_function_instruction_arg_t *b, \
            const hc_vm_function_instruction_arg_t *dest) { \
        ASSERT_WRITABLE(dest); \
        const hc_vm_type_t* a_real_type; \
        hc_vm_type_get_deref(a->type_ptr, &a_real_type); \
        if (a_real_type->type >= HC_VM_TYPE_MAX_PRIMITIVE_TYPES) return HC_VM_BUILD_INVALID_TYPE_ERR; /* Only primitive types are allowed */ \
        SIMPLE_INSTRUCTION_INJECT(a_real_type); \
        UNWRAP_ERROR(hc_vm_assert_types_compatible(a->type_ptr, b->type_ptr)); \
        UNWRAP_ERROR(hc_vm_assert_types_compatible(a->type_ptr, dest->type_ptr)); /* All arguments should be compatible */ \
        return hc_vm_function_pack_instruction(module, HC_VM_INSTR_##instr_type, a, b, dest); \
    }

SIMPLE_INSTRUCTION(add, ADD);
SIMPLE_INSTRUCTION(sub, SUB);
SIMPLE_INSTRUCTION(mul, MUL);
SIMPLE_INSTRUCTION(div, DIV);
#undef SIMPLE_INSTRUCTION_INJECT
#define SIMPLE_INSTRUCTION_INJECT(real_type) if (real_type->type == HC_VM_TYPE_DOUBLE) return HC_VM_BUILD_INVALID_TYPE_ERR;
SIMPLE_INSTRUCTION(mod, MOD);


#define BOOL_INSTRUCTION(name, instr_type) \
    uint64_t hc_vm_function_instr_##name( \
            hc_vm_function_t *module, \
            const hc_vm_function_instruction_arg_t *a, \
            const hc_vm_function_instruction_arg_t *b, \
            const hc_vm_function_instruction_arg_t *dest) { \
        ASSERT_WRITABLE(dest); \
        const hc_vm_type_t* a_real_type; \
        hc_vm_type_get_deref(a->type_ptr, &a_real_type); \
        if (a_real_type->type >= HC_VM_TYPE_MAX_PRIMITIVE_TYPES) return 1; /* Only primitive types are allowed */ \
        UNWRAP_ERROR(hc_vm_assert_types_compatible(a->type_ptr, b->type_ptr)); /* a and b should be compatible */ \
        const hc_vm_type_t* dest_real_type; \
        hc_vm_type_get_deref(dest->type_ptr, &dest_real_type); \
        if (dest_real_type->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return 1; /* dest must be unsigned */ \
        return hc_vm_function_pack_instruction(module, HC_VM_INSTR_##instr_type, a, b, dest); \
    }

BOOL_INSTRUCTION(greater, GREATER);
BOOL_INSTRUCTION(greater_eq, GREATER_EQ);
BOOL_INSTRUCTION(eq, EQ);
BOOL_INSTRUCTION(less, LESS);
BOOL_INSTRUCTION(less_eq, LESS_EQ);

#define BIT_INSTRUCTION(name, instr_type) \
    uint64_t hc_vm_function_instr_##name( \
            hc_vm_function_t *module, \
            const hc_vm_function_instruction_arg_t *a, \
            const hc_vm_function_instruction_arg_t *b, \
            const hc_vm_function_instruction_arg_t *dest) { \
        ASSERT_WRITABLE(dest); \
        const hc_vm_type_t* a_real_type; \
        hc_vm_type_get_deref(a->type_ptr, &a_real_type); \
        if (a_real_type->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return 1; /* Only unsigned types are allowed */ \
        UNWRAP_ERROR(hc_vm_assert_types_compatible(a->type_ptr, b->type_ptr)); \
        UNWRAP_ERROR(hc_vm_assert_types_compatible(a->type_ptr, dest->type_ptr)); /* All arguments should be compatible */ \
        return hc_vm_function_pack_instruction(module, HC_VM_INSTR_##instr_type, a, b, dest); \
    }

BIT_INSTRUCTION(or, OR);
BIT_INSTRUCTION(and, AND);
BIT_INSTRUCTION(shift_left, BSL);
BIT_INSTRUCTION(shift_right, BSR);

uint64_t hc_vm_function_instr_not(
            hc_vm_function_t *module,
            const hc_vm_function_instruction_arg_t *a,
            const hc_vm_function_instruction_arg_t *dest) {
    ASSERT_WRITABLE(dest);
    const hc_vm_type_t* a_real_type;
    hc_vm_type_get_deref(a->type_ptr, &a_real_type);
    if (a_real_type->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return 1; /* Only unsigned types are allowed */
    UNWRAP_ERROR(hc_vm_assert_types_compatible(a->type_ptr, dest->type_ptr)); /* All arguments should be compatible */
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_NOT, a, dest, NULL);
}

uint64_t hc_vm_function_instr_ifnz(hc_vm_function_t* module, const hc_vm_function_instruction_arg_t *address) {
    const hc_vm_type_t* addr_real_type;
    hc_vm_type_get_deref(address->type_ptr, &addr_real_type);
    if (addr_real_type->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return HC_VM_BUILD_INVALID_TYPE_ERR; /* address must be unsigned */
    UNWRAP_ERROR(hc_vm_function_pack_instruction(module, HC_VM_INSTR_IFNZ, address, NULL, NULL));
    return hc_vm_add_scope(module);
}

uint64_t hc_vm_function_instr_isnzptr(
        hc_vm_function_t* module,
        const hc_vm_function_instruction_arg_t* ptr,
        const hc_vm_function_instruction_arg_t* result) {
    ASSERT_WRITABLE(result);
    if (ptr->type_ptr->type != HC_VM_TYPE_POINTER) return HC_VM_BUILD_INVALID_TYPE_ERR;
    if (result->type_ptr->type >= HC_VM_TYPE_MAX_UNSIGNED_TYPES) return HC_VM_BUILD_INVALID_TYPE_ERR;
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_ISNZPTR, ptr, result, NULL);
}

uint64_t hc_vm_function_instr_else(hc_vm_function_t* module) {
    hc_vm_function_scope_t* scope = hc_vm_last_scope(module);
    hc_vm_function_instruction_t* instr = &module->instructions[scope->first_instr];
    if (instr->instr != HC_VM_INSTR_IFNZ) {
        return HC_VM_BUILD_INCORRECT_SCOPE_ERR;
    }
    UNWRAP_ERROR(hc_vm_function_pack_instruction(module, HC_VM_INSTR_ELSE, NULL, NULL, NULL));
    instr = &module->instructions[scope->first_instr];
    instr->next_instruction = module->instr_count - 1;
    UNWRAP_ERROR(hc_vm_remove_scope(module));
    return hc_vm_add_scope(module);
}
uint64_t hc_vm_function_instr_endif(hc_vm_function_t *module) {
    hc_vm_function_scope_t* scope = hc_vm_last_scope(module);
    hc_vm_function_instruction_t* instr = &module->instructions[scope->first_instr];
    if (instr->instr != HC_VM_INSTR_IFNZ && instr->instr != HC_VM_INSTR_ELSE) {
        return HC_VM_BUILD_INCORRECT_SCOPE_ERR;
    }
    UNWRAP_ERROR(hc_vm_function_pack_instruction(module, HC_VM_INSTR_ENDIF, NULL, NULL, NULL));
    // instr pointer is invalidated
    instr = &module->instructions[scope->first_instr];
    instr->next_instruction = module->instr_count - 1;
    return hc_vm_remove_scope(module);
}

uint64_t hc_vm_function_instr_arr_length(
        hc_vm_function_t *module,
        const hc_vm_function_instruction_arg_t *arr,
        const hc_vm_function_instruction_arg_t *length) {
    ASSERT_WRITABLE(length);
    const hc_vm_type_t* arr_true_type;
    hc_vm_type_get_deref(arr->type_ptr, &arr_true_type);
    if (arr_true_type->type != HC_VM_TYPE_MULTI) {
        return HC_VM_BUILD_INVALID_TYPE_ERR;
    }
    const hc_vm_type_t* length_true_type;
    hc_vm_type_get_deref(length->type_ptr, &length_true_type);
    if (length_true_type->type != HC_VM_TYPE_ULONG) {
        return HC_VM_BUILD_INVALID_TYPE_ERR;
    }
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_ARR_LENGTH, arr, length, NULL);
}

uint64_t hc_vm_function_instr_arr_index(
        hc_vm_function_t *module,
        const hc_vm_function_instruction_arg_t *arr,
        const hc_vm_function_instruction_arg_t *idx,
        hc_vm_function_instruction_arg_t *target) {
    ASSERT_WRITABLE(target);
    // Argument is an array or a pointer to one
    const hc_vm_type_t* true_type;
    hc_vm_type_get_deref(arr->type_ptr, &true_type);
    if (true_type->type != HC_VM_TYPE_MULTI) {
        return HC_VM_BUILD_INVALID_TYPE_ERR;
    }
    UNWRAP_ERROR(hc_vm_check_pointer_safety(arr, target));
    UNWRAP_ERROR(hc_vm_update_double_pointer_ref_scope(target, arr->scope_idx));
    // idx should be a valid type too
    UNWRAP_ERROR(hc_vm_assert_types_compatible(idx->type_ptr, &hc_vm_type_ulong));
    // No pointer offset is calculated, since it may not be know at compile time
    // immidiate index checking perhaps?
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_ARR_INDEX, arr, idx, target);
}

uint64_t hc_vm_function_instr_struct_index(
        hc_vm_function_t *module,
        const hc_vm_function_instruction_arg_t *st,
        const hc_vm_function_instruction_arg_t *idx,
        hc_vm_function_instruction_arg_t *target) {
    ASSERT_WRITABLE(target);
    // Argument is a struct or a pointer to one
    const hc_vm_type_t* true_type;
    hc_vm_type_get_deref(st->type_ptr, &true_type);
    if (true_type->type != HC_VM_TYPE_STRUCT) {
        return HC_VM_BUILD_INVALID_TYPE_ERR;
    }
    UNWRAP_ERROR(hc_vm_assert_types_compatible(idx->type_ptr, &hc_vm_type_ulong));
    // require immidiate values
    if (!idx->immidiate) {
        return HC_VM_BUILD_NON_IMMIDIATE_VALUE;
    }
    UNWRAP_ERROR(hc_vm_check_pointer_safety(st, target));
    UNWRAP_ERROR(hc_vm_update_double_pointer_ref_scope(target, st->scope_idx));
    UNWRAP_ERROR(hc_vm_function_pack_instruction(module, HC_VM_INSTR_STRUCT_INDEX, st, idx, target));
    // No overflow is possible since those are checked on type creation
    uint64_t offset = 0;
    uint64_t idx_value = *(uint64_t*)idx->primitive;
    if (hc_vm_type_struct_offset(true_type, idx_value, &offset)) return 1;
    hc_vm_function_instruction_t *instr = hc_vm_function_last_instruction(module);
    // Offset is precalculated since it is relatively expensive
    instr->pointer_offset = offset;
    return 0;
}

uint64_t hc_vm_function_instr_trap(hc_vm_function_t *func, void* data) {
    if (data == NULL) {
        return HC_VM_BUILD_LEGACY_ERR;
    }
    hc_vm_function_instruction_arg_t arg;
    hc_vm_function_wrap_user_data(func, data, &arg);
    return hc_vm_function_pack_instruction(func, HC_VM_INSTR_TRAP, &arg, NULL, NULL);
}

uint64_t hc_vm_function_instr_return(hc_vm_function_t *module) {
    return hc_vm_function_pack_instruction(module, HC_VM_INSTR_RETURN, NULL, NULL, NULL);
}

static uint64_t hc_vm_function_instr_scope(hc_vm_function_t* function) {
    UNWRAP_ERROR(hc_vm_function_pack_instruction(function, HC_VM_INSTR_SCOPE, NULL, NULL, NULL));
    return hc_vm_add_scope(function);
}

static uint64_t hc_vm_function_instr_scope_end(hc_vm_function_t* function) {
    UNWRAP_ERROR(hc_vm_remove_scope(function));
    return hc_vm_function_pack_instruction(function, HC_VM_INSTR_SCOPE_END, NULL, NULL, NULL);
}

uint64_t hc_vm_function_instr_loop_ascending(
        hc_vm_function_t *function,
        const hc_vm_function_instruction_arg_t *start,
        const hc_vm_function_instruction_arg_t *limit,
        hc_vm_function_instruction_arg_t *idx_pointer) {
    UNWRAP_ERROR(hc_vm_assert_types_compatible(start->type_ptr, limit->type_ptr));
    const hc_vm_type_t* true_type;
    hc_vm_type_get_deref(start->type_ptr, &true_type);
    if (true_type->type != HC_VM_TYPE_ULONG) return 1;
    // Create a scope for utility variables
    UNWRAP_ERROR(hc_vm_function_instr_scope(function));
    // Create a counter and set its contents
    hc_vm_function_instruction_arg_t counter;
    UNWRAP_ERROR(hc_vm_function_instr_push_stack(function, true_type, &counter));
    UNWRAP_ERROR(hc_vm_function_instr_set(function, start, &counter));
    *idx_pointer = counter;
    // Cannot edit the counter
    idx_pointer->user_read_only = true;
    // copy the limit, to avoid the loop limit extension
    hc_vm_function_instruction_arg_t copied_limit;
    UNWRAP_ERROR(hc_vm_function_instr_push_stack(function, true_type, &copied_limit));
    UNWRAP_ERROR(hc_vm_function_instr_set(function, limit, &copied_limit));
    hc_vm_function_instruction_arg_t step;
    uint64_t step_val = 1;
    hc_vm_function_data_immidiate(&step_val, &step, &hc_vm_type_ulong);
    // Create the actual instruciton with copied args
    UNWRAP_ERROR(hc_vm_function_pack_instruction(function, HC_VM_INSTR_LOOP_ASC, &counter, &copied_limit, &step));
    // open one more scope for internal variables
    return hc_vm_add_scope(function);
}

uint64_t hc_vm_function_instr_loop_ascending_end(hc_vm_function_t *function) {
    hc_vm_function_scope_t* scope = hc_vm_last_scope(function);
    hc_vm_function_instruction_t* instr = &function->instructions[scope->first_instr];
    if (instr->instr != HC_VM_INSTR_LOOP_ASC) {
        return HC_VM_BUILD_INCORRECT_SCOPE_ERR;
    }
    instr->next_instruction = function->instr_count;
    UNWRAP_ERROR(hc_vm_function_pack_instruction(function, HC_VM_INSTR_LOOP_ASC_END, NULL, NULL, NULL));
    instr = hc_vm_function_last_instruction(function);
    // go back to the loop body
    instr->next_instruction = scope->first_instr;
    UNWRAP_ERROR(hc_vm_remove_scope(function));
    return hc_vm_function_instr_scope_end(function);
}

uint64_t hc_vm_function_instr_loop(hc_vm_function_t* func) {
    UNWRAP_ERROR(hc_vm_function_pack_instruction(func, HC_VM_INSTR_LOOP, NULL, NULL, NULL));
    func->metadata.finite_run_time = false;
    return hc_vm_add_scope(func);
}

uint64_t hc_vm_function_instr_loop_end(hc_vm_function_t* func) {
    hc_vm_function_scope_t* scope = hc_vm_last_scope(func);
    hc_vm_function_instruction_t* start_instr = &func->instructions[scope->first_instr];
    if (start_instr->instr != HC_VM_INSTR_LOOP) {
        return HC_VM_BUILD_INCORRECT_SCOPE_ERR;
    }
    UNWRAP_ERROR(hc_vm_function_pack_instruction(func, HC_VM_INSTR_LOOP_END, NULL, NULL, NULL));
    start_instr = &func->instructions[scope->first_instr];
    hc_vm_function_instruction_t* end_instr = hc_vm_function_last_instruction(func);
    end_instr->next_instruction = scope->first_instr;
    start_instr->next_instruction = func->instr_count - 1;
    return hc_vm_remove_scope(func);
}

uint64_t hc_vm_function_instr_call(
        hc_vm_function_t* function,
        const hc_vm_function_t* other_function,
        const hc_vm_function_instruction_arg_t* arg) {
    if (arg != NULL && other_function->metadata.signature != arg->type_ptr) {
        return HC_VM_BUILD_SIGNATURE_MISMATCH;
    }
    function->metadata.finite_run_time &= other_function->metadata.finite_run_time;
    UNWRAP_ERROR(hc_vm_function_instr_scope(function));
    uint64_t raw_stack_offset = hc_vm_current_stack_offset_raw(function);
    uint64_t new_stack_offset = raw_stack_offset;
    // Allocate alignment, so function's memory is correctly aligned
    hc_vm_type_align(&new_stack_offset, hc_vm_max_aligment);
    ALLOC_STACK_MEMORY(function, new_stack_offset - raw_stack_offset);
    // Remember amount of required memory
    uint64_t pseudo_stack_depth = hc_vm_stack_depth(function);
    ALLOC_STACK_MEMORY(function, other_function->metadata.required_stack_bytes);
    hc_vm_function_instruction_arg_t wrapped_function;
    hc_vm_function_wrap_user_data(function, (void*)other_function, &wrapped_function);
    UNWRAP_ERROR(hc_vm_function_pack_instruction(function, HC_VM_INSTR_CALL, &wrapped_function, arg, NULL));
    // Write old required memory to avoid allocating all the memory at once
    hc_vm_function_instruction_t* instr = hc_vm_function_last_instruction(function);
    instr->stack_depth = pseudo_stack_depth;
    return hc_vm_function_instr_scope_end(function);
}

uint64_t hc_vm_function_instr_native_call(
        hc_vm_function_t* func,
        const hc_vm_native_function_t* native_func,
        const hc_vm_function_instruction_arg_t* arg) {
    if (native_func->signature != arg->type_ptr) {
        return HC_VM_BUILD_SIGNATURE_MISMATCH;
    }
    hc_vm_function_instruction_arg_t wrapped_function;
    hc_vm_function_wrap_user_data(func, (void*)native_func, &wrapped_function);
    return hc_vm_function_pack_instruction(func, HC_VM_INSTR_NATIVE_CALL, &wrapped_function, arg, NULL);
}

uint64_t hc_vm_function_finalize(hc_vm_function_t* module) {
    // Terminate the function
    hc_vm_remove_scope(module);
    uint64_t error = hc_vm_function_instr_return(module);
    if (module->build_context->last_scope != 0) {
        error = HC_VM_BUILD_UNCLOSED_SCOPE_ERR;
    }
    hc_vm_type_init_multi(&hc_vm_type_byte, sizeof(hc_vm_executor_state_t), &module->metadata.ti_struct);

    uint64_t ti_struct_size = module->metadata.ti_struct.size_bytes;
    UNWRAP_ERROR(hc_vm_type_align(&ti_struct_size, hc_vm_max_aligment));

    if (UINT64_MAX - ti_struct_size < module->metadata.required_stack_bytes) {
        return HC_VM_BUILD_STACK_SIZE_ERR;
    }
    module->metadata.required_stack_bytes += ti_struct_size;
    module->metadata.red_zone_bytes = ti_struct_size;

    // build context is no longer required
    free(module->build_context);
    return error;
}

static uint64_t hc_vm_find_first_loop(hc_vm_function_t* module, uint64_t* loop_index) {
    uint64_t idx = module->build_context->last_scope;
    // First scope is never a loop
    while(idx > 0) {
        hc_vm_function_scope_t* scope = &module->build_context->scopes[idx];
        uint64_t instr_idx = scope->first_instr;
        uint64_t isntr_type = module->instructions[instr_idx].instr;
        if (isntr_type == HC_VM_INSTR_LOOP_ASC || isntr_type == HC_VM_INSTR_LOOP) {
            *loop_index = instr_idx;
            return HC_VM_BUILD_OK;
        }
        --idx;
    }
    return HC_VM_BUILD_LOOP_EXPECTED;
}

uint64_t hc_vm_function_instr_continue(hc_vm_function_t* func) {
    uint64_t instr_idx;
    UNWRAP_ERROR(hc_vm_find_first_loop(func, &instr_idx));
    UNWRAP_ERROR(hc_vm_function_pack_instruction(func, HC_VM_INSTR_CONTINUE, NULL, NULL, NULL));
    hc_vm_function_instruction_t* instr = hc_vm_function_last_instruction(func);
    instr->next_instruction = instr_idx;
    return HC_VM_BUILD_OK;
}

uint64_t hc_vm_function_instr_break(hc_vm_function_t* func) {
    uint64_t instr_idx;
    UNWRAP_ERROR(hc_vm_find_first_loop(func, &instr_idx));
    UNWRAP_ERROR(hc_vm_function_pack_instruction(func, HC_VM_INSTR_BREAK, NULL, NULL, NULL));
    hc_vm_function_instruction_t* instr = hc_vm_function_last_instruction(func);
    instr->next_instruction = instr_idx;
    return HC_VM_BUILD_OK;
}
