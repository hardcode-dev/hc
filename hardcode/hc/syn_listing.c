#include "hc_syn.h"
#include "hc_syn_rpn.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int print_syn_listing(FILE *fh, const hc_syn_insn_t *instructions, int instruction_count);

char *make_syn_listing(const hc_syn_insn_t *instructions, int instruction_count, int *listing_len) {
    char *buffer;
    size_t size;
    FILE *fh = open_memstream(&buffer, &size);
    if (fh) {
        if (print_syn_listing(fh, instructions, instruction_count) >= 0) {
            if (fclose(fh) >= 0) {
                *listing_len = size;
                return buffer;
            } else {
                fh = NULL;
            }
        }
    } else {
        buffer = NULL;
    }
    if (fh)
        fclose(fh);
    free(buffer);
    *listing_len = 0;
    return NULL;
}

static int syn_insn_print(FILE *fh, int type, const hc_syn_insn_value_t *value) {
    switch (type) {
    case HC_SYN_INSN_TYPE_STRING:
        return fprintf(fh, "\"%.*s\"", value->_string.len, (char *)value->_string.data);
    case HC_SYN_INSN_TYPE_WORD:
        return fprintf(fh, "%" PRIdPTR " <word>", value->_word);
    case HC_SYN_INSN_TYPE_UWORD:
        return fprintf(fh, "%" PRIuPTR " <uword>", value->_uword);
    case HC_SYN_INSN_TYPE_BYTE:
        return fprintf(fh, "%d <byte>", (int)value->_byte);
    case HC_SYN_INSN_TYPE_UBYTE:
        return fprintf(fh, "%u <ubyte>", (unsigned int)value->_ubyte);
    case HC_SYN_INSN_TYPE_SHORT:
        return fprintf(fh, "%d <short>", (int)value->_short);
    case HC_SYN_INSN_TYPE_USHORT:
        return fprintf(fh, "%d <ushort>", (unsigned int)value->_ushort);
    case HC_SYN_INSN_TYPE_INT:
        return fprintf(fh, "%d <int>", value->_int);
    case HC_SYN_INSN_TYPE_UINT:
        return fprintf(fh, "%u <uint>", value->_uint);
    case HC_SYN_INSN_TYPE_LONG:
        return fprintf(fh, "%" PRId64 " <long>", value->_long);
    case HC_SYN_INSN_TYPE_ULONG:
        return fprintf(fh, "%" PRIu64 " <ulong>", value->_ulong);
    case HC_SYN_INSN_TYPE_FLOAT:
        return fprintf(fh, "%g <float>", value->_float);
    case HC_SYN_INSN_TYPE_DOUBLE:
        return fprintf(fh, "%g <double>", value->_double);
    case HC_SYN_INSN_DECLARATION:
        // I need the declaration info
        return fprintf(fh, "<declaration>");
    }
    return fprintf(fh, "<unknown>");
}
static int print_syn_insn(FILE *fh, const void *insn) {
    const hc_syn_insn_t *syn_insn = insn;

#define STRING_OF_FIRST_ARG(a, b) {.name = #a, .name_len = sizeof(#a) - sizeof("")},
    static const struct {
        const char *name;
        size_t name_len;
    } names[] = {HC_SYN_DEFINE_INSTRUCTIONS(STRING_OF_FIRST_ARG)};
#undef STRING_OF_FIRST_ARG

    static const size_t alignment = 24;

    if (fprintf(fh, " - [%s]", names[HC_SYN_INSN_INDEX(syn_insn->type)].name) < 0)
        return -1;
    if (HC_SYN_INSN_VALUE_TYPE(syn_insn->type) == HC_SYN_INSN_TYPE_VOID) {
        if (fprintf(fh, "\n") < 0)
            return -1;
    } else {
        size_t j = 0;
        for (j = 0; j < (alignment - names[HC_SYN_INSN_INDEX(syn_insn->type)].name_len); ++j)
            if (putc(' ', fh) < 0)
                return -1;
        if (fprintf(fh, "(") < 0)
            return -1;
        if (syn_insn_print(fh, HC_SYN_INSN_VALUE_TYPE(syn_insn->type), &syn_insn->value) < 0)
            return -1;
        if (fprintf(fh, ")\n") < 0)
            return -1;
    }
    return 0;
}

static int print_syn_listing(FILE *fh, const hc_syn_insn_t *instructions, int instruction_count) {
    int i;
    for (i = 0; i < instruction_count; ++i) {
        if (print_syn_insn(fh, instructions + i) < 0)
            return -1;
    }
    return 0;
}
