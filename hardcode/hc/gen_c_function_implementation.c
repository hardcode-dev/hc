#include "gen_c_function_implementation.h"

#include "hc_syn.h"
#include "hc_syn_rpn.h"
#include "hc_sem.h"
#include "hc_sem_rpn.h"
#include "hc_module.h"
#include "hc_backend_c.h"
#include "syn_listing.h"
#include "sem_listing.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


int gen_c_function_implementation (hc_module_t *module, char **generated, int *generated_len, char *error_message, size_t max_error_message_len)
{
    hc_module_sem_t *module_sem = hc_module_sem (module);
    hc_sem_rpn_t *sem_rpn = module_sem->function_body;
    if (!sem_rpn) {
	snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing semantic tree");
        return -1;
    }

    *generated = hc_backend_c_generate (hc_sem_rpn_get_instructions (sem_rpn), hc_sem_rpn_get_instruction_count (sem_rpn), generated_len, error_message, max_error_message_len);

    if (!*generated) {
	free (*generated);
	return -1;
    }

    return 0;
}
