#pragma once

typedef struct hc_sem_insn_t hc_sem_insn_t;


extern char *make_sem_listing (
    const hc_sem_insn_t *instructions,
    int instruction_count,
    int *listing_len);
