#include "generate_c.h"

#include "gen_c_function_implementation.h"
#include "gen_c_type_tree.h"
#include "gen_c_function_tree.h"
#include "gen_c_function_implementation_loader.h"
#include "hc_syn.h"
#include "hc_syn_rpn.h"
#include "hc_sem.h"
#include "hc_sem_rpn.h"
#include "hc_parser.h"
#include "hc_sem_analyzer.h"
#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_snapshot.h"
#include "syn_listing.h"
#include "sem_listing.h"

#include <string.h>
#include <errno.h>
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>


static int compile_self_and_children (hc_module_t *module, const char *path, const char *name, FILE *impl_fh, FILE *impl_verbose_fh, FILE *load_fh, char *error_message, size_t max_error_message_len);

static int check_generated_c_path_directory (const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    struct stat st;
    if (!lstat (generated_c_path, &st)) {
        if ((st.st_mode & S_IFMT) != S_IFDIR) {
            snprintf (error_message, max_error_message_len, "Failed to create directory '%s': inode already exists but isn't a directory", generated_c_path);
            return -1;
        }
        return 0;
    }
    if (errno != ENOENT) {
        snprintf (error_message, max_error_message_len, "Failed to stat inode '%s': %s", generated_c_path, strerror (errno));
        return -1;
    }
    if (mkdir (generated_c_path, 0777)) {
        snprintf (error_message, max_error_message_len, "Failed to create directory '%s': %s", generated_c_path, strerror (errno));
        return -1;
    }

    return 0;
}
static int output_implementation (hc_snapshot_t *snapshot, const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    hc_module_t *root = hc_snapshot_root (snapshot);
    FILE *impl_fh;
    FILE *impl_verbose_fh;
    FILE *load_fh;
    {
	char fname[512];
	snprintf (fname, sizeof (fname), "%s/.generated_functions_impl.h", generated_c_path);
	if (!(impl_fh = fopen (fname, "w"))) {
	    snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
            return -1;
	}
    }
    {
	char fname[512];
	snprintf (fname, sizeof (fname), "%s/.generated_functions_impl_verbose.h", generated_c_path);
	if (!(impl_verbose_fh = fopen (fname, "w"))) {
	    snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
            fclose (impl_fh);
            return -1;
	}
    }
    {
	char fname[512];
	snprintf (fname, sizeof (fname), "%s/.generated_functions_load.h", generated_c_path);
	if (!(load_fh = fopen (fname, "w"))) {
	    snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
            fclose (impl_fh);
            fclose (impl_verbose_fh);
            return -1;
	}
    }
    int ret = compile_self_and_children (root, "", NULL, impl_fh, impl_verbose_fh, load_fh, error_message, max_error_message_len);
    fclose (impl_fh);
    fclose (impl_verbose_fh);
    fclose (load_fh);
    return ret;
}
static int output_types (hc_snapshot_t *snapshot, const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    char fname[512];
    snprintf (fname, sizeof (fname), "%s/.types.h", generated_c_path);
    FILE *fh = fopen (fname, "w");
    if (!fh) {
        snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
        return -1;
    }
    if (!gen_c_type_tree (snapshot, fh)) {
        snprintf (error_message, max_error_message_len, "Failed to generate type tree");
        fclose (fh);
        return -1;
    }
    fclose (fh);
    return 0;
}
static int output_hc_type (const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    static const char data[] =
"#pragma once\n"
"\n"
"#include \".types.h\"\n"
"\n"
"\n"
"extern hc_type_t hc_type;\n"
"\n"
"#define HC_TYPE(type) typeof (hc_type.type._)\n";
    size_t data_len = sizeof (data) - sizeof ("");

    char fname[512];
    snprintf (fname, sizeof (fname), "%s/.hc_type.h", generated_c_path);
    FILE *fh = fopen (fname, "w");
    if (!fh) {
        snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
        return -1;
    }
    if (fwrite (data, 1, data_len, fh) != data_len) {
        snprintf (error_message, max_error_message_len, "Failed to write to file '%s': %s", fname, strerror (errno));
        fclose (fh);
        return -1;
    }
    fclose (fh);
    return 0;
}
static int output_hc_types (const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    static const char data[] =
"#pragma once\n"
"\n"
"#include <stdint.h>\n"
"\n"
"typedef void *hc_trait_t;\n"
"typedef void *hc_class_t;\n"
"typedef void *hc_struct_t;\n"
"typedef void *hc_trait_t;\n"
"typedef void *hc_union_t;\n"
"\n"
"typedef struct hc_slice_t {\n"
"    struct hc_slice_t *parent;\n"
"    intptr_t size;\n"
"    // Data: sizeof(element)*size\n"
"} hc_slice_t;\n"
"\n"
"typedef struct hc_fixed_slice_t {\n"
"    struct hc_fixed_slice_t *parent;\n"
"    // Data: sizeof(element)*size\n"
"} hc_fixed_slice_t;\n"
"\n"
"typedef struct hc_variant_t {\n"
"    intptr_t type;\n"
"    // Data\n"
"} hc_variant_t;\n"
"\n"
"typedef struct hc_error_t {\n"
"    struct hc_error_t *next;\n"
"    union {\n"
"	int code;\n"
"	intptr_t code_padding;\n"
"    };\n"
"    char *unit;\n"
"    char *message;\n"
"    // String data\n"
"} hc_error_t;\n"
"\n"
"typedef intptr_t hc_index_t;\n"
"typedef intptr_t hc_bool_t;\n"
"\n"
"/*\n"
" * Iterator ??\n"
" * Key ??\n"
" */\n";
    size_t data_len = sizeof (data) - sizeof ("");

    char fname[512];
    snprintf (fname, sizeof (fname), "%s/.hc_types.h", generated_c_path);
    FILE *fh = fopen (fname, "w");
    if (!fh) {
        snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
        return -1;
    }
    if (fwrite (data, 1, data_len, fh) != data_len) {
        snprintf (error_message, max_error_message_len, "Failed to write to file '%s': %s", fname, strerror (errno));
        fclose (fh);
        return -1;
    }
    fclose (fh);
    return 0;
}
static int output_function_tree (hc_snapshot_t *snapshot, const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    char fname[512];
    snprintf (fname, sizeof (fname), "%s/.functions.h", generated_c_path);
    FILE *fh = fopen (fname, "w");
    if (!fh) {
        snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
        return -1;
    }
    if (!gen_c_function_tree (snapshot, fh)) {
        snprintf (error_message, max_error_message_len, "Failed to generate function tree");
        fclose (fh);
        return -1;
    }
    fclose (fh);
    return 0;
}
static int output_function_implementation_loader (hc_snapshot_t *snapshot, const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    char fname[512];
    snprintf (fname, sizeof (fname), "%s/.load_functions_impl.h", generated_c_path);
    FILE *fh = fopen (fname, "w");
    if (!fh) {
        snprintf (error_message, max_error_message_len, "Failed to open file '%s': %s", fname, strerror (errno));
        return -1;
    }
    if (!gen_c_function_implementation_loader (snapshot, fh)) {
        snprintf (error_message, max_error_message_len, "Failed to generate function tree");
        fclose (fh);
        return -1;
    }
    fclose (fh);
    return 0;
}
int generate_c (hc_snapshot_t *snapshot, const char *generated_c_path, char *error_message, size_t max_error_message_len)
{
    if (check_generated_c_path_directory (generated_c_path, error_message, max_error_message_len))
        return -1;
    if (output_implementation (snapshot, generated_c_path, error_message, max_error_message_len))
        return -1;
    if (output_types (snapshot, generated_c_path, error_message, max_error_message_len))
        return -1;
    if (output_hc_type (generated_c_path, error_message, max_error_message_len))
        return -1;
    if (output_hc_types (generated_c_path, error_message, max_error_message_len))
        return -1;
    if (output_function_tree (snapshot, generated_c_path, error_message, max_error_message_len))
        return -1;
    if (output_function_implementation_loader (snapshot, generated_c_path, error_message, max_error_message_len))
        return -1;

    return 0;
}

typedef struct callback_state_t {
    const char *path;
    FILE *impl_fh;
    FILE *impl_verbose_fh;
    FILE *load_fh;
    char *error_message;
    size_t max_error_message_len;
} callback_state_t;

static int callback (const char *name, hc_module_t *module, callback_state_t *state)
{
    char path[512];
    if (*state->path)
	snprintf (path, sizeof (path), "%s.%s", state->path, name);
    else
	snprintf (path, sizeof (path), "%s", name);
    return compile_self_and_children (module, path, name, state->impl_fh, state->impl_verbose_fh, state->load_fh, state->error_message, state->max_error_message_len);
}
static int print_func_name (const char *module_name, FILE *fh)
{
    if (fputs ("_module_func__", fh) == EOF)
        return -1;
    const char *ptr = module_name;
    char c;
    while ((c = *(ptr++))) {
	switch (c) {
	case '_':
	    if (fputc ('_', fh) == EOF)
                return -1;
	    if (fputc ('_', fh) == EOF)
                return -1;
	    break;
	case '.':
            if (fputc ('_', fh) == EOF)
                return -1;
	    if (fputc ('D', fh) == EOF)
                return -1;
            if (fputc ('_', fh) == EOF)
                return -1;
	    break;
	default:
	    if (fputc (c, fh) == EOF)
                return -1;
	}
    }
    return 0;
}
static int print_impl (hc_module_t *module, const char *path, FILE *fh,
                       const char *function_return_type_syn_listing, int function_return_type_syn_listing_len,
                       const char *function_parameters_syn_listing, int function_parameters_syn_listing_len,
                       const char *syn_listing, int syn_listing_len,
                       const char *sem_listing, int sem_listing_len,
                       const char *generated, int generated_len,
                       int store_listings)
{
    hc_module_source_t *module_source = hc_module_source (module);
    if (fprintf (fh, "/* %.*s %s (%.*s) */\n",
                 (int) module_source->function_return_type_baked.len, (char *) module_source->function_return_type_baked.data,
                 path,
                 (int) module_source->function_parameters_baked.len, (char *) module_source->function_parameters_baked.data) < 0)
        return -1;
    if (fprintf (fh, "static %.*s ", (int) module_source->function_return_type_baked.len, (char *) module_source->function_return_type_baked.data) < 0)
        return -1;
    if (print_func_name (path, fh) < 0)
        return -1;
    if (fprintf (fh, " (%.*s)\n", (int) module_source->function_parameters_baked.len, (char *) module_source->function_parameters_baked.data) < 0)
        return -1;
    if (fprintf (fh, "{\n") < 0)
        return -1;
    if (fprintf (fh, "%.*s\n", generated_len, generated) < 0)
        return -1;
    if (store_listings) {
        if (fprintf (fh, "\n"
                     "/*\n"
                     "[Return type syntactic RPN:]\n"
                     "%.*s"
                     "\n"
                     "[Parameters syntactic RPN:]\n"
                     "%.*s"
                     "\n"
                     "[Syntactic RPN:]\n"
                     "%.*s"
                     "\n"
                     "<Semantic RPN:>\n"
                     "%.*s"
                     " */\n",
                     function_return_type_syn_listing_len, function_return_type_syn_listing,
                     function_parameters_syn_listing_len, function_parameters_syn_listing,
                     syn_listing_len, syn_listing,
                     sem_listing_len, sem_listing) < 0)
            return -1;
    }
    if (fprintf (fh, "}\n") < 0)
        return -1;
    if (fprintf (fh, "\n") < 0)
        return -1;
    return 0;
}
static int print_loader (const char *path, FILE *fh)
{
    if (fprintf (fh, "HC_CALL (%s) = ", path) < 0)
        return -1;
    if (print_func_name (path, fh) < 0)
        return -1;
    if (fputs (";\n", fh) == EOF)
        return -1;
    return 0;
}
static int gen_function_implementation (hc_module_t *module, const char *path, FILE *impl_fh, FILE *impl_verbose_fh, FILE *load_fh, char *error_message, size_t max_error_message_len)
{
    hc_module_syn_t *module_syn = hc_module_syn (module);
    if (!module_syn->function_return_type) {
        snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing function return type syntactic tree");
        return -1;
    }
    if (!module_syn->function_parameters) {
        snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing function parameters syntactic tree");
        return -1;
    }
    if (!module_syn->function_body) {
	snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing function body syntactic tree");
        return -1;
    }

    hc_module_sem_t *module_sem = hc_module_sem (module);
    /* if (!module_sem->function_return_type) { */
    /*     snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing function return type semantic tree"); */
    /*     return -1; */
    /* } */
    /* if (!module_sem->function_parameters) { */
    /*     snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing function parameters semantic tree"); */
    /*     return -1; */
    /* } */
    if (!module_sem->function_body) {
	snprintf (error_message, max_error_message_len, "Failed to generate function implementation: missing function body semantic tree");
        return -1;
    }


    int function_return_type_syn_listing_len;
    char *function_return_type_syn_listing = make_syn_listing (hc_syn_rpn_get_instructions (module_syn->function_return_type),
                                                               hc_syn_rpn_get_instruction_count (module_syn->function_return_type), &function_return_type_syn_listing_len);
    int function_parameters_syn_listing_len;
    char *function_parameters_syn_listing = make_syn_listing (hc_syn_rpn_get_instructions (module_syn->function_parameters),
                                                              hc_syn_rpn_get_instruction_count (module_syn->function_parameters), &function_parameters_syn_listing_len);
    int function_body_syn_listing_len;
    char *function_body_syn_listing = make_syn_listing (hc_syn_rpn_get_instructions (module_syn->function_body),
                                                        hc_syn_rpn_get_instruction_count (module_syn->function_body), &function_body_syn_listing_len);

    /* int function_return_type_sem_listing_len; */
    /* char *function_return_type_sem_listing = make_sem_listing (hc_sem_rpn_get_instructions (module_sem->function_return_type), */
    /*                                                            hc_sem_rpn_get_instruction_count (module_sem->function_return_type), &function_return_type_sem_listing_len); */
    /* int function_parameters_sem_listing_len; */
    /* char *function_parameters_sem_listing = make_sem_listing (hc_sem_rpn_get_instructions (module_sem->function_parameters), */
    /*                                                           hc_sem_rpn_get_instruction_count (module_sem->function_parameters), &function_parameters_sem_listing_len); */
    int function_body_sem_listing_len;
    char *function_body_sem_listing = make_sem_listing (hc_sem_rpn_get_instructions (module_sem->function_body),
                                                        hc_sem_rpn_get_instruction_count (module_sem->function_body), &function_body_sem_listing_len);


    char *generated;
    int generated_len;

    int status = gen_c_function_implementation (module, &generated, &generated_len, error_message, max_error_message_len);
    if (status)
        return -1;

    if (print_impl (module, path, impl_fh,
                    function_return_type_syn_listing, function_return_type_syn_listing_len,
                    function_parameters_syn_listing, function_parameters_syn_listing_len,
                    function_body_syn_listing, function_body_syn_listing_len,
                    function_body_sem_listing, function_body_sem_listing_len,
                    generated, generated_len,
                    0)) {
	snprintf (error_message, max_error_message_len, "Failed to write to implementation file: %s", strerror (errno));
        goto fail;
    }
    if (print_impl (module, path, impl_verbose_fh,
                    function_return_type_syn_listing, function_return_type_syn_listing_len,
                    function_parameters_syn_listing, function_parameters_syn_listing_len,
                    function_body_syn_listing, function_body_syn_listing_len,
                    function_body_sem_listing, function_body_sem_listing_len,
                    generated, generated_len,
                    1)) {
	snprintf (error_message, max_error_message_len, "Failed to write to verbose implementation file: %s", strerror (errno));
        goto fail;
    }
    if (print_loader (path, load_fh)) {
	snprintf (error_message, max_error_message_len, "Failed to write to write to loading file: %s", strerror (errno));
        goto fail;
    }
    free (function_return_type_syn_listing);
    free (function_parameters_syn_listing);
    free (function_body_syn_listing);
    free (function_body_sem_listing);
    free (generated);
    return 0;

fail:
    free (function_return_type_syn_listing);
    free (function_parameters_syn_listing);
    free (function_body_syn_listing);
    free (function_body_sem_listing);
    free (generated);
    return -1;
}
static int compile_self_and_children (hc_module_t *module, const char *path, const char *name, FILE *impl_fh, FILE *impl_verbose_fh, FILE *load_fh, char *error_message, size_t max_error_message_len)
{
    hc_module_source_t *module_source = hc_module_source (module);
    hc_module_map_t *module_children = hc_module_children (module);
    if (name && module_source->function_body.data) { // TODO: Include return type and parameters
	if (gen_function_implementation (module, path, impl_fh, impl_verbose_fh, load_fh, error_message, max_error_message_len))
            return -1;
    }
    callback_state_t state = {
	.path = path,
	.impl_fh = impl_fh,
	.impl_verbose_fh = impl_verbose_fh,
	.load_fh = load_fh,
        .error_message = error_message,
        .max_error_message_len = max_error_message_len,
    };
    return hc_module_map_foreach (module_children, (void *) callback, &state);
}
