#pragma once

#include <stddef.h>

typedef struct hc_module_t hc_module_t;


extern int gen_c_function_implementation (
    hc_module_t *module,
    char **generated,
    int *generated_len,
    char *error_message,
    size_t max_error_message_len);
