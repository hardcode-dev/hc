#include "gen_c_type_tree.h"

#include "hc_syn.h"
#include "hc_syn_rpn.h"
#include "hc_sem.h"
#include "hc_sem_rpn.h"
#include "hc_snapshot.h"
#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_backend_c.h"
#include "syn_listing.h"
#include "sem_listing.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


typedef struct callback_state_t {
    const char *path;
    FILE *fh;
    int depth;
} callback_state_t;

static int handle_self_and_children (hc_module_t *module, const char *path, const char *name, int depth, FILE *fh);

int gen_c_function_tree (hc_snapshot_t *snapshot, FILE *fh)
{
    fprintf (fh, "#pragma once\n");
    fprintf (fh, "\n");
    fprintf (fh, "#include <stdint.h>\n");
    fprintf (fh, "\n");
    fprintf (fh, "#include \".hc_types.h\"\n");
    fprintf (fh, "#include \".types.h\"\n");
    fprintf (fh, "\n");
    fprintf (fh, "typedef ");
    int ret = handle_self_and_children (hc_snapshot_root (snapshot), "", "hc_function_t", 0, fh);
    return ret;
}

static int callback (const char *name, hc_module_t *module, callback_state_t *state)
{
    char path[512];
    if (*state->path)
	snprintf (path, sizeof (path), "%s.%s", state->path, name);
    else
	snprintf (path, sizeof (path), "%s", name);
    return handle_self_and_children (module, path, name, state->depth, state->fh) ? 0 : -1;
}
static int handle_self_and_children (hc_module_t *module, const char *path, const char *name, int depth, FILE *fh)
{
    hc_module_source_t *module_source = hc_module_source (module);
    hc_module_map_t *module_children = hc_module_children (module);

    int identation = depth*4;
    int i, j;
    for (j = 0; j < identation; ++j)
        fputc (' ', fh);
    fputs ("struct {\n", fh);
    if (module_source->function_return_type_baked.data &&
        module_source->function_parameters_baked.data) { // TODO: Generate from parsed 'function_return_type' and 'function_parameters'
        int identation = (depth + 1)*4;
        for (j = 0; j < identation; ++j)
            fputc (' ', fh);
        fprintf (fh, "%.*s (*_) (%.*s);\n",
                 (int) module_source->function_return_type_baked.len, (char*) module_source->function_return_type_baked.data,
                 (int) module_source->function_parameters_baked.len, (char*) module_source->function_parameters_baked.data);
    } else if (module_source->_bind_baked.data) { // TODO: Generate from parsed 'bind'
        int identation = (depth + 1)*4;
        for (j = 0; j < identation; ++j)
            fputc (' ', fh);
        fprintf (fh, "%.*s\n", (int) module_source->_bind_baked.len, (char*) module_source->_bind_baked.data);
    }
#if 0
    if (exists $current_module->{data}->{function_return_type_baked} && exists $current_module->{data}->{function_parameters_baked}) {
	my $function_return_type_baked = $current_module->{data}->{function_return_type_baked};
	chomp ($function_return_type_baked);
	my $function_parameters_baked = $current_module->{data}->{function_parameters_baked};
	chomp ($function_parameters_baked);
	print $fh (" " x $identation, "    $function_return_type_baked (*_) ($function_parameters_baked);\n");
    } elsif (exists $current_module->{data}->{bind_baked} && ((@{$current_module->{data}->{bind_baked}}) > 0)) {
    	my $bind_baked_list = $current_module->{data}->{bind_baked};
    	(@$bind_baked_list) == 1 or die "Unsupported multiple binds";
	my $bind_baked = $bind_baked_list->[0];
	print $fh (" " x $identation, "    $bind_baked->{return_type} (*_) $bind_baked->{parameters};\n");
    }
#endif
    callback_state_t state = {
	.path = path,
	.fh = fh,
        .depth = depth + 1,
    };
    if (hc_module_map_foreach (module_children, (void *) callback, &state))
        return 0;
    for (i = 0; i < identation; ++i)
        fputc (' ', fh);
    fprintf (fh, "} %s;\n", name);

    return 1;
}
