#pragma once

#include <stddef.h>

typedef struct hc_snapshot_t hc_snapshot_t;

extern int generate_c (
    hc_snapshot_t *snapshot,
    const char *generated_c_path,
    char *error_message,
    size_t max_error_message_len);
