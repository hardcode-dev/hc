#include "gen_c_type_tree.h"

#include "hc_syn.h"
#include "hc_syn_rpn.h"
#include "hc_sem.h"
#include "hc_sem_rpn.h"
#include "hc_snapshot.h"
#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_backend_c.h"
#include "syn_listing.h"
#include "sem_listing.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


typedef struct callback_state_t {
    const char *path;
    FILE *fh;
    int depth;
} callback_state_t;

static int handle_self_and_children (hc_module_t *module, const char *path, const char *name, int depth, FILE *fh);

int gen_c_function_implementation_loader (hc_snapshot_t *snapshot, FILE *fh)
{
    int ret = handle_self_and_children (hc_snapshot_root (snapshot), "", "hc_type_t", 0, fh);
    return ret;
}

static int callback (const char *name, hc_module_t *module, callback_state_t *state)
{
    char path[512];
    if (*state->path)
	snprintf (path, sizeof (path), "%s.%s", state->path, name);
    else
	snprintf (path, sizeof (path), "%s", name);
    return handle_self_and_children (module, path, name, state->depth, state->fh) ? 0 : -1;
}
static int handle_self_and_children (hc_module_t *module, const char *path, const char *name, int depth, FILE *fh)
{
    (void) name;

    hc_module_source_t *module_source = hc_module_source (module);
    hc_module_map_t *module_children = hc_module_children (module);

    if (module_source->_bind_name.data &&
        module_source->_bind_baked.data) { // TODO: Generate from parsed 'bind'
	fprintf (fh, "HC_CALL (%s) = hc_find_function (\"%.*s\"); /* %.*s */\n",
                 path,
                 (int) module_source->_bind_name.len, (char*) module_source->_bind_name.data,
                 (int) module_source->_bind_baked.len, (char*) module_source->_bind_baked.data);
    }
    callback_state_t state = {
	.path = path,
	.fh = fh,
        .depth = depth + 1,
    };
    if (hc_module_map_foreach (module_children, (void *) callback, &state))
        return 0;

    return 1;
}
