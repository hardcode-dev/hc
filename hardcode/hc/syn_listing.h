#pragma once

typedef struct hc_syn_insn_t hc_syn_insn_t;


extern char *make_syn_listing (
    const hc_syn_insn_t *instructions,
    int instruction_count,
    int *listing_len);
