#include "generate_c.h"
#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_parser.h"
#include "hc_snapshot.h"
#include "hc_syn_rpn.h"
#include "syn_listing.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int target_compile_snapshot (const char *snapshot_path, const char *generated_c_path)
{
    char error_message[512];
    hc_snapshot_t *snapshot = hc_snapshot_load (snapshot_path, error_message, sizeof (error_message));
    if (!snapshot) {
        fprintf (stderr, "Failed to load snapshot at '%s': %s\n", snapshot_path, error_message);
        return 1;
    }
    if (hc_snapshot_parse (snapshot)) {
        fprintf (stderr, "Failed to parse snapshot at '%s': %s\n", snapshot_path, error_message);
        hc_snapshot_destroy (snapshot);
        return 1;
    }
    if (hc_snapshot_sem_analyze (snapshot)) {
        fprintf (stderr, "Failed to semantically analyze snapshot at '%s': %s\n", snapshot_path, error_message);
        hc_snapshot_destroy (snapshot);
        return 1;
    }
    if (generate_c (snapshot, generated_c_path, error_message, sizeof (error_message))) {
        fprintf (stderr, "Failed to compile snapshot at '%s': %s\n", snapshot_path, error_message);
        hc_snapshot_destroy (snapshot);
        return 1;
    }
    hc_snapshot_destroy (snapshot);
    return 0;
}

static void print_syn_section (FILE *out, const char *name, hc_syn_rpn_t *rpn, const char *kind_str)
{
    int syn_str_len = 0;
    char *syn_str = make_syn_listing (hc_syn_rpn_get_instructions (rpn), hc_syn_rpn_get_instruction_count (rpn), &syn_str_len);

    fprintf (out, "Module Name: %s\nModule Kind: %s\nINSN count: %d\n", name, kind_str, hc_syn_rpn_get_instruction_count (rpn));
    fprintf (out, "%s\n\n", syn_str);
    free(syn_str);
}

static void print_syn_module (FILE *out, const char *name, hc_module_syn_t *module_syn)
{
    if (module_syn->_class) {
        print_syn_section (out, name, module_syn->_class, "class");
    }
    if (module_syn->function_return_type) {
        print_syn_section (out, name, module_syn->function_return_type, "function_return_type");
    }
    if (module_syn->function_parameters) {
        print_syn_section (out, name, module_syn->function_parameters, "function_parameters");
    }
    if (module_syn->function_body) {
        print_syn_section (out, name, module_syn->function_body, "function_body");
    }
}

static int print_syn_children (const char *name, hc_module_t *module, void *user_data)
{
    if (!(name && module))
        return -1;
    print_syn_module ((FILE *)user_data, name, hc_module_syn (module));

    return hc_module_map_foreach (hc_module_children (module), print_syn_children, user_data);
}

static int target_dump_syn_snapshot (const char *snapshot_path, const char *parsed_path)
{
    char error_message[512];
    hc_snapshot_t *snapshot = hc_snapshot_load (snapshot_path, error_message, sizeof (error_message));
    if (!snapshot) {
        fprintf (stderr, "Failed to load snapshot at '%s': %s\n", snapshot_path, error_message);
        return 1;
    }
    if (hc_snapshot_parse (snapshot)) {
        fprintf (stderr, "Failed to parse snapshot at '%s': %s\n", snapshot_path, error_message);
        hc_snapshot_destroy (snapshot);
        return 1;
    }

    FILE *fh = fopen (parsed_path, "w");
    if (fh == NULL) {
        fprintf(stderr, "Failed to open syndump file at %s\n", parsed_path);
        perror("Reason");
        return 1;
    }

    hc_module_t *root_module = hc_snapshot_root (snapshot);
    return hc_module_map_foreach (hc_module_children (root_module), print_syn_children, fh);
}

int main (int argc, char **argv)
{
    if (argc == 1) {
        fprintf (stderr, "TODO: Usage\n");
        return 1;
    }

    const char *target = argv[1];

    if (!strcmp (target, "--compile-snapshot")) {
        if (argc != 4) {
            fprintf (stderr, "--compile-snapshot requires 2 arguments\n");
            return 1;
        }
        const char *snapshot_path = argv[2];
        const char *generated_c_path = argv[3];
        return target_compile_snapshot (snapshot_path, generated_c_path);
    }

    if (!strcmp (target, "--dump-syn")) {
        if (argc != 4) {
            fprintf (stderr, "--dump-syn requires 2 arguments\n");
            return 1;
        }
        const char *snapshot_path = argv[2];
        const char *generated_c_path = argv[3];
        return target_dump_syn_snapshot (snapshot_path, generated_c_path);
    }

    fprintf (stderr, "FATAL ERROR: Invalid invocation!!\n");
    return 1;
}
