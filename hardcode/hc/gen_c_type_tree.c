#include "gen_c_type_tree.h"

#include "hc_syn.h"
#include "hc_syn_rpn.h"
#include "hc_sem.h"
#include "hc_sem_rpn.h"
#include "hc_snapshot.h"
#include "hc_module.h"
#include "hc_module_map.h"
#include "hc_backend_c.h"
#include "syn_listing.h"
#include "sem_listing.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


typedef struct callback_state_t {
    const char *path;
    FILE *fh;
    int depth;
} callback_state_t;

static int handle_self_and_children (hc_module_t *module, const char *path, const char *name, int depth, FILE *fh);

int gen_c_type_tree (hc_snapshot_t *snapshot, FILE *fh)
{
    fprintf (fh, "#pragma once\n");
    fprintf (fh, "\n");
    fprintf (fh, "#include <stdint.h>\n");
    fprintf (fh, "\n");
    fprintf (fh, "typedef ");
    int ret = handle_self_and_children (hc_snapshot_root (snapshot), "", "hc_type_t", 0, fh);
    return ret;
}

static int callback (const char *name, hc_module_t *module, callback_state_t *state)
{
    char path[512];
    if (*state->path)
	snprintf (path, sizeof (path), "%s.%s", state->path, name);
    else
	snprintf (path, sizeof (path), "%s", name);
    return handle_self_and_children (module, path, name, state->depth, state->fh) ? 0 : -1;
}
static int handle_self_and_children (hc_module_t *module, const char *path, const char *name, int depth, FILE *fh)
{
    hc_module_source_t *module_source = hc_module_source (module);
    hc_module_map_t *module_children = hc_module_children (module);

    int identation = depth*4;
    int i, j;
    for (j = 0; j < identation; ++j)
        fputc (' ', fh);
    fputs ("struct {\n", fh);
    if (module_source->_class_baked.data) { // TODO: Generate from parsed 'class'
        const char *s = module_source->_class_baked.data;
        int len = module_source->_class_baked.len;
        int identation = (depth + 1)*4;
        for (j = 0; j < identation; ++j)
            fputc (' ', fh);
        fputs ("struct {\n", fh);
        identation = (depth + 2)*4;
        for (j = 0; j < identation; ++j)
            fputc (' ', fh);
        for (i = 0; i < len; ++i) {
            char c = s[i];
            fputc (c, fh);
            if (c == '\n') {
                int identation = (depth + 2)*4;
                for (j = 0; j < identation; ++j)
                    fputc (' ', fh);
            }
        }
        fputc ('\n', fh);
        identation = (depth + 1)*4;
        for (i = 0; i < identation; ++i)
            fputc (' ', fh);
        fputs ("} _;\n", fh);
    } else if (module_source->_array_baked.data) { // TODO: Generate from parsed 'array'
        const char *s = module_source->_array_baked.data;
        int len = module_source->_array_baked.len;
        int identation = (depth + 1)*4;
        for (j = 0; j < identation; ++j)
            fputc (' ', fh);
        for (i = 0; i < len; ++i) {
            char c = s[i];
            if (c == '[') {
                fputc (' ', fh);
                fputc ('_', fh);
            }
            fputc (c, fh);
        }
        fputs ("\n", fh);
    }
    callback_state_t state = {
	.path = path,
	.fh = fh,
        .depth = depth + 1,
    };
    if (hc_module_map_foreach (module_children, (void *) callback, &state))
        return 0;
    for (i = 0; i < identation; ++i)
        fputc (' ', fh);
    fprintf (fh, "} %s;\n", name);

    return 1;
}
