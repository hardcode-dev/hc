#pragma once

#include "hc_syn.h"

typedef struct hc_syn_rpn_t hc_syn_rpn_t;

extern hc_syn_rpn_t *hc_syn_rpn_create();

extern void hc_syn_rpn_destroy(hc_syn_rpn_t *s);

extern hc_syn_insn_t *hc_syn_rpn_get_instructions(hc_syn_rpn_t *s);

extern int hc_syn_rpn_get_instruction_count(const hc_syn_rpn_t *s);

extern int hc_syn_rpn_move_instruction(hc_syn_rpn_t *s, int type, int token_off, int token_size, const hc_syn_insn_value_t *value);
