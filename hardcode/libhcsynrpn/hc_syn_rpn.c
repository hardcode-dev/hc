#include "hc_syn_rpn.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct hc_syn_rpn_t {
    hc_syn_insn_t *instructions;
    int count;
    int reserved;
} hc_syn_rpn_t;

hc_syn_rpn_t *hc_syn_rpn_create() {
    hc_syn_rpn_t *s = malloc(sizeof(hc_syn_rpn_t));
    if (!s)
        return NULL;
    s->instructions = malloc(sizeof(hc_syn_insn_t) * (s->reserved = 16));
    s->count = 0;
    return s;
}

void hc_syn_rpn_destroy(hc_syn_rpn_t *s) {
    if (!s)
        return;
    hc_syn_insn_t *insn = s->instructions;
    hc_syn_insn_t *einsn = insn + s->count;
    for (; insn < einsn; ++insn)
        if (HC_SYN_INSN_VALUE_TYPE(insn->type) == HC_SYN_INSN_TYPE_STRING)
            free(insn->value._string.data);
    free(s->instructions);
    free(s);
}

hc_syn_insn_t *hc_syn_rpn_get_instructions(hc_syn_rpn_t *s) {
    return s->instructions;
}

int hc_syn_rpn_get_instruction_count(const hc_syn_rpn_t *s) { return s->count; }

int hc_syn_rpn_move_instruction(hc_syn_rpn_t *s, int type, int token_off, int token_size, const hc_syn_insn_value_t *value) {
    if (s->count == s->reserved) {
        if (!(s->instructions = realloc(s->instructions, sizeof(hc_syn_insn_t) * (s->reserved <<= 1))))
            return 0;
    }
    hc_syn_insn_t *insn = s->instructions + s->count++;
    insn->type = type;
    insn->source_off = token_off;
    insn->source_size = token_size;
    if (HC_SYN_INSN_VALUE_TYPE(type) != HC_SYN_INSN_TYPE_VOID)
        insn->value = *value;
    return 1;
}
