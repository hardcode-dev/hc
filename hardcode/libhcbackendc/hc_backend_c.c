#include "hc_backend_c.h"

#include "hc_sem.h"
#include "hc_backend_c_sem_value_stack.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <assert.h>


static int handle_instruction (sem_value_t **value_stack, const hc_sem_insn_t *insn, char *error_message, size_t max_error_message_len)
{
    switch (insn->type) {
    case HC_SEM_INSN_VARIABLE: {
        SEM_VALUE_STACK_PUSH_UNARY (
            value_stack,
            "(", insn->value._string.data, insn->value._string.len, ")"
        );
    } break;
    case HC_SEM_INSN_MULTIPLICATION: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for multiplication");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "(", a->value, strlen (a->value), " * ", b->value, strlen (b->value), ")"
	);

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_DIVISION: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for division");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "(", a->value, strlen (a->value), " / ", b->value, strlen (b->value), ")"
	);

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_ADDITION: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for addition");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "(", a->value, strlen (a->value), " + ", b->value, strlen (b->value), ")"
	);

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_SUBTRACTION: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for subtraction");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "(", a->value, strlen (a->value), " - ", b->value, strlen (b->value), ")"
	    );

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_ARRAY_ELEMENT_ACCESS: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for array element access");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "(", a->value, strlen (a->value), "[", b->value, strlen (b->value), "])"
	);

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_EMPTY_EXPRESSION: {
        SEM_VALUE_STACK_PUSH_UNARY_LITERAL (
            value_stack,
            "(", "(void) 0", ")"
        );
    } break;
    case HC_SEM_INSN_LITERAL_INTEGER: {
        char str_value[32];
        size_t str_len = snprintf (str_value, sizeof (str_value), "(%"PRId64")", insn->value._long);
        SEM_VALUE_STACK_PUSH_UNARY (
            value_stack,
            "(", str_value, str_len, ")"
        );
    } break;
    case HC_SEM_INSN_LITERAL_FLOAT: {
        char str_value[512];
        size_t str_len = snprintf (str_value, sizeof (str_value), "(%a /* %g */)", insn->value._float, insn->value._float);
        SEM_VALUE_STACK_PUSH_UNARY (
            value_stack,
            "(", str_value, str_len, ")"
        );
    } break;
    case HC_SEM_INSN_LITERAL_DOUBLE: {
        char str_value[512];
        size_t str_len = snprintf (str_value, sizeof (str_value), "(%a /* %g */)", insn->value._double, insn->value._double);
        SEM_VALUE_STACK_PUSH_UNARY (
            value_stack,
            "(", str_value, str_len, ")"
        );
    } break;
    case HC_SEM_INSN_INITIALIZE: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for initialization statement");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "", a->value, strlen (a->value), " = ", b->value, strlen (b->value), ""
	);

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_MUTATE: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for mutation statement");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "", a->value, strlen (a->value), " = ", b->value, strlen (b->value), ""
	);

        free (a);
        free (b);
    } break;
    case HC_SEM_INSN_STAPLE_BLOCK_ITEMS: {
        sem_value_t *b = sem_value_stack_pop (value_stack);
        sem_value_t *a = sem_value_stack_pop (value_stack);
        if (!a || !b) {
            snprintf (error_message, max_error_message_len, "Invalid semantic tree: missing arguments for stapling block items");
            free (a);
            free (b);
            return -1;
        }

        SEM_VALUE_STACK_PUSH_BINARY (
            value_stack,
            "", a->value, strlen (a->value), ";\n",
            b->value, strlen (b->value), ""
	);

        free (a);
        free (b);
    } break;
    default: {
        snprintf (error_message, max_error_message_len, "Unexpected instruction %d at C generation", (int) insn->type);
        return -1;
    }
    }

    return 0;
}


char *hc_backend_c_generate (const hc_sem_insn_t *instructions, int instruction_count, int *generated_len, char *error_message, size_t max_error_message_len)
{
#define STR_AND_LEN(s) s, (sizeof (s) - sizeof (""))
    sem_value_t *value_stack = NULL;

    int i;
    for (i = 0; i < instruction_count; ++i) {
	const hc_sem_insn_t *insn = instructions + i;
        if (handle_instruction (&value_stack, insn, error_message, max_error_message_len)) {
            sem_value_stack_clear (&value_stack);
            return NULL;
        }
    }

    {
	sem_value_t *a = sem_value_stack_pop (&value_stack);
        if (!a) {
            snprintf (error_message, max_error_message_len, "Empty value stack at the end");
            sem_value_stack_clear (&value_stack);
            return NULL;
        }
        if (value_stack) {
            snprintf (error_message, max_error_message_len, "More than 1 element on value stack at the end");
            free (a);
            sem_value_stack_clear (&value_stack);
            return NULL;
        }

	SEM_VALUE_STACK_PUSH_UNARY (
	    &value_stack,
	    "return ({\n",
            a->value, strlen (a->value), ";\n"
            "});"
	);

	free (a);
    }

    char *ret = strdup (value_stack->value);
    sem_value_stack_clear (&value_stack);

    int len = strlen (ret);
    *generated_len = len;

    return ret;
}
