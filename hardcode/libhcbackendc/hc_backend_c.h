#pragma once

#include <stddef.h>

typedef struct hc_sem_insn_t hc_sem_insn_t;


extern char *hc_backend_c_generate (
    const hc_sem_insn_t *instructions,
    int instruction_count,
    int *generated_len,
    char *error_message,
    size_t max_error_message_len);
