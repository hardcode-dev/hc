#pragma once

#include <stddef.h>


typedef struct sem_value_t sem_value_t;

typedef struct sem_value_t {
    sem_value_t *prev;
    char value[1]; /* Variable size */
} sem_value_t;


extern void sem_value_stack_clear (
    sem_value_t **stack_p);

extern void sem_value_stack_push (
    sem_value_t **stack,
    sem_value_t *value);

extern sem_value_t *sem_value_stack_pop (
    sem_value_t **stack);

extern sem_value_t *sem_value_stack_node_alloc (
    size_t value_len);

extern sem_value_t *sem_value_stack_push_unary(
    sem_value_t **stack,
    const char *prefix,
    size_t prefix_len,
    const char *a,
    size_t a_len,
    const char *suffix,
    size_t suffix_len);

extern sem_value_t *sem_value_stack_push_binary(
    sem_value_t **stack,
    const char *prefix,
    size_t prefix_len,
    const char *a,
    size_t a_len,
    const char *middle,
    size_t middle_len,
    const char *b,
    size_t b_len,
    const char *suffix,
    size_t suffix_len);

#define SEM_VALUE_STACK_PUSH_UNARY(stack,prefix,a,a_len,suffix)	\
    sem_value_stack_push_unary (				\
	(stack),						\
	("" prefix), (sizeof ("" prefix) - sizeof ("")),	\
	(a), (a_len),						\
	("" suffix), (sizeof ("" suffix) - sizeof (""))		\
    )
#define SEM_VALUE_STACK_PUSH_UNARY_LITERAL(stack,prefix,a,suffix)	\
    sem_value_stack_push_unary (					\
	(stack),							\
	("" prefix), (sizeof ("" prefix) - sizeof ("")),		\
	("" a), (sizeof ("" a) - sizeof ("")),				\
	("" suffix), (sizeof ("" suffix) - sizeof (""))			\
    )

#define SEM_VALUE_STACK_PUSH_BINARY(stack,prefix,a,a_len,middle,b,b_len,suffix)	\
    sem_value_stack_push_binary (					\
	(stack),							\
	("" prefix), (sizeof ("" prefix) - sizeof ("")),		\
	(a), (a_len),							\
	("" middle), (sizeof ("" middle) - sizeof ("")),		\
	(b), (b_len),							\
	("" suffix), (sizeof ("" suffix) - sizeof (""))			\
    )
