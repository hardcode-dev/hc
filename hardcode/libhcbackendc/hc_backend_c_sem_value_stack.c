#include "hc_backend_c_sem_value_stack.h"

#include <stdlib.h>
#include <string.h>


/* Stack is actually pointer to node tail */
void sem_value_stack_clear (sem_value_t **stack_p)
{
    sem_value_t *stack = *stack_p;
    while (stack) {
	sem_value_t *prev = stack->prev;
	free (stack);
	stack = prev;
    }
    *stack_p = NULL;
}
void sem_value_stack_push (sem_value_t **stack, sem_value_t *value)
{
    value->prev = *stack;
    *stack = value;
}
sem_value_t *sem_value_stack_pop (sem_value_t **stack)
{
    sem_value_t *value = *stack;
    if (value)
	*stack = value->prev;
    return value;
}
sem_value_t *sem_value_stack_node_alloc (size_t value_len)
{
    return malloc (sizeof (sem_value_t) + value_len);
}
sem_value_t *sem_value_stack_push_unary (sem_value_t **stack,
                                         const char *prefix, size_t prefix_len,
                                         const char *a, size_t a_len,
                                         const char *suffix, size_t suffix_len)
{
    size_t str_len = prefix_len + a_len + suffix_len;
    sem_value_t *new_value = sem_value_stack_node_alloc (str_len);
    char *ptr = new_value->value;
    memcpy (ptr, prefix, prefix_len);
    ptr += prefix_len;
    memcpy (ptr, a, a_len);
    ptr += a_len;
    memcpy (ptr, suffix, suffix_len);
    ptr += suffix_len;
    *ptr = '\0';
    sem_value_stack_push (stack, new_value);
    return new_value;
}
sem_value_t *sem_value_stack_push_binary (sem_value_t **stack,
                                          const char *prefix, size_t prefix_len,
                                          const char *a, size_t a_len,
                                          const char *middle, size_t middle_len,
                                          const char *b, size_t b_len,
                                          const char *suffix, size_t suffix_len)
{
    size_t str_len = prefix_len + a_len + middle_len + b_len + suffix_len;
    sem_value_t *new_value = sem_value_stack_node_alloc (str_len);
    char *ptr = new_value->value;
    memcpy (ptr, prefix, prefix_len);
    ptr += prefix_len;
    memcpy (ptr, a, a_len);
    ptr += a_len;
    memcpy (ptr, middle, middle_len);
    ptr += middle_len;
    memcpy (ptr, b, b_len);
    ptr += b_len;
    memcpy (ptr, suffix, suffix_len);
    ptr += suffix_len;
    *ptr = '\0';
    sem_value_stack_push (stack, new_value);
    return new_value;
}
