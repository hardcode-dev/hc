#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


static inline void *check_malloc (size_t size)
{
    void *ptr = malloc (size);
    if (!ptr) {
	fprintf (stderr, "Failed to allocate %llu bytes\n", (long long unsigned int) size);
	exit (1);
    }
    return ptr;
}
static inline void *check_realloc (void *ptr, size_t size)
{
    void *new_ptr = realloc (ptr, size);
    if (!new_ptr) {
	fprintf (stderr, "Failed to allocate %llu bytes\n", (long long unsigned int) size);
	exit (1);
    }
    return new_ptr;
}
static inline double fclamp (double value, double min, double max)
{
    if (value > max)
	value = max;
    if (value < min)
	value = min;
    return value;
}
static inline double square_distance (double x1, double y1, double x2, double y2)
{
    double dx = x1 - x2;
    double dy = y1 - y2;
    return dx*dx + dy*dy;
}
static inline double distance (double x1, double y1, double x2, double y2)
{
    return sqrt (square_distance (x1, y1, x2, y2));
}
