#pragma once

#include <GL/gl.h>


extern GLuint gpu_load_program (
    const GLchar *vertex_shader_source,
    GLint vertex_shader_source_len,
    const GLchar *fragment_shader_source,
    GLint fragment_shader_source_len);
