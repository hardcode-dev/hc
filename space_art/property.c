#include "property.h"

#include <string.h>


typedef struct property_callback_t {
    property_callback_function_t callback_function;
    void *user_data;
    struct property_callback_t *prev;
    struct property_callback_t *next;
} property_callback_t;

typedef struct property_t {
    uint32_t size;
    uint32_t type;
    property_callback_t callback_sentinel;
} property_t;


static inline void init_callback_sentinel (property_callback_t *callback_sentinel)
{
    callback_sentinel->prev = callback_sentinel;
    callback_sentinel->next = callback_sentinel;
}


property_t *property_create (uint32_t size, uint32_t type)
{
    property_t *s = check_malloc (sizeof (property_t) + size);
    s->size = size;
    s->type = type;
    init_callback_sentinel (&s->callback_sentinel);
    return s;
}
void property_destroy (property_t *s)
{
    if (!s) return;
    property_clear_callbacks (s);
    free (s);
}
// Copy memory area and call all callbacks in order of addition
void property_set_value (property_t *s, const void *new_value)
{
    memcpy (s + 1, new_value, s->size);
}
// Just return memory address
const void *property_get_value (property_t *s)
{
    return s + 1;
}
uint32_t property_get_type (property_t *s)
{
    return s->type;
}
uint32_t property_get_size (property_t *s)
{
    return s->size;
}
property_callback_t *property_add_callback (property_t *s, void *user_data, property_callback_function_t callback_function)
{
    property_callback_t *callback = malloc (sizeof (property_callback_t));
    callback->callback_function = callback_function;
    callback->user_data = user_data;
    callback->prev = s->callback_sentinel.prev;
    callback->next = &s->callback_sentinel;
    s->callback_sentinel.prev->next = callback;
    s->callback_sentinel.prev = callback;
    return callback;
}
void property_drop_callback (property_t *s, property_callback_t *callback)
{
    (void) s;

    callback->prev->next = callback->next;
    callback->next->prev = callback->prev;
    free (callback);
}
void property_clear_callbacks (property_t *s)
{
    property_callback_t *callback = s->callback_sentinel.next;
    while (callback != &s->callback_sentinel) {
	property_callback_t *next = callback->next;
	free (callback);
	callback = next;
    }
}
