#pragma once

typedef struct object_t object_t;
typedef struct string_t string_t;
typedef struct property_t property_t;


extern object_t *object_create ();

extern void object_destroy (
    object_t *s);

extern property_t *object_get_property (
    object_t *s,
    const string_t *key);

extern void object_insert_property (
    object_t *s,
    const string_t *key,
    property_t *property);

extern void object_drop_property (
    object_t *s,
    const string_t *key);
