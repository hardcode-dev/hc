#include "world.h"

#include "common.h"
#include "unit.h"
#include "unit_seal.h"
#include "sprite.h"
#include "image.h"
#include "hc_module_tree.h"
#include "gpu_program.h"
#include "gpu_programs/sprite/main.vert.h"
#include "gpu_programs/sprite/main.frag.h"
#include "gpu_programs/image/main.vert.h"
#include "gpu_programs/image/main.frag.h"
#include "gpu_programs/vertices/main.vert.h"
#include "gpu_programs/vertices/main.frag.h"

#include <GLFW/glfw3.h>
#include <string.h>


typedef struct unit_seal_t unit_seal_t;

static const double min_rectangle_selection_distance = 10.0;
static const double min_rectangle_selection_square_distance = 100.0;


static int load_programs (GLuint *sprite_program, GLuint *image_program, GLuint *vertices_program)
{
    if ((*sprite_program = gpu_load_program ((const GLchar *) gpu_programs_sprite_main_vert, sizeof (gpu_programs_sprite_main_vert),
					     (const GLchar *) gpu_programs_sprite_main_frag, sizeof (gpu_programs_sprite_main_frag)))) {
	if ((*image_program = gpu_load_program ((const GLchar *) gpu_programs_image_main_vert, sizeof (gpu_programs_image_main_vert),
						(const GLchar *) gpu_programs_image_main_frag, sizeof (gpu_programs_image_main_frag)))) {
	    if ((*vertices_program = gpu_load_program ((const GLchar *) gpu_programs_vertices_main_vert, sizeof (gpu_programs_vertices_main_vert),
							(const GLchar *) gpu_programs_vertices_main_frag, sizeof (gpu_programs_vertices_main_frag)))) {
		return 1;
	    }
	    glDeleteProgram (*image_program);
	}
	glDeleteProgram (*sprite_program);
    }
    return 0;
}


world_t *world_create (uint32_t width, uint32_t height)
{
    GLuint sprite_program, image_program, vertices_program;

    if (load_programs (&sprite_program, &image_program, &vertices_program)) {
	world_t *s = check_malloc (sizeof (world_t));
	s->internal_string_set = internal_string_set_create ();
	{
	    s->internal_strings.position = internal_string_set_add_string (s->internal_string_set, "position", sizeof ("position") - sizeof (""));
	    s->internal_strings.destination = internal_string_set_add_string (s->internal_string_set, "destination", sizeof ("destination") - sizeof (""));
	}
	s->area_size[0] = width;
	s->area_size[1] = height;
	s->sprite_program = sprite_program;
	s->image_program = image_program;
	s->vertices_program = vertices_program;
	s->background_image = image_create (image_program, "data/background.png");
	s->image = image_create (image_program, "data/test_image.png");
	s->cursor_image = image_create (image_program, "data/cursor.png");
	s->vertices = HC_CALL (render.vertices) (vertices_program);
	s->unit_sprite_set = unit_sprite_set_create (sprite_program);
	s->cursor_pos[0] = width*0.5;
	s->cursor_pos[1] = height*0.5;
	s->shift_pressed = 0;
	s->selecting = 0;
	s->selecting_start_pos[0] = 0.0;
	s->selecting_start_pos[1] = 0.0;
	unit_array_init (&s->unit_array);
	size_t i;
	for (i = 0; i < 20; ++i) {
	    unit_t *seal = unit_array_open (&s->unit_array);
	    HC_TYPE (math.dvec2) initial_pos = {
		10 + i*100,
		700,
	    };
	    unit_init (s, seal, seal_vtable, initial_pos, 0.8);
	}
	return s;
    }
    return NULL;
}
void world_destroy (world_t *s)
{
    if (!s) return;
    internal_string_set_destroy (s->internal_string_set);
    free (s);
}
void world_update (world_t *s, double dtime)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit)
	unit_update (unit, dtime);
}
void world_draw_map (world_t *s)
{
    world_draw_image (s, s->background_image, 0, 0);
}
void world_draw_units (world_t *s)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit)
	unit_draw (unit);
}
void world_draw_selection (world_t *s)
{
    if (s->selecting) {
	double box[] = {
	    fmin (s->cursor_pos[0], s->selecting_start_pos[0]),
	    fmin (s->cursor_pos[1], s->selecting_start_pos[1]),
	    fmax (s->cursor_pos[0], s->selecting_start_pos[0]),
	    fmax (s->cursor_pos[1], s->selecting_start_pos[1]),
	};
	world_draw_line_box (s, box, 0x00ff00ff);
    }
}
void world_draw_cursor (world_t *s)
{
    world_draw_image (s, s->cursor_image, s->cursor_pos[0] - 32, s->cursor_pos[1] - 32);
}
void world_draw (world_t *s)
{
    world_draw_map (s);
    world_draw_units (s);
    world_draw_selection (s);
    world_draw_cursor (s);
}
void world_draw_sprite (world_t *s, sprite_t *sprite, double center_x, double center_y, double rotation)
{
    HC_TYPE (math.dmat4) m, o;
    HC_CALL (math.dmat4.rotate) (m, m, 0, 0, -1, rotation);
    HC_CALL (math.dmat4.translation) (m, center_x, center_y, 0);
    HC_CALL (math.dmat4.ortho) (o, 0, s->area_size[0], s->area_size[1], 0, 1, -1);
    HC_TYPE (math.dmat4) m_flat;
    HC_CALL (math.dmat4.mul) (m_flat, o, m);

    float m_flat_[16];
    size_t i;
    for (i = 0; i < 16; ++i)
	m_flat_[i] = m_flat[i];


    glEnableVertexAttribArray (sprite->vertex_pos);
    glEnableVertexAttribArray (sprite->vertex_tex_coord);

    glUseProgram (sprite->program);
    glBindTexture (GL_TEXTURE_2D, sprite->tex);
    glBindBuffer (GL_ARRAY_BUFFER, sprite->vertex_buffer);
    glVertexAttribPointer (sprite->vertex_pos, 2, GL_FLOAT, GL_FALSE, sizeof (float[4]), (void *) 0);
    glVertexAttribPointer (sprite->vertex_tex_coord, 2, GL_FLOAT, GL_FALSE, sizeof (float[4]), (void *) (sizeof (float[2])));
    glUniformMatrix4fv (sprite->uniform_transformation, 1, GL_FALSE, (const GLfloat *) m_flat_);
    glUniform1i (sprite->uniform_color_texture, 0);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA,  GL_ONE_MINUS_SRC_ALPHA);

    glDrawArrays (GL_TRIANGLES, 0, 6);
}
void world_draw_image (world_t *s, image_t *image, int x, int y)
{
    HC_TYPE (math.dmat4) m, o;
    HC_CALL (math.dmat4.translation) (m, x, y, 0);
    HC_CALL (math.dmat4.ortho) (o, 0, s->area_size[0], s->area_size[1], 0, 1, -1);
    HC_TYPE (math.dmat4) m_flat;
    HC_CALL (math.dmat4.mul) (m_flat, o, m);

    float m_flat_[16];
    size_t i;
    for (i = 0; i < 16; ++i)
	m_flat_[i] = m_flat[i];

    glEnableVertexAttribArray (image->vertex_pos);

    glUseProgram (image->program);
    glBindTexture (GL_TEXTURE_RECTANGLE, image->tex);
    glBindBuffer (GL_ARRAY_BUFFER, image->vertex_buffer);
    glVertexAttribPointer (image->vertex_pos, 2, GL_FLOAT, GL_FALSE, sizeof (float[2]), (void *) 0);
    glUniformMatrix4fv (image->uniform_transformation, 1, GL_FALSE, (const GLfloat *) m_flat_);
    glUniform1i (image->uniform_color_texture, 0);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA,  GL_ONE_MINUS_SRC_ALPHA);

    glBindBuffer (GL_ARRAY_BUFFER, image->vertex_buffer);
    glBindTexture (GL_TEXTURE_RECTANGLE, image->tex);
    glDrawArrays (GL_TRIANGLES, 0, 6);
}
void world_draw_vertices (world_t *s, HC_TYPE (render.vertices) *vertices, GLenum mode, uint32_t color)
{
    HC_TYPE (math.dmat4) m, o;
    HC_CALL (math.dmat4.translation) (m, 0, 0, 0);
    HC_CALL (math.dmat4.ortho) (o, 0, s->area_size[0], s->area_size[1], 0, 1, -1);
    HC_TYPE (math.dmat4) m_flat;
    HC_CALL (math.dmat4.mul) (m_flat, o, m);

    float m_flat_[16];
    size_t i;
    for (i = 0; i < 16; ++i)
	m_flat_[i] = m_flat[i];

    glEnableVertexAttribArray (vertices->vertex_pos);

    glUseProgram (vertices->program);
    glVertexAttribPointer (vertices->vertex_pos, 2, GL_FLOAT, GL_FALSE, sizeof (float[2]), (void *) 0);
    glUniformMatrix4fv (vertices->uniform_transformation, 1, GL_FALSE, (const GLfloat *) m_flat_);
    glUniform4f (vertices->uniform_color,
		 (color >> 24)/255.0,
		 ((color >> 16) & 0xff)/255.0,
		 ((color >> 8) & 0xff)/255.0,
		 (color & 0xff)/255.0);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA,  GL_ONE_MINUS_SRC_ALPHA);

    HC_CALL (render.vertices.draw) (vertices, mode);
}
void world_draw_rectangle (world_t *s, double x, double y, double w, double h, uint32_t color)
{
    double bounding_box[4] = {
    	x, y, x + w, y + h,
    };
    HC_CALL (render.vertices.load_rectangle) (s->vertices, bounding_box);
    world_draw_vertices (s, s->vertices, GL_TRIANGLES, color);
}
void world_draw_circle (world_t *s, double x, double y, double radius, uint32_t color)
{
    HC_CALL (render.vertices.load_circle) (s->vertices, x, y, radius);
    world_draw_vertices (s, s->vertices, GL_TRIANGLES, color);
}
void world_draw_ring (world_t *s, double x, double y, double radius, uint32_t color)
{
    HC_CALL (render.vertices.load_ring) (s->vertices, x, y, radius);
    world_draw_vertices (s, s->vertices, GL_LINE_LOOP, color);
}
void world_draw_line (world_t *s, double src_x, double src_y, double dest_x, double dest_y, uint32_t color)
{
    HC_CALL (render.vertices.load_line) (s->vertices, src_x, src_y, dest_x, dest_y);
    world_draw_vertices (s, s->vertices, GL_LINES, color);
}
void world_draw_line_strip (world_t *s, size_t point_count, const double *points, uint32_t color)
{
    HC_CALL (render.vertices.load_line_strip) (s->vertices, point_count, points);
    world_draw_vertices (s, s->vertices, GL_LINE_STRIP, color);
}
void world_draw_line_box (world_t *s, const double *bounding_box, uint32_t color)
{
    HC_CALL (render.vertices.load_line_box) (s->vertices, bounding_box);
    world_draw_vertices (s, s->vertices, GL_LINE_LOOP, color);
}
void world_select_unit (world_t *s, unit_t *selected_unit)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit)
	unit_set_selected (unit, unit == selected_unit);
}
void world_try_select_unit (world_t *s)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit) {
	HC_TYPE (math.circled) selection_circle;
	unit_get_selection_circle (unit, &selection_circle);
	HC_TYPE (math.dvec2) cursor_pos = {
	    s->cursor_pos[0],
	    s->cursor_pos[1],
	};
	if (HC_CALL (intersections.check_point_circle) (cursor_pos, &selection_circle)) {
	    world_select_unit (s, unit);
	    break;
	}
    }
}
void world_toggle_unit_selection (world_t *s)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit) {
	HC_TYPE (math.circled) selection_circle;
	unit_get_selection_circle (unit, &selection_circle);
	HC_TYPE (math.dvec2) cursor_pos = {
	    s->cursor_pos[0],
	    s->cursor_pos[1],
	};
	if (HC_CALL (intersections.check_point_circle) (cursor_pos, &selection_circle)) {
	    unit_set_selected (unit, !unit_is_selected (unit));
	    break;
	}
    }
}
void world_rectangle_select_units (world_t *s)
{
    HC_TYPE (math.boxd) box = {
	.left = fmin (s->cursor_pos[0], s->selecting_start_pos[0]),
	.top = fmin (s->cursor_pos[1], s->selecting_start_pos[1]),
	.right = fmax (s->cursor_pos[0], s->selecting_start_pos[0]),
	.bottom = fmax (s->cursor_pos[1], s->selecting_start_pos[1]),
    };
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit) {
	HC_TYPE (math.circled) selection_circle;
	unit_get_selection_circle (unit, &selection_circle);
	unit_set_selected (unit, HC_CALL (intersections.check_box_circle) (&box, &selection_circle));
    }
}
void world_add_rectangle_select_units (world_t *s)
{
    HC_TYPE (math.boxd) box = {
	.left = fmin (s->cursor_pos[0], s->selecting_start_pos[0]),
	.top = fmin (s->cursor_pos[1], s->selecting_start_pos[1]),
	.right = fmax (s->cursor_pos[0], s->selecting_start_pos[0]),
	.bottom = fmax (s->cursor_pos[1], s->selecting_start_pos[1]),
    };
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit) {
	HC_TYPE (math.circled) selection_circle;
	unit_get_selection_circle (unit, &selection_circle);
	if (HC_CALL (intersections.check_box_circle) (&box, &selection_circle))
	    unit_set_selected (unit, 1);
    }
}
void world_try_rectangle_select_units (world_t *s)
{
    HC_TYPE (math.boxd) box = {
	.left = fmin (s->cursor_pos[0], s->selecting_start_pos[0]),
	.top = fmin (s->cursor_pos[1], s->selecting_start_pos[1]),
	.right = fmax (s->cursor_pos[0], s->selecting_start_pos[0]),
	.bottom = fmax (s->cursor_pos[1], s->selecting_start_pos[1]),
    };
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit) {
	HC_TYPE (math.circled) selection_circle;
	unit_get_selection_circle (unit, &selection_circle);
	if (HC_CALL (intersections.check_box_circle) (&box, &selection_circle)) {
	    world_rectangle_select_units (s);
	    break;
	}
    }
}
void world_selected_units_move_to (world_t *s, double dest_x, double dest_y)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit)
	if (unit_is_selected (unit))
	    unit_move_to (unit, dest_x, dest_y);
}
void world_selected_units_stop (world_t *s)
{
    unit_t *unit = s->unit_array.data;
    unit_t *eunit = s->unit_array.data + s->unit_array.count;
    for (; unit < eunit; ++unit)
	if (unit_is_selected (unit))
	    unit_stop (unit);
}
void world_input_event_key (world_t *s, int key, int action)
{
    if (action == GLFW_PRESS) {
	switch (key) {
	case GLFW_KEY_LEFT_SHIFT: {
	    s->shift_pressed = 1;
	} break;
	case GLFW_KEY_S: {
	    world_selected_units_stop (s);
	} break;
	}
    } else if (action == GLFW_RELEASE) {
	switch (key) {
	case GLFW_KEY_LEFT_SHIFT: {
	    s->shift_pressed = 0;
	} break;
	}
    }

}
void world_input_event_mouse_button (world_t *s, int button, int action)
{
    if (action == GLFW_PRESS) {
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT: {
	    s->selecting = 1;
	    s->selecting_start_pos[0] = s->cursor_pos[0];
	    s->selecting_start_pos[1] = s->cursor_pos[1];
	} break;
	}
    } else if (action == GLFW_RELEASE) {
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT: {
	    if (s->selecting) {
		if (square_distance (s->selecting_start_pos[0], s->selecting_start_pos[1],
				     s->cursor_pos[0], s->cursor_pos[1]) < min_rectangle_selection_square_distance) {
		    if (s->shift_pressed)
			world_toggle_unit_selection (s);
		    else
			world_try_select_unit (s);
		} else {
		    if (s->shift_pressed)
			world_add_rectangle_select_units (s);
		    else
			world_try_rectangle_select_units (s);
		}
		s->selecting = 0;
	    }
	} break;
	case GLFW_MOUSE_BUTTON_RIGHT: {
	    world_selected_units_move_to (s, s->cursor_pos[0], s->cursor_pos[1]);
	} break;
	}
    }
}
void world_input_event_mouse_cursor_move_callback (world_t *s, double dx, double dy)
{
    const double sensitivity = 0.5;
    s->cursor_pos[0] = fclamp (s->cursor_pos[0] + dx*sensitivity, 0.0, s->area_size[0] - 1.0);
    s->cursor_pos[1] = fclamp (s->cursor_pos[1] + dy*sensitivity, 0.0, s->area_size[1] - 1.0);
}
