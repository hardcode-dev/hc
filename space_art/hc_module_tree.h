#pragma once

#include <GL/gl.h>

#include ".hc_type.h"
#include "hc/.types.h"
#include "hc/.functions.h"

#include <glib.h>


typedef struct hc_t {
    GHashTable *function_map;
    hc_function_t functions;
} hc_t;


extern hc_t hc;

extern void hc_init ();

extern void hc_module_tree_init ();

extern void hc_bind_function (
    const char *name,
    void *function);

extern void *hc_find_function (
    const char *name);

#define HC_CALL(m) hc.functions.m._
