#include "png_file.h"

#include "common.h"

#include <png.h>
#include <stdio.h>
#include <string.h>


void *png_file_load (const char *fname, uint32_t *width, uint32_t *height)
{
    png_image image;
    memset (&image, 0, (sizeof image));
    image.version = PNG_IMAGE_VERSION;

    if (png_image_begin_read_from_file (&image, fname)) {
	png_bytep buffer;
	image.format = PNG_FORMAT_RGBA;

	if ((buffer = check_malloc (PNG_IMAGE_SIZE (image)))) {
	    if (png_image_finish_read (&image, NULL, buffer, 0, NULL)) {
		*width = image.width;
		*height = image.height;
		return buffer;
	    } else {
		fprintf (stderr, "Failed to read PNG image '%s': %s\n", fname, image.message);
	    }
	    free (buffer);
	} else {
	    fprintf (stderr, "Failed to allocate memory\n");
	}
	png_image_free (&image);
    } else {
	fprintf (stderr, "Failed to read PNG image '%s': %s\n", fname, image.message);
    }

    return NULL;
}
