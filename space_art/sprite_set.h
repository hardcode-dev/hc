#pragma once

#include <GL/gl.h>

#include ".hc_type.h"

typedef struct unit_sprite_set_t {
    HC_TYPE (render.sprite) *crusader;
    HC_TYPE (render.sprite) *seal;
    HC_TYPE (render.sprite) *harvester;
    HC_TYPE (render.sprite) *prophet;
    HC_TYPE (render.sprite) *selection;
} unit_sprite_set_t;


extern unit_sprite_set_t *unit_sprite_set_create (
    uint32_t sprite_program);
