#include "sprite.h"

#include "common.h"
#include "png_file.h"

#include <errno.h>
#include <string.h>


static int is_power_of_2 (uint32_t v)
{
    return v && !(v & (v - 1));
}


sprite_t *sprite_create (uint32_t program, const char *fname)
{
    uint32_t tex_w, tex_h;
    void *pixels = png_file_load (fname, &tex_w, &tex_h);
    if (!pixels)
	return NULL;

    if (!is_power_of_2 (tex_w) || !is_power_of_2 (tex_h)) {
	fprintf (stderr, "Failed to load sprite '%s': power of 2 width and height expected\n", fname);
	free (pixels);
	return NULL;
    }

    sprite_t *s = check_malloc (sizeof (sprite_t));

    s->program = program;

    float vertices[] = {
    	-0.5f*tex_w, -0.5f*tex_h,  0.0f,  0.0f,
	 0.5f*tex_w, -0.5f*tex_h,  1.0f,  0.0f,
	 0.5f*tex_w,  0.5f*tex_h,  1.0f,  1.0f,
    	-0.5f*tex_w, -0.5f*tex_h,  0.0f,  0.0f,
	 0.5f*tex_w,  0.5f*tex_h,  1.0f,  1.0f,
    	-0.5f*tex_w,  0.5f*tex_h,  0.0f,  1.0f,
    };

    glGenBuffers (1, &s->vertex_buffer);
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[4])*6, vertices, GL_STATIC_DRAW);

    s->uniform_transformation = glGetUniformLocation (s->program, "transformation");
    s->uniform_color_texture = glGetUniformLocation (s->program, "color_texture");
    s->vertex_pos = glGetAttribLocation (s->program, "pos");
    s->vertex_tex_coord = glGetAttribLocation (s->program, "tex_coord");

    glGenTextures (1, &s->tex);
    glBindTexture (GL_TEXTURE_2D, s->tex);

    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, tex_w, tex_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    free (pixels);

    return s;
}
