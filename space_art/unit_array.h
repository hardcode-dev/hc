#ifndef UNIT_ARRAY_H
#define UNIT_ARRAY_H

#include <stddef.h>


typedef struct unit_t unit_t;

typedef struct unit_array_t {
    unit_t *data;
    size_t count;
    size_t reserve;
} unit_array_t;


extern void unit_array_init (
    unit_array_t *unit_array);

extern void unit_array_uninit (
    unit_array_t *unit_array);

extern unit_t *unit_array_open (
    unit_array_t *unit_array);

#endif
