#include "unit_seal.h"

#include "unit.h"
#include "world.h"
#include "common.h"
#include "internal_string_set.h"
#include "runtime.h"
#include "hc_module_tree.h"
#include "object.h"
#include "property.h"
#include ".hc_type.h"
#include ".hc_types.h"

#include <string.h>
#include <assert.h>


typedef struct world_t world_t;


#define EPSILON1 0.0000000001

static const double selection_radius = 30.0;
static const double velocity = 300.0;


typedef struct unit_seal_t {
    world_t *world;
    HC_TYPE (render.sprite) *sprite;
    HC_TYPE (render.sprite) *sprite_selection;
    double rotation;
    double max_hp;
    double current_hp;
    int selected;
    int dead;
    object_t *object;
} unit_seal_t;


// void units.seal -> .apply_movement (double dtime) | private method
static void unit_seal_apply_movement (unit_seal_t *s, double dtime)
{
    property_t *pos_property = object_get_property (s->object, s->world->internal_strings.position);
    property_t *dest_property = object_get_property (s->object, s->world->internal_strings.destination);
    if (pos_property && dest_property) {
	const double *pos = property_get_value (pos_property);
	const double *dest = property_get_value (dest_property);
	if (pos[0] != dest[0] || pos[1] != dest[1]) {
	    double distance = velocity*dtime;
	    double dx = dest[0] - pos[0];
	    double dy = dest[1] - pos[1];
	    double full_distance_squared = HC_CALL (math.dvec2.mul_inner) ((double *) dest, (double *) pos);
	    if (distance*distance >= full_distance_squared) {
		property_set_value (pos_property, dest);
	    } else {
		double full_distance = sqrt (full_distance_squared);
                fprintf(stderr, "full_distance = %f\n", full_distance);
		double new_value[2] = {
		    pos[0] + dx*distance/full_distance,
		    pos[1] + dy*distance/full_distance,
		};
		property_set_value (pos_property, new_value);
	    }
	}
    }
}
// void units.seal -> .apply_repulsion (double dtime) | private method
static void unit_seal_apply_repulsion (unit_seal_t *s, double dtime)
{
    property_t *pos_property = object_get_property (s->object, s->world->internal_strings.position);
    assert (pos_property); // TODO: Maintain
    const double *pos = property_get_value (pos_property);
    unit_t *unit = s->world->unit_array.data;
    unit_t *eunit = s->world->unit_array.data + s->world->unit_array.count;
    HC_TYPE (math.dvec2) repulsion = {0.0, 0.0};
    for (; unit < eunit; ++unit) {
	if (unit->data == s)
	    continue;
	HC_TYPE (math.circled) opponent_selection_circle;
	HC_TYPE (math.dvec2) opponent_selection_circle_v = {
	    opponent_selection_circle.x,
	    opponent_selection_circle.y,
	};
	unit_get_selection_circle (unit, &opponent_selection_circle);
	double safe_distance = selection_radius + opponent_selection_circle.radius;
	double current_distance = distance (opponent_selection_circle.x, opponent_selection_circle.y, pos[0], pos[1]);
	if (current_distance < safe_distance) {
	    double repulsion_factor = (safe_distance - current_distance)/safe_distance;
	    HC_TYPE (math.dvec2) current_repulsion;
	    if (current_distance < EPSILON1)
		sincos (random ()*M_PI*2/RAND_MAX, current_repulsion + 0, current_repulsion + 1);
	    else
		HC_CALL (math.dvec2.sub) (current_repulsion, (double *) pos, opponent_selection_circle_v);
	    HC_CALL (math.dvec2.resize) (current_repulsion, current_repulsion, repulsion_factor);
	    HC_CALL (math.dvec2.add) (repulsion, repulsion, current_repulsion);
	}
    }
    if (HC_CALL (math.dvec2.len) (repulsion) >= EPSILON1) {
	double distance = velocity*dtime;
	HC_CALL (math.dvec2.resize) (repulsion, repulsion, distance);
	double new_value[2];
	HC_CALL (math.dvec2.add) (new_value, (double *) pos, repulsion);
	property_set_value (pos_property, new_value);
    }
}

// class units.seal units.seal () | constructor
static unit_seal_t *unit_seal_create (world_t *world, HC_TYPE (math.dvec2) initial_pos, double initial_rotation)
{
    HC_TYPE (render.sprite) *sprite = world->unit_sprite_set->seal;
    unit_seal_t *s = check_malloc (sizeof (unit_seal_t));
    s->world = world;
    s->sprite = sprite;
    s->sprite_selection = world->unit_sprite_set->selection;
    s->rotation = initial_rotation;
    s->max_hp = 40;
    s->current_hp = 40;
    s->selected = 0;
    s->dead = 0;

    s->object = object_create ();
    {
	property_t *pos_property = property_create (sizeof (HC_TYPE (math.dvec2)), 300 /* TODO */);
	property_set_value (pos_property, initial_pos);
	object_insert_property (s->object, s->world->internal_strings.position, pos_property);
    }

    return s;
}
// ???
static void unit_seal_destroy (unit_seal_t *s)
{
    if (!s) return;
    object_destroy (s->object);
    free (s);
}
// void units.seal.update (double dtime)
static void unit_seal_update (unit_seal_t *s, double dtime)
{
    unit_seal_apply_movement (s, dtime);
    unit_seal_apply_repulsion (s, dtime);
}
// void units.seal.draw ()
static void unit_seal_draw (unit_seal_t *s)
{
    property_t *pos_property = object_get_property (s->object, s->world->internal_strings.position);
    if (pos_property) {
	const double *pos = property_get_value (pos_property);
	world_draw_circle (s->world, pos[0], pos[1], selection_radius, 0x0000ffff);
	if (s->selected)
	    world_draw_ring (s->world, pos[0], pos[1], selection_radius, 0x00ff00ff);
	world_draw_sprite (s->world, s->sprite, pos[0], pos[1], s->rotation);

	property_t *dest_property = object_get_property (s->object, s->world->internal_strings.destination);
	if (dest_property) {
	    const double *dest = property_get_value (dest_property);
	    if (pos[0] != dest[0] || pos[1] != dest[1])
		world_draw_line (s->world, pos[0], pos[1], dest[0], dest[1], 0x00ff00ff);
	}
    }
}
// void units.seal.set_selected (bool selected) | method
static void unit_seal_set_selected (unit_seal_t *s, int selected)
{
    s->selected = selected;
}
// bool units.seal.is_selected () | method
static int unit_seal_is_selected (unit_seal_t *s)
{
    return s->selected;
}
// optional dvec4 units.seal.get_bounding_box () | method
static void unit_seal_get_bounding_box (unit_seal_t *s, HC_TYPE (math.dvec4) bounding_box)
{
    property_t *pos_property = object_get_property (s->object, s->world->internal_strings.position);
    assert (pos_property); // TODO: Maintain
    const double *pos = property_get_value (pos_property);
    bounding_box[0] = pos[0] - selection_radius;
    bounding_box[1] = pos[1] - selection_radius;
    bounding_box[2] = pos[0] + selection_radius;
    bounding_box[3] = pos[1] + selection_radius;
}
static void unit_seal_get_selection_circle (unit_seal_t *s, HC_TYPE (math.circled) *selection_circle)
{
    property_t *pos_property = object_get_property (s->object, s->world->internal_strings.position);
    if (pos_property) {
	const double *pos = property_get_value (pos_property);
	selection_circle->x = pos[0];
	selection_circle->y = pos[1];
	selection_circle->radius = selection_radius;
    }
}
// void units.seal.move_to (dvec2 new_pos) | method
static void unit_seal_move_to (unit_seal_t *s, double dest_x, double dest_y)
{
    property_t *dest_property = property_create (sizeof (HC_TYPE (math.dvec2)), 300 /* TODO */);
    double value[2] = {
	dest_x,
	dest_y,
    };
    property_set_value (dest_property, value);
    object_insert_property (s->object, s->world->internal_strings.destination, dest_property);
}
// void units.seal.stop () | method
static void unit_seal_stop (unit_seal_t *s)
{
    object_drop_property (s->object, s->world->internal_strings.destination);
}

static const unit_vtable_t seal_vtable_impl = {
    .create = (void *) unit_seal_create,
    .destroy = (void *) unit_seal_destroy,
    .update = (void *) unit_seal_update,
    .draw = (void *) unit_seal_draw,
    .set_selected = (void *) unit_seal_set_selected,
    .is_selected = (void *) unit_seal_is_selected,
    .get_bounding_box = (void *) unit_seal_get_bounding_box,
    .get_selection_circle = (void *) unit_seal_get_selection_circle,
    .move_to = (void *) unit_seal_move_to,
    .stop = (void *) unit_seal_stop,
};

const unit_vtable_t *seal_vtable = &seal_vtable_impl;
