#pragma once

#include "world.h"

#include <GL/gl.h>

#define sprite_t HC_TYPE (render.sprite)

extern HC_TYPE (render.sprite) *sprite_create (
    uint32_t program,
    const char *fname);
