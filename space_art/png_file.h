#ifndef PNG_FILE_H
#define PNG_FILE_H

#include <stdint.h>

extern void *png_file_load (
    const char *fname,
    uint32_t *width,
    uint32_t *height);

#endif
