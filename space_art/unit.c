#include "unit.h"


void unit_init (world_t *world, unit_t *unit, const unit_vtable_t *vtable, HC_TYPE (math.dvec2) initial_pos, double initial_rotation)
{
    unit->data = vtable->create (world, initial_pos, initial_rotation);
    unit->vtable = vtable;
}
