#pragma once

#include <stddef.h>

typedef struct internal_string_set_t internal_string_set_t;
typedef struct string_t string_t;


extern internal_string_set_t *internal_string_set_create ();

extern void internal_string_set_destroy (
    internal_string_set_t *s);

extern const string_t *internal_string_set_add_string (
    internal_string_set_t *s,
    const char *data,
    size_t len);

extern const char *internal_string_get (
    string_t *str,
    size_t *len);
