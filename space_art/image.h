#pragma once

#include "world.h"

#include <GL/gl.h>

typedef struct image_t {
    GLuint program;
    GLuint vertex_buffer;
    GLint uniform_color_texture;
    GLint uniform_transformation;
    GLint vertex_pos;
    GLuint tex;
} image_t;

extern image_t *image_create (
    GLuint program,
    const char *fname);
