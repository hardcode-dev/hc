#ifndef PROPERTY_H
#define PROPERTY_H

#include "common.h"

#include <stdint.h>


typedef void (*property_callback_function_t) (const void *user_data, const void *value);

typedef struct property_callback_t property_callback_t;
typedef struct property_t property_t;


extern property_t *property_create (
    uint32_t size,
    uint32_t type);

extern void property_destroy (
    property_t *s);

// Copy memory area and call all callbacks in order of addition
extern void property_set_value (
    property_t *s,
    const void *new_value);

// Just return memory address
extern const void *property_get_value (
    property_t *s);

extern uint32_t property_get_type (
    property_t *s);

extern uint32_t property_get_size (
    property_t *s);

extern property_callback_t *property_add_callback (
    property_t *s,
    void *user_data,
    property_callback_function_t callback_function);

extern void property_drop_callback (
    property_t *s,
    property_callback_t *callback);

extern void property_clear_callbacks (
    property_t *s);

#endif
