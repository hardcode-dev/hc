#include "unit_array.h"

#include "common.h"
#include "unit.h"


void unit_array_init (unit_array_t *unit_array)
{
    unit_array->data = check_malloc (sizeof (unit_t)*32);
    unit_array->count = 0;
    unit_array->reserve = 32;
}
void unit_array_uninit (unit_array_t *unit_array)
{
    size_t i;
    unit_t *unit = unit_array->data;
    for (i = 0; i < unit_array->count; ++i, ++unit)
	unit_destroy (unit);
    free (unit_array->data);
}
unit_t *unit_array_open (unit_array_t *unit_array)
{
    if (unit_array->count == unit_array->reserve)
	unit_array->data = check_realloc (unit_array->data, sizeof (unit_t)*(unit_array->reserve <<= 1));
    return unit_array->data + unit_array->count++;
}
