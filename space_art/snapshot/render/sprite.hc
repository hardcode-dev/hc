@class@
uint program;
uint vertex_buffer;
int uniform_color_texture;
int uniform_transformation;
int vertex_pos;
int vertex_tex_coord;
uint tex;
@@

@class_baked@
uint32_t program;
uint32_t vertex_buffer;
int32_t uniform_color_texture;
int32_t uniform_transformation;
int32_t vertex_pos;
int32_t vertex_tex_coord;
uint32_t tex;
@@

@trait@
true
@@
