@class@
uint program;
uint vertex_buffer;
int uniform_transformation;
int uniform_color;
int vertex_pos;
int vertex_count;
@@

@class_baked@
uint32_t program;
uint32_t vertex_buffer;
int32_t uniform_transformation;
int32_t uniform_color;
int32_t vertex_pos;
int32_t vertex_count;
@@

@trait@
true
@@

@bind@
HC_TYPE (render.vertices) *(GLuint program) "render.vertices";
@@

@bind_name@
render.vertices
@@

@bind_baked@
HC_TYPE (render.vertices) * (*_) (GLuint program);
@@
