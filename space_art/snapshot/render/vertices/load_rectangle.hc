@bind@
void (HC_TYPE (render.vertices) *s, const double *bounding_box) "render.vertices.load_rectangle";
@@

@bind_name@
render.vertices.load_rectangle
@@

@bind_baked@
void (*_) (HC_TYPE (render.vertices) *s, const double *bounding_box);
@@
