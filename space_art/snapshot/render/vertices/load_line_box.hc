@bind@
void (HC_TYPE (render.vertices) *s, const double *box) "render.vertices.load_line_box";
@@

@bind_name@
render.vertices.load_line_box
@@

@bind_baked@
void (*_) (HC_TYPE (render.vertices) *s, const double *box);
@@
