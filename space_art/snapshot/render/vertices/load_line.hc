@bind@
void (HC_TYPE (render.vertices) *s, double src_x, double src_y, double dest_x, double dest_y) "render.vertices.load_line";
@@

@bind_name@
render.vertices.load_line
@@

@bind_baked@
void (*_) (HC_TYPE (render.vertices) *s, double src_x, double src_y, double dest_x, double dest_y);
@@
