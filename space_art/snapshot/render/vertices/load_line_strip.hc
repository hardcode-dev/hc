@bind@
void (HC_TYPE (render.vertices) *s, size_t point_count, const double *points) "render.vertices.load_line_strip";
@@

@bind_name@
render.vertices.load_line_strip
@@

@bind_baked@
void (*_) (HC_TYPE (render.vertices) *s, size_t point_count, const double *points);
@@
