@bind@
void (HC_TYPE (render.vertices) *s, double x, double y, double radius) "render.vertices.load_circle";
@@

@bind_name@
render.vertices.load_circle
@@

@bind_baked@
void (*_) (HC_TYPE (render.vertices) *s, double x, double y, double radius);
@@
