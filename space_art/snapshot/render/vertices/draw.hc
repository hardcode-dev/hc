@bind@
void (HC_TYPE (render.vertices) *s, GLenum mode) "render.vertices.draw";
@@

@bind_name@
render.vertices.draw
@@

@bind_baked@
void (*_) (HC_TYPE (render.vertices) *s, GLenum mode);
@@
