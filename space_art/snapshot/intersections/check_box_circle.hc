@bind@
hc_bool_t (HC_TYPE (math.boxd) *box, HC_TYPE (math.circled) *circle) "intersections.check_box_circle";
@@

@bind_name@
intersections.check_box_circle
@@

@bind_baked@
hc_bool_t (*_) (HC_TYPE (math.boxd) *box, HC_TYPE (math.circled) *circle);
@@
