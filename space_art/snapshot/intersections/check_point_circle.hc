@bind@
hc_bool_t (HC_TYPE (math.dvec2) point, HC_TYPE (math.circled) *circle) "intersections.check_point_circle";
@@

@bind_name@
intersections.check_point_circle
@@

@bind_baked@
hc_bool_t (*_) (HC_TYPE (math.dvec2) point, HC_TYPE (math.circled) *circle);
@@
