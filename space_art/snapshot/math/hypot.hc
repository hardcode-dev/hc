@bind@
double (double a, double b) "math.hypot";
@@

@bind_name@
math.hypot
@@

@bind_baked@
double (*_) (double a, double b);
@@
