@function_return_type_baked@
double
@@

@function_return_type@
double
@@

@function_parameters_baked@
double a, double b
@@

@function_parameters@
double& a, double& b
@@

@function_body@
a*a + b*b
@@
