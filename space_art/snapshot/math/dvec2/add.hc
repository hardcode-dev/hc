@bind@
void (HC_TYPE (math.dvec2) r, HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b) "math.dvec2.add";
@@

@bind_name@
math.dvec2.add
@@

@bind_baked@
void (*_) (HC_TYPE (math.dvec2) r, HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b);
@@
