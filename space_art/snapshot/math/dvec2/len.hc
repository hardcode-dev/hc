@bind@
double (HC_TYPE (math.dvec2) v) "math.dvec2.len";
@@

@bind_name@
math.dvec2.len
@@

@bind_baked@
double (*_) (HC_TYPE (math.dvec2) v);
@@
