@bind@
void (HC_TYPE (math.dvec2) d, HC_TYPE (math.dvec2) s, double scale) "math.dvec2.resize";
@@

@bind_name@
math.dvec2.resize
@@

@bind_baked@
void (*_) (HC_TYPE (math.dvec2) d, HC_TYPE (math.dvec2) s, double scale);
@@
