@bind@
double (HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b) "math.dvec2.mul_inner";
@@

@bind_name@
math.dvec2.mul_inner
@@

@bind_baked@
double (*_) (HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b);
@@
