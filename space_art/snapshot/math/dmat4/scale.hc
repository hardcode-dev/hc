@bind@
void (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, double s) "math.dmat4.scale";
@@

@bind_name@
math.dmat4.scale
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, double s);
@@
