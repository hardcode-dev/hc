@bind@
void (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, HC_TYPE (math.dmat4) b) "math.dmat4.sub";
@@

@bind_name@
math.dmat4.sub
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, HC_TYPE (math.dmat4) b);
@@
