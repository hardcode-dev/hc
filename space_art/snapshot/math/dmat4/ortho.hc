@bind@
void (HC_TYPE (math.dmat4) D, double l, double r, double b, double t, double n, double f) "math.dmat4.ortho";
@@

@bind_name@
math.dmat4.ortho
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) D, double l, double r, double b, double t, double n, double f);
@@
