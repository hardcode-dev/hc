@bind@
void (HC_TYPE (math.dmat4) R, HC_TYPE (math.dmat4) M, double x, double y, double z, double angle) "math.dmat4.rotate";
@@

@bind_name@
math.dmat4.rotate
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) R, HC_TYPE (math.dmat4) M, double x, double y, double z, double angle);
@@
