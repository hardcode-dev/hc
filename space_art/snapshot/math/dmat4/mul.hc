@bind@
void (HC_TYPE (math.dmat4) d, const HC_TYPE (math.dmat4) a, const HC_TYPE (math.dmat4) b) "math.dmat4.mul";
@@

@bind_name@
math.dmat4.mul
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) d, const HC_TYPE (math.dmat4) a, const HC_TYPE (math.dmat4) b);
@@
