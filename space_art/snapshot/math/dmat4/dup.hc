@bind@
void (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) N) "math.dmat4.dup";
@@

@bind_name@
math.dmat4.dup
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) N);
@@
