@bind@
void (HC_TYPE (math.dmat4) M, HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b) "math.dmat4.from_vec3_mul_outer";
@@

@bind_name@
math.dmat4.from_vec3_mul_outer
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) M, HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b);
@@
