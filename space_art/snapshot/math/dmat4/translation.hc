@bind@
void (HC_TYPE (math.dmat4) T, double x, double y, double z) "math.dmat4.translation";
@@

@bind_name@
math.dmat4.translation
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) T, double x, double y, double z);
@@
