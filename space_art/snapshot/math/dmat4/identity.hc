@function_return_type_baked@
void
@@

@function_return_type@
void
@@

@function_parameters_baked@
HC_TYPE (math.dmat4) m
@@

@function_parameters@
math.dmat4& m
@@

@function_body@
m[0] = 1.0;
m[1] = 0.0;
m[2] = 0.0;
m[3] = 0.0;

m[4] = 0.0;
m[5] = 1.0;
m[6] = 0.0;
m[7] = 0.0;

m[8] = 0.0;
m[9] = 0.0;
m[10] = 1.0;
m[11] = 0.0;

m[12] = 0.0;
m[13] = 0.0;
m[14] = 0.0;
m[15] = 1.0;
@@
