@bind@
void (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, HC_TYPE (math.dmat4) b) "math.dmat4.add";
@@

@bind_name@
math.dmat4.add
@@

@bind_baked@
void (*_) (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, HC_TYPE (math.dmat4) b);
@@
