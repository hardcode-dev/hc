@bind@
double (HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b) "math.dvec3.mul_inner";
@@

@bind_name@
math.dvec3.mul_inner
@@

@bind_baked@
double (*_) (HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b);
@@
