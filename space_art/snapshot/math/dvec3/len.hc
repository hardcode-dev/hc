@bind@
double (HC_TYPE (math.dvec3) v) "math.dvec3.len";
@@

@bind_name@
math.dvec3.len
@@

@bind_baked@
double (*_) (HC_TYPE (math.dvec3) v);
@@
