@function_return_type_baked@
double
@@

@function_return_type@
double
@@

@function_parameters_baked@
HC_TYPE (math.dvec3) v
@@

@function_parameters@
math.dvec3& v
@@

@function_body@
v[0]*v[0] + v[1]*v[1] + v[2]*v[2]
@@
