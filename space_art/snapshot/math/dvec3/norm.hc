@bind@
void (HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b) "math.dvec3.norm";
@@

@bind_name@
math.dvec3.norm
@@

@bind_baked@
void (*_) (HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b);
@@
