@bind@
double (double v) "math.sqrt";
@@

@bind_name@
math.sqrt
@@

@bind_baked@
double (*_) (double v);
@@
