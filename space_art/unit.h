#pragma once

#include ".hc_types.h"
#include ".hc_type.h"

typedef struct world_t world_t;

typedef struct unit_vtable_t {
    void *(*create) (world_t *world, HC_TYPE (math.dvec2) initial_pos, float initial_rotation);
    void (*destroy) (void *s);
    void (*update) (void *s, double dtime);
    void (*draw) (void *s);
    void (*set_selected) (void *s, int selected);
    int (*is_selected) (void *s);
    void (*get_bounding_box) (void *s, double *bounding_box);
    void (*get_selection_circle) (void *s, HC_TYPE (math.circled) *selection_circle);
    void (*move_to) (void *s, double dest_x, double dest_y);
    void (*stop) (void *s);
} unit_vtable_t;

typedef struct unit_t {
    void *data;
    const unit_vtable_t *vtable;
} unit_t;


static inline void unit_destroy (unit_t *unit)
{
    unit->vtable->destroy (unit->data);
}
static inline void unit_update (unit_t *unit, double dtime)
{
    unit->vtable->update (unit->data, dtime);
}
static inline void unit_draw (unit_t *unit)
{
    unit->vtable->draw (unit->data);
}
static inline void unit_set_selected (unit_t *unit, int selected)
{
    unit->vtable->set_selected (unit->data, selected);
}
static inline int unit_is_selected (unit_t *unit)
{
    return unit->vtable->is_selected (unit->data);
}
static inline void unit_get_bounding_box (unit_t *unit, HC_TYPE (math.dvec4) bounding_box)
{
    unit->vtable->get_bounding_box (unit->data, bounding_box);
}
static inline void unit_get_selection_circle (unit_t *unit, HC_TYPE (math.circled) *selection_circle)
{
    unit->vtable->get_selection_circle (unit->data, selection_circle);
}
static inline void unit_move_to (unit_t *unit, double dest_x, double dest_y)
{
    unit->vtable->move_to (unit->data, dest_x, dest_y);
}
static inline void unit_stop (unit_t *unit)
{
    unit->vtable->stop (unit->data);
}

extern void unit_init (
    world_t *world,
    unit_t *unit,
    const unit_vtable_t *vtable,
    HC_TYPE (math.dvec2) initial_pos,
    double initial_rotation);
