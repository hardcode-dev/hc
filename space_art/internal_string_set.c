#include "internal_string_set.h"

#include "common.h"

#include <glib.h>


static void key_free (GString *key)
{
    g_string_free (key, TRUE);
}

typedef struct internal_string_set_t {
    GHashTable *string_set;
    pthread_mutex_t mutex;
} internal_string_set_t;


internal_string_set_t *internal_string_set_create ()
{
    internal_string_set_t *s = check_malloc (sizeof (internal_string_set_t));
    s->string_set = g_hash_table_new_full ((GHashFunc) g_string_hash, (GEqualFunc) g_string_equal, (GDestroyNotify) key_free, (GDestroyNotify) NULL);
    pthread_mutex_init (&s->mutex, NULL);
    return s;
}
void internal_string_set_destroy (internal_string_set_t *s)
{
    if (!s) return;
    pthread_mutex_destroy (&s->mutex);
    g_hash_table_destroy (s->string_set);
}
const string_t *internal_string_set_add_string (internal_string_set_t *s, const char *data, size_t len)
{
    pthread_mutex_lock (&s->mutex);
    GString *str = g_string_new_len (data, len);
    string_t *internalized_string = g_hash_table_lookup (s->string_set, str);
    if (!internalized_string) {
	g_hash_table_add (s->string_set, str);
	internalized_string = (string_t *) str;
    }
    pthread_mutex_unlock (&s->mutex);
    return internalized_string;
}
const char *internal_string_get (string_t *str, size_t *len)
{
    *len = ((GString*) str)->len;
    return ((GString*) str)->str;
}
