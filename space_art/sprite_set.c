#include "sprite_set.h"

#include "common.h"
#include "sprite.h"


unit_sprite_set_t *unit_sprite_set_create (GLuint sprite_program)
{
    unit_sprite_set_t *s = check_malloc (sizeof (unit_sprite_set_t));
    s->crusader = sprite_create (sprite_program, "data/crusader.png");
    s->seal = sprite_create (sprite_program, "data/seal.png");
    s->harvester = sprite_create (sprite_program, "data/harvester.png");
    s->prophet = sprite_create (sprite_program, "data/prophet.png");
    s->selection = sprite_create (sprite_program, "data/selection.png");
    return s;
}
