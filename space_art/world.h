#pragma once

#include "unit_array.h"
#include "sprite_set.h"
#include "internal_string_set.h"
#include ".hc_type.h"
#include ".hc_types.h"

#include <stdint.h>
#include <GL/gl.h>


typedef struct image_t image_t;
typedef struct world_t world_t;
typedef struct unit_array_t unit_array_t;


typedef struct world_t {
    internal_string_set_t *internal_string_set;
    struct {
	const string_t *position;
	const string_t *destination;
    } internal_strings;
    HC_TYPE (math.dvec2) area_size;
    HC_TYPE (math.dvec2) cursor_pos;
    char shift_pressed;
    hc_bool_t selecting;
    HC_TYPE (math.dvec2) selecting_start_pos;
    uint32_t sprite_program;
    uint32_t image_program;
    uint32_t vertices_program;
    image_t *background_image;
    image_t *image;
    image_t *cursor_image;
    HC_TYPE (render.vertices) *vertices;
    unit_sprite_set_t *unit_sprite_set;
    unit_array_t unit_array;
} world_t;


extern world_t *world_create (
    uint32_t width,
    uint32_t height);

extern void world_destroy (
    world_t *s);

extern void world_update (
    world_t *s,
    double dtime);

extern void world_draw (
    world_t *s);

extern void world_draw_sprite (
    world_t *s,
    HC_TYPE (render.sprite) *sprite,
    double center_x,
    double center_y,
    double rotation);

extern void world_draw_image (
    world_t *s,
    image_t *image,
    int x,
    int y);

extern void world_draw_rectangle (
    world_t *s,
    double x,
    double y,
    double w,
    double h,
    uint32_t color);

extern void world_draw_circle (
    world_t *s,
    double x,
    double y,
    double radius,
    uint32_t color);

extern void world_draw_ring (
    world_t *s,
    double x,
    double y,
    double radius,
    uint32_t color);

extern void world_draw_line (
    world_t *s,
    double src_x,
    double src_y,
    double dest_x,
    double dest_y,
    uint32_t color);

extern void world_draw_line_strip (
    world_t *s,
    size_t point_count,
    const double *points,
    uint32_t color);

extern void world_draw_line_box (
    world_t *s,
    const double *bounding_box,
    uint32_t color);

extern void world_input_event_key (
    world_t *s,
    int key,
    int action);

extern void world_input_event_mouse_button (
    world_t *s,
    int button,
    int action);

extern void world_input_event_mouse_cursor_move_callback (
    world_t *s,
    double dx,
    double dy);
