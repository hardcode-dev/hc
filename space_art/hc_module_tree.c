#include "hc_module_tree.h"

#include <stdio.h>


hc_t hc;

#include "hc/.generated_functions_impl.h"


static void load_functions ()
{
#include "hc/.load_functions_impl.h"
}
static void load_generated_functions ()
{
#include "hc/.generated_functions_load.h"
}

static void key_free (GString *key)
{
    g_string_free (key, TRUE);
}

void hc_init ()
{
    hc.function_map = g_hash_table_new_full ((GHashFunc) g_string_hash, (GEqualFunc) g_string_equal, (GDestroyNotify) key_free, (GDestroyNotify) NULL);
}
void hc_module_tree_init ()
{
    load_functions ();
    load_generated_functions ();
}
void hc_bind_function (const char *name, void *function)
{
    GString *name_str = g_string_new (name);
    if (!g_hash_table_lookup (hc.function_map, name_str))
    	g_hash_table_insert (hc.function_map, name_str, function);
}
void *hc_find_function (const char *name)
{
    GString *name_str = g_string_new (name);
    return g_hash_table_lookup (hc.function_map, name_str);
}
