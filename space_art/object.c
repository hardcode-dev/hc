#include "object.h"

#include "property.h"

#include <glib.h>


typedef struct object_t {
    void *feature_set;
    GHashTable *property_map; /* GString -> property_t */
} object_t;


object_t *object_create ()
{
    object_t *s = check_malloc (sizeof (object_t));
    s->feature_set = NULL;
    s->property_map = g_hash_table_new_full ((GHashFunc) g_direct_hash, (GEqualFunc) g_direct_equal, (GDestroyNotify) NULL, (GDestroyNotify) property_destroy);
    return s;
}
void object_destroy (object_t *s)
{
    if (!s) return;
    g_hash_table_destroy (s->property_map);
    free (s);
}
property_t *object_get_property (object_t *s, const string_t *key)
{
    return g_hash_table_lookup (s->property_map, key);
}
void object_insert_property (object_t *s, const string_t *key, property_t *property)
{
    g_hash_table_insert (s->property_map, (void*) key, property);
}
void object_drop_property (object_t *s, const string_t *key)
{
    g_hash_table_remove (s->property_map, (void*) key);
}
