#include "image.h"

#include "common.h"
#include "png_file.h"


image_t *image_create (GLuint program, const char *fname)
{
    uint32_t tex_w, tex_h;
    void *pixels = png_file_load (fname, &tex_w, &tex_h);
    if (!pixels)
	return NULL;

    image_t *s = check_malloc (sizeof (image_t));

    s->program = program;

    float vertices[] = {
	0.0f,  0.0f,
	tex_w, 0.0f,
	tex_w, tex_h,
    	0.0f,  0.0f,
	tex_w, tex_h,
	0.0f,  tex_h,
    };

    glGenBuffers (1, &s->vertex_buffer);
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*6, vertices, GL_STATIC_DRAW);

    s->uniform_transformation = glGetUniformLocation (s->program, "transformation");
    s->uniform_color_texture = glGetUniformLocation (s->program, "color_texture");
    s->vertex_pos = glGetAttribLocation (s->program, "pos");

    glGenTextures (1, &s->tex);
    glBindTexture (GL_TEXTURE_RECTANGLE, s->tex);

    glTexImage2D (GL_TEXTURE_RECTANGLE, 0, GL_RGBA, tex_w, tex_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    free (pixels);

    return s;
}
