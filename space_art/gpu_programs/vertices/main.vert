#version 400

uniform mat4 transformation;

in vec2 pos;

void main()
{
    gl_Position = transformation * vec4 (pos, 0.0, 1.0);
}
