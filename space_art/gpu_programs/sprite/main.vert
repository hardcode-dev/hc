#version 400

uniform mat4 transformation;

in vec2 pos;
in vec2 tex_coord;

out vec2 int_pos;

void main()
{
    int a;
    gl_Position = transformation*vec4 (pos, 0.0, 1.0);
    int_pos = tex_coord.xy;
}
