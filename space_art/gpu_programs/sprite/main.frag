#version 400

uniform sampler2D color_texture;

in vec2 int_pos;

layout (location = 0) out vec4 frag_color;

void main ()
{
    frag_color = texture2D (color_texture, int_pos);
}
