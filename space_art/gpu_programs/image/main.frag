#version 400
#extension GL_ARB_texture_rectangle : enable

uniform sampler2DRect color_texture;

in vec2 int_tex_coord;

layout (location = 0) out vec4 frag_color;

void main ()
{
    frag_color = texture2DRect (color_texture, int_tex_coord);
}
