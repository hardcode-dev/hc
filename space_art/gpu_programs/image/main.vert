#version 400

uniform mat4 transformation;

in vec2 pos;

out vec2 int_tex_coord;

void main()
{
    int a;
    gl_Position = transformation * vec4 (pos, 0.0, 1.0);
    int_tex_coord = pos.xy;
}
