#include "common.h"
#include "world.h"
#include "runtime.h"
#include "hc_module_tree.h"
#include "bindings.h"

#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>


static const double mouse_sensitivity = 0.2;


static void error_callback (int error, const char *description)
{
    (void) error;

    fprintf (stderr, "Error: %s\n", description);
}
static void key_callback (GLFWwindow *window, int key, int scancode, int action, int mods)
{
    (void) scancode;
    (void) mods;

    world_t *world = glfwGetWindowUserPointer (window);

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose (window, GLFW_TRUE);
	return;
    }

    world_input_event_key (world, key, action);
}
static void mouse_button_callback (GLFWwindow *window, int button, int action, int mods)
{
    (void) mods;

    world_t *world = glfwGetWindowUserPointer (window);
    world_input_event_mouse_button (world, button, action);
}
static void mouse_cursor_move_callback (GLFWwindow *window, double dx, double dy)
{
    world_t *world = glfwGetWindowUserPointer (window);
    world_input_event_mouse_cursor_move_callback (world, mouse_sensitivity*dx, mouse_sensitivity*dy);
    glfwSetCursorPos (window, 0.0, 0.0);
}
static void mouse_cursor_move_pre_callback (GLFWwindow *window, double dx, double dy)
{
    (void) dx;
    (void) dy;

    glfwSetCursorPos (window, 0.0, 0.0);
    glfwSetCursorPosCallback (window, mouse_cursor_move_callback);
}
static inline double diff_secs (const struct timespec *a, const struct timespec *b)
{
    struct timespec diff = {
	.tv_sec = a->tv_sec - b->tv_sec,
	.tv_nsec = a->tv_nsec - b->tv_nsec,
    };
    if (diff.tv_nsec < 0) {
	diff.tv_sec--;
	diff.tv_nsec += 1000000000;
    }
    return diff.tv_sec + diff.tv_nsec*0.000000001;
}

int main ()
{
    hc_init ();
    bind_functions ();
    hc_module_tree_init ();
    runtime_init ();

    int win_w = 1920;
    int win_h = 1080;
    glfwSetErrorCallback (error_callback);
    if (!glfwInit ())
        exit (EXIT_FAILURE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint (GLFW_DECORATED, GL_FALSE);
    GLFWwindow *window = glfwCreateWindow (win_w, win_h, "Simple example", NULL, NULL);
    if (!window) {
        glfwTerminate ();
        exit (EXIT_FAILURE);
    }
    glfwMakeContextCurrent (window);
    glfwSwapInterval (1);

    world_t *world = world_create (win_w, win_h);
    glfwSetWindowUserPointer (window, world);

    glfwSetKeyCallback (window, key_callback);
    glfwSetMouseButtonCallback (window, mouse_button_callback);
    glfwSetCursorPosCallback (window, mouse_cursor_move_pre_callback);

    glfwSetCursorPos (window, 0.0, 0.0);

    glfwSetInputMode (window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetInputMode (window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);

    struct timespec prev_ts;
    clock_gettime (CLOCK_MONOTONIC_RAW, &prev_ts);

    while (!glfwWindowShouldClose (window)) {
	struct timespec ts;
	clock_gettime (CLOCK_MONOTONIC_RAW, &ts);
	world_update (world, diff_secs (&ts, &prev_ts));
	prev_ts = ts;

        int width, height;
        glfwGetFramebufferSize (window, &width, &height);
        glViewport (0, height - win_h, win_w, win_h);
        glClear (GL_COLOR_BUFFER_BIT);

	world_draw (world);

        glfwSwapBuffers (window);
        glfwPollEvents ();
    }

    glfwDestroyWindow (window);
    glfwTerminate ();

    return 0;
}
