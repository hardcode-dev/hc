#include "world.h"
#include "common.h"
#include "hc_module_tree.h"
#include ".hc_types.h"

#include <GL/gl.h>


static HC_TYPE (render.vertices) *binding_render_vertices (GLuint program)
{
    HC_TYPE (render.vertices) *s = check_malloc (sizeof (HC_TYPE (render.vertices)));

    s->program = program;
    glGenBuffers (1, &s->vertex_buffer);
    s->uniform_transformation = glGetUniformLocation (s->program, "transformation");
    s->uniform_color = glGetUniformLocation (s->program, "color");
    s->vertex_pos = glGetAttribLocation (s->program, "pos");

    return s;
}
static void binding_render_vertices_draw (HC_TYPE (render.vertices) *s, GLenum mode)
{
    glDrawArrays (mode, 0, s->vertex_count);
}
static void binding_render_vertices_load_circle (HC_TYPE (render.vertices) *s, double x, double y, double radius)
{
    const size_t segment_count = 16;
    float vertices[6*segment_count];
    size_t i;
    for (i = 0; i < segment_count; ++i) {
	double rx, ry;
	vertices[i*6 + 0] = x;
	vertices[i*6 + 1] = y;

	sincos (i*M_PI*2.0/segment_count, &ry, &rx);
	vertices[i*6 + 2] = x + rx*radius;
	vertices[i*6 + 3] = y + ry*radius;

	sincos ((i + 1)*M_PI*2.0/segment_count, &ry, &rx);
	vertices[i*6 + 4] = x + rx*radius;
	vertices[i*6 + 5] = y + ry*radius;
    }

    s->vertex_count = 3*segment_count;
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*s->vertex_count, vertices, GL_DYNAMIC_DRAW);
}
static void binding_render_vertices_load_line (HC_TYPE (render.vertices) *s, double src_x, double src_y, double dest_x, double dest_y)
{
    float vertices[] = {
	src_x, src_y,
	dest_x, dest_y,
    };
    s->vertex_count = 2;
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*s->vertex_count, vertices, GL_DYNAMIC_DRAW);
}
static void binding_render_vertices_load_line_box (HC_TYPE (render.vertices) *s, const double *box)
{
    float vertices[2*4] = {
	box[0], box[1],
	box[2], box[1],
	box[2], box[3],
	box[0], box[3],
    };
    s->vertex_count = 4;
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*s->vertex_count, vertices, GL_DYNAMIC_DRAW);
}
static void binding_render_vertices_load_line_strip (HC_TYPE (render.vertices) *s, size_t point_count, const double *points)
{
    float *vertices = check_malloc (sizeof (float[2])*point_count);
    size_t i;
    for (i = 0; i < point_count; ++i) {
	vertices[i*2 + 0] = points[i*2 + 0];
	vertices[i*2 + 1] = points[i*2 + 1];
    }
    s->vertex_count = point_count;
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*s->vertex_count, vertices, GL_DYNAMIC_DRAW);
    free (vertices);
}
static void binding_render_vertices_load_rectangle (HC_TYPE (render.vertices) *s, const double *bounding_box)
{
    float vertices[] = {
    	bounding_box[0], bounding_box[1],
    	bounding_box[2], bounding_box[1],
    	bounding_box[2], bounding_box[3],
    	bounding_box[0], bounding_box[1],
    	bounding_box[2], bounding_box[3],
    	bounding_box[0], bounding_box[3],
    };
    s->vertex_count = 6;
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*s->vertex_count, vertices, GL_DYNAMIC_DRAW);
}
static void binding_render_vertices_load_ring (HC_TYPE (render.vertices) *s, double x, double y, double radius)
{
    const size_t segment_count = 16;
    float vertices[2*segment_count];
    size_t i;
    for (i = 0; i < segment_count; ++i) {
	double rx, ry;
	sincos (i*M_PI*2.0/segment_count, &ry, &rx);
	vertices[i*2 + 0] = x + rx*radius;
	vertices[i*2 + 1] = y + ry*radius;
    }

    s->vertex_count = segment_count;
    glBindBuffer (GL_ARRAY_BUFFER, s->vertex_buffer);
    glBufferData (GL_ARRAY_BUFFER, sizeof (float[2])*s->vertex_count, vertices, GL_DYNAMIC_DRAW);
}
static hc_bool_t binding_intersections_check_point_circle (HC_TYPE (math.dvec2) point, HC_TYPE (math.circled) *circle)
{
    double dx = point[0] - circle->x;
    double dy = point[1] - circle->y;
    return (dx*dx + dy*dy) < circle->radius*circle->radius;
}
static hc_bool_t binding_intersections_check_box_circle (HC_TYPE (math.boxd) *box, HC_TYPE (math.circled) *circle)
{
    double radius = circle->radius;
    double altered_box[] = {
	box->left - circle->x,
	box->top - circle->y,
	box->right - circle->x,
	box->bottom - circle->y,
    };
    if (altered_box[0] >= radius ||
	altered_box[1] >= radius ||
	altered_box[2] <= -radius ||
	altered_box[3] <= -radius)
	return 0;

    if ((altered_box[0] <= 0.0 && altered_box[2] >= 0.0) ||
	(altered_box[1] <= 0.0 && altered_box[3] >= 0.0))
	return 1;

    double corner_x, corner_y;

    if (altered_box[0] > 0.0)
	corner_x = altered_box[0];
    else if (altered_box[2] < 0.0)
	corner_x = altered_box[2];
    else
	return 1;

    if (altered_box[1] > 0.0)
	corner_y = altered_box[1];
    else if (altered_box[3] < 0.0)
	corner_y = altered_box[3];
    else
	return 1;

    return (corner_x*corner_x + corner_y*corner_y) <= radius*radius;
}

static double binding_math_sqrt (double v)
{
    return sqrt (v);
}
static double binding_math_hypot (double a, double b)
{
    return sqrt (HC_CALL (math.square_hypot) (a, b));
}
static double binding_math_dvec2_len (HC_TYPE (math.dvec2) v)
{
    return HC_CALL (math.sqrt) (HC_CALL (math.dvec2.mul_inner) (v, v));
}
static void binding_math_dvec2_sub (HC_TYPE (math.dvec2) r, HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b)
{
    r[0] = a[0] - b[0];
    r[1] = a[1] - b[1];
}
static void binding_math_dvec2_add (HC_TYPE (math.dvec2) r, HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b)
{
    r[0] = a[0] + b[0];
    r[1] = a[1] + b[1];
}
static void binding_math_dvec2_resize (HC_TYPE (math.dvec2) d, HC_TYPE (math.dvec2) s, double scale)
{
    d[0] = s[0]*scale;
    d[1] = s[1]*scale;
}
static double binding_math_dvec2_mul_inner (HC_TYPE (math.dvec2) a, HC_TYPE (math.dvec2) b)
{
    return a[0]*b[0] + a[1]*b[1];
}

static double binding_math_dvec3_mul_inner (HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b)
{
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}
static double binding_math_dvec3_len (HC_TYPE (math.dvec3) v)
{
    return sqrt (HC_CALL (math.dvec3.mul_inner) (v, v));
}
static void binding_math_dvec3_norm (HC_TYPE (math.dvec3) d, HC_TYPE (math.dvec3) s)
{
    double k = 1.0/HC_CALL (math.dvec3.len) (s);
    d[0] = s[0]*k;
    d[1] = s[1]*k;
    d[2] = s[2]*k;
}

static void binding_math_dmat4_dup (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) N)
{
    size_t i;
    for (i = 0; i < 16; ++i)
	M[i] = N[i];
}
static void binding_math_dmat4_add (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, HC_TYPE (math.dmat4) b)
{
    size_t i;
    for (i = 0; i < 16; ++i)
	M[i] = a[i] + b[i];
}
static void binding_math_dmat4_sub (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, HC_TYPE (math.dmat4) b)
{
    size_t i;
    for (i = 0; i < 16; ++i)
	M[i] = a[i] - b[i];
}
static void binding_math_dmat4_mul (HC_TYPE (math.dmat4) d, const HC_TYPE (math.dmat4) a, const HC_TYPE (math.dmat4) b)
{
    d[0] = a[0]*b[0] + a[4]*b[1] + a[8]*b[2] + a[12]*b[3];
    d[1] = a[1]*b[0] + a[5]*b[1] + a[9]*b[2] + a[13]*b[3];
    d[2] = a[2]*b[0] + a[6]*b[1] + a[10]*b[2] + a[14]*b[3];
    d[3] = a[3]*b[0] + a[7]*b[1] + a[11]*b[2] + a[15]*b[3];

    d[4] = a[0]*b[4] + a[4]*b[5] + a[8]*b[6] + a[12]*b[7];
    d[5] = a[1]*b[4] + a[5]*b[5] + a[9]*b[6] + a[13]*b[7];
    d[6] = a[2]*b[4] + a[6]*b[5] + a[10]*b[6] + a[14]*b[7];
    d[7] = a[3]*b[4] + a[7]*b[5] + a[11]*b[6] + a[15]*b[7];

    d[8] = a[0]*b[8] + a[4]*b[9] + a[8]*b[10] + a[12]*b[11];
    d[9] = a[1]*b[8] + a[5]*b[9] + a[9]*b[10] + a[13]*b[11];
    d[10] = a[2]*b[8] + a[6]*b[9] + a[10]*b[10] + a[14]*b[11];
    d[11] = a[3]*b[8] + a[7]*b[9] + a[11]*b[10] + a[15]*b[11];

    d[12] = a[0]*b[12] + a[4]*b[13] + a[8]*b[14] + a[12]*b[15];
    d[13] = a[1]*b[12] + a[5]*b[13] + a[9]*b[14] + a[13]*b[15];
    d[14] = a[2]*b[12] + a[6]*b[13] + a[10]*b[14] + a[14]*b[15];
    d[15] = a[3]*b[12] + a[7]*b[13] + a[11]*b[14] + a[15]*b[15];
}
static void binding_math_dmat4_scale (HC_TYPE (math.dmat4) M, HC_TYPE (math.dmat4) a, double s)
{
    size_t i;
    for (i = 0; i < 16; ++i)
	M[i] = a[i]*s;
}
static void bindings_math_dmat4_from_vec3_mul_outer (HC_TYPE (math.dmat4) M, HC_TYPE (math.dvec3) a, HC_TYPE (math.dvec3) b)
{
    M[0] = a[0]*b[0];
    M[1] = a[0]*b[1];
    M[2] = a[0]*b[2];
    M[3] = 0.0f;

    M[4] = a[1]*b[0];
    M[5] = a[1]*b[1];
    M[6] = a[1]*b[2];
    M[7] = 0.0f;

    M[8] = a[2]*b[0];
    M[9] = a[2]*b[1];
    M[10] = a[2]*b[2];
    M[11] = 0.0f;

    M[12] = 0.0f;
    M[13] = 0.0f;
    M[14] = 0.0f;
    M[15] = 0.0f;
}
static void binding_math_dmat4_rotate (HC_TYPE (math.dmat4) R, HC_TYPE (math.dmat4) M, double x, double y, double z, double angle)
{
    double s = sinf (angle);
    double c = cosf (angle);
    HC_TYPE (math.dvec3) u = {x, y, z};

    if (HC_CALL (math.dvec3.len) (u) > 1e-4) {
	HC_CALL (math.dvec3.norm) (u, u);
	HC_TYPE (math.dmat4) T;
	HC_CALL (math.dmat4.from_vec3_mul_outer) (T, u, u);

	HC_TYPE (math.dmat4) S = {
	    0, u[2]*s, -u[1]*s, 0,
	    -u[2]*s, 0, u[0]*s, 0,
	    u[1]*s, -u[0]*s, 0, 0,
	    0, 0, 0, 0,
	};

	HC_TYPE (math.dmat4) C;
	HC_CALL (math.dmat4.identity) (C);
	HC_CALL (math.dmat4.sub) (C, C, T);

	HC_CALL (math.dmat4.scale) (C, C, c);

	HC_CALL (math.dmat4.add) (T, T, C);
	HC_CALL (math.dmat4.add) (T, T, S);

	T[15] = 1.0f;
	HC_CALL (math.dmat4.mul) (R, M, T);
    } else {
	HC_CALL (math.dmat4.dup) (R, M);
    }
}
static void binding_math_dmat4_translation (HC_TYPE (math.dmat4) T, double x, double y, double z)
{
    HC_CALL (math.dmat4.identity) (T);
    T[12] = x;
    T[13] = y;
    T[14] = z;
}
static void binding_math_dmat4_ortho (HC_TYPE (math.dmat4) D, double l, double r, double b, double t, double n, double f)
{
    D[0] = 2.0/(r - l);
    D[1] = 0.0;
    D[2] = 0.0;
    D[3] = 0.0;

    D[4] = 0.0;
    D[5] = 2.0/(t - b);
    D[6] = 0.0;
    D[7] = 0.0;

    D[8] = 0.0;
    D[9] = 0.0;
    D[10] = -2.0/(f - n);
    D[11] = 0.0;

    D[12] = (r + l)/(l - r);
    D[13] = (t + b)/(b - t);
    D[14] = (f + n)/(n - f);
    D[15] = 1.0;
}


void bind_functions ()
{
    hc_bind_function ("render.vertices", binding_render_vertices);
    hc_bind_function ("render.vertices.draw", binding_render_vertices_draw);
    hc_bind_function ("render.vertices.load_circle", binding_render_vertices_load_circle);
    hc_bind_function ("render.vertices.load_line", binding_render_vertices_load_line);
    hc_bind_function ("render.vertices.load_line_box", binding_render_vertices_load_line_box);
    hc_bind_function ("render.vertices.load_line_strip", binding_render_vertices_load_line_strip);
    hc_bind_function ("render.vertices.load_rectangle", binding_render_vertices_load_rectangle);
    hc_bind_function ("render.vertices.load_ring", binding_render_vertices_load_ring);

    hc_bind_function ("intersections.check_point_circle", binding_intersections_check_point_circle);
    hc_bind_function ("intersections.check_box_circle", binding_intersections_check_box_circle);

    hc_bind_function ("math.sqrt", binding_math_sqrt);
    hc_bind_function ("math.hypot", binding_math_hypot);

    hc_bind_function ("math.dvec2.sub", binding_math_dvec2_sub);
    hc_bind_function ("math.dvec2.add", binding_math_dvec2_add);
    hc_bind_function ("math.dvec2.resize", binding_math_dvec2_resize);
    hc_bind_function ("math.dvec2.mul_inner", binding_math_dvec2_mul_inner);
    hc_bind_function ("math.dvec2.len", binding_math_dvec2_len);

    hc_bind_function ("math.dvec3.mul_inner", binding_math_dvec3_mul_inner);
    hc_bind_function ("math.dvec3.len", binding_math_dvec3_len);
    hc_bind_function ("math.dvec3.norm", binding_math_dvec3_norm);

    hc_bind_function ("math.dmat4.dup", binding_math_dmat4_dup);
    hc_bind_function ("math.dmat4.add", binding_math_dmat4_add);
    hc_bind_function ("math.dmat4.sub", binding_math_dmat4_sub);
    hc_bind_function ("math.dmat4.mul", binding_math_dmat4_mul);
    hc_bind_function ("math.dmat4.scale", binding_math_dmat4_scale);
    hc_bind_function ("math.dmat4.from_vec3_mul_outer", bindings_math_dmat4_from_vec3_mul_outer);
    hc_bind_function ("math.dmat4.rotate", binding_math_dmat4_rotate);
    hc_bind_function ("math.dmat4.translation", binding_math_dmat4_translation);
    hc_bind_function ("math.dmat4.ortho", binding_math_dmat4_ortho);
}
