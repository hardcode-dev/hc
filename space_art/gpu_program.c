#include "gpu_program.h"

#include "common.h"


static inline const char *get_shader_name (GLenum type)
{
    switch (type) {
    case GL_VERTEX_SHADER:
	return "vertex shader";
    case GL_FRAGMENT_SHADER:
	return "fragment shader";
    }
    return "shader";
}
static void print_shader_log (GLenum type, GLuint shader, int fail)
{
    GLint info_log_len;
    glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &info_log_len);
    if (info_log_len <= 1 && !fail)
	return;

    if (fail)
	fprintf (stderr, "Failed to compile %s:\n", get_shader_name (type));
    else
	fprintf (stderr, "Compilation log for %s:\n", get_shader_name (type));
    GLchar *info_log = check_malloc (info_log_len);
    GLsizei len;
    glGetShaderInfoLog (shader, info_log_len, &len, info_log);
    fprintf (stderr, "%.*s\n", (int) len, info_log);
    free (info_log);
}
static void print_program_log (GLuint program, int fail)
{
    GLint info_log_len;
    glGetProgramiv (program, GL_INFO_LOG_LENGTH, &info_log_len);
    if (info_log_len <= 1 && !fail)
	return;

    if (fail)
	fprintf (stderr, "Failed to compile program:\n");
    else
	fprintf (stderr, "Compilation log for program:\n");
    GLchar *info_log = check_malloc (info_log_len);
    GLsizei len;
    glGetProgramInfoLog (program, info_log_len, &len, info_log);
    fprintf (stderr, "%.*s\n", (int) len, info_log);
    free (info_log);
}
static GLuint gpu_compile_shader (GLenum type, const GLchar *shader_source, GLint shader_source_len)
{
    GLuint shader = glCreateShader (type);
    if (shader) {
	glShaderSource (shader, 1, &shader_source, &shader_source_len);
	glCompileShader (shader);
	GLint status;
	glGetShaderiv (shader, GL_COMPILE_STATUS, &status);
	if (status) {
	    print_shader_log (type, shader, 0);
	    return shader;
	} else {
	    print_shader_log (type, shader, 1);
	}
	glDeleteShader (shader);
    } else {
	fprintf (stderr, "Failed to create shader object\n");
    }
    return 0;
}
static GLuint gpu_link_program (GLuint vertex_shader, GLuint fragment_shader)
{
    GLuint program = glCreateProgram ();
    if (program) {
	glAttachShader (program, vertex_shader);
	glAttachShader (program, fragment_shader);
	glLinkProgram (program);
	GLint status;
	glGetProgramiv (program, GL_LINK_STATUS, &status);
	if (status) {
	    print_program_log (program, 0);
	    return program;
	} else {
	    print_program_log (program, 1);
	}
	glDeleteProgram (program);
    } else {
	fprintf (stderr, "Failed to create program object\n");
    }
    return 0;
}


GLuint gpu_load_program (const GLchar *vertex_shader_source, GLint vertex_shader_source_len,
			 const GLchar *fragment_shader_source, GLint fragment_shader_source_len)
{
    GLuint vertex_shader = gpu_compile_shader (GL_VERTEX_SHADER, vertex_shader_source, vertex_shader_source_len);
    if (vertex_shader) {
	GLuint fragment_shader = gpu_compile_shader (GL_FRAGMENT_SHADER, fragment_shader_source, fragment_shader_source_len);
	if (fragment_shader) {
	    GLuint program = gpu_link_program (vertex_shader, fragment_shader);
	    if (program) {
		glDeleteShader (vertex_shader);
		glDeleteShader (fragment_shader);
		return program;
	    }
	    glDeleteShader (fragment_shader);
	}
	glDeleteShader (vertex_shader);
    }
    return 0;
}
